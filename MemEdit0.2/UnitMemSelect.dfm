object frmMemSelect: TfrmMemSelect
  Left = 204
  Top = 116
  BorderStyle = bsDialog
  Caption = #20869#23384#36807#28388#35774#32622
  ClientHeight = 460
  ClientWidth = 443
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object BitBtnOK: TBitBtn
    Left = 120
    Top = 416
    Width = 75
    Height = 25
    Caption = #30830#23450
    Default = True
    TabOrder = 0
    OnClick = BitBtnOKClick
  end
  object BitBtnCancel: TBitBtn
    Left = 232
    Top = 416
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 1
    OnClick = BitBtnCancelClick
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 80
    Width = 409
    Height = 321
    Caption = #36873#25321#38656#35201#26597#35810#30340#27169#22359
    TabOrder = 2
    object RxCheckListBoxModuleList: TRxCheckListBox
      Left = 7
      Top = 17
      Width = 394
      Height = 256
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #23435#20307
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      Sorted = True
      TabOrder = 0
      InternalVersion = 202
      Strings = (
        'erter'
        0
        True
        'iyhi'
        1
        True
        'ter'
        0
        True
        'tre'
        1
        True)
    end
    object BitBtnRefresh: TBitBtn
      Left = 48
      Top = 284
      Width = 89
      Height = 25
      Caption = #24403#21069#31995#32479#35774#32622
      TabOrder = 1
      OnClick = BitBtnRefreshClick
    end
    object BitBtn1: TBitBtn
      Left = 160
      Top = 284
      Width = 89
      Height = 25
      Caption = #20840#37096#36873#20013
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 272
      Top = 284
      Width = 89
      Height = 25
      Caption = #20840#37096#21462#28040
      TabOrder = 3
      OnClick = BitBtn2Click
    end
  end
end
