.386
.model flat, stdcall
option casemap:none

include \masm32\include\kernel32.inc
include \masm32\include\user32.inc
include \masm32\include\windows.inc

includelib \masm32\lib\kernel32.lib
includelib \masm32\lib\user32.lib

; helper macro

	; exit a proc
	Exit MACRO
		xor eax, eax
		ret
	ENDM
      
      ; copy V1 to V2
	CopyValue MACRO V1, V2
		push V1
		pop V2 
	ENDM
	
; structure
	TMappingMem struct
		Handle DWORD ?
		MsgID DWORD ?
		MouseStruct MOUSEHOOKSTRUCT <>
	TMappingMem ends
  	
.const
	MappingFileName db "57D6A971_MouseHookDLL_442C0DB1", 0
  	MSGMOUSEMOVE db "MSGMOUSEMOVE57D6A971-049B-45AF-A8CD-37E0B706E036", 0
  	MSGMOUSECLICK db "MSGMOUSECLICK442C0DB1-3198-4C2B-A718-143F6E2D1760", 0
  	
.data
	hInstance dd ?
	szError db "cannot create share memory!", 0
	szError2 db "cannot map share memory!", 0
	szCaption db "Error", 0
  	
.data?
	MSG_MOUSEMOVE dd ?
  	MSG_MOUSECLICK dd ?
  	pMapMem dd ?
  	hMappingFile dd ?
  	mhook dd ?
  	fblocked dd ?
  	
.code
	DllEntry proc _hInstance, _dwReason, _dwReserved
		mov eax, _dwReason
		.if eax == DLL_PROCESS_ATTACH
			; init hinstance
			push _hInstance
			pop hInstance
			
			; open a file mapping
			invoke OpenFileMapping, FILE_MAP_WRITE, FALSE, addr MappingFileName
			mov hMappingFile, eax
			
			; create a file mapping to a TMappingMem
			.if hMappingFile == 0
				invoke CreateFileMapping, 0FFFFFFFFh, NULL, PAGE_READWRITE, 0, \
					sizeof TMappingMem, addr MappingFileName
				mov hMappingFile, eax
			.endif
			
			; popup error info
			.if hMappingFile == 0 
				invoke MessageBox, NULL, addr szError, addr szCaption, MB_OK or MB_ICONERROR
			.endif
			
			; map the mapping file
			invoke MapViewOfFile, hMappingFile, FILE_MAP_WRITE or FILE_MAP_READ, 0, 0, 0
			mov pMapMem, eax
			
			; popup error info
			.if pMapMem == NULL
				invoke CloseHandle, hMappingFile
				invoke MessageBox, 0, addr szError2, addr szCaption, MB_OK or MB_ICONERROR
			.endif
			
			; init mhook = 0
			mov mhook, 0h
			
			; get unique message uint
			invoke RegisterWindowMessage, addr MSGMOUSEMOVE
			mov MSG_MOUSEMOVE, eax
			invoke RegisterWindowMessage, addr MSGMOUSECLICK
			mov MSG_MOUSECLICK, eax
			
			; return true
			mov eax, TRUE
		.elseif eax == DLL_PROCESS_DETACH
			; unmap mapping file, close file handle
			invoke UnmapViewOfFile, pMapMem
			invoke CloseHandle, hMappingFile
			
			; if didn't stop hook, then stop it
			.if mhook != 0
				call DisableMouseHook
			.endif
		.endif
		ret 
	DllEntry endp
	
	MouseHookProc proc iCode, wParam, lParam
		.if iCode < 0 
			invoke CallNextHookEx, mhook, iCode, wParam, lParam
		.endif
		.if (wParam == WM_MOUSEMOVE) || (wParam == WM_NCMOUSEMOVE)
			mov ecx, pMapMem
			mov edx, lParam
			assume ecx:ptr TMappingMem
			assume edx:ptr MOUSEHOOKSTRUCT 
			CopyValue [ecx].MsgID, MSG_MOUSECLICK
			CopyValue [ecx].MouseStruct, [edx] 
			invoke SendMessage, [ecx].Handle, [ecx].MsgID, 0, 0
			xor ecx, ecx
			xor edx, edx
		.endif
		ret
	MouseHookProc endp
	
	EnableMouseHook proc hWindow, Blocked
		.if mhook == 0
			Exit
		.endif
		CopyValue Blocked, fblocked
		invoke SetWindowsHookEx, WH_MOUSE, addr MouseHookProc, hInstance, 0
		mov mhook, eax
		mov eax, FALSE
		.if mhook != 0
			mov eax, TRUE
		.endif
		ret
	EnableMouseHook endp
	
	DisableMouseHook proc
		.if mhook != 0
			invoke UnhookWindowsHookEx, mhook
			mov mhook, 0
		.endif
		mov eax, FALSE
		.if mhook == 0
			mov eax, TRUE
		.endif
		ret
	DisableMouseHook endp
	
	; end point
	end DllEntry