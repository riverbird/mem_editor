unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, StdCtrls,
  MouseHook, KeyboardHook, RXShell, Menus, marsCap, RxMenus, DBTables,
  MemTable, DB, RxMemDS, CoolTrayIcon;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    btStartMouse: TButton;
    btStartKeyboard: TButton;
    Label2: TLabel;
    Label3: TLabel;
    RxPopupMenu1: TRxPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    CoolTrayIcon1: TCoolTrayIcon;
    Button1: TButton;
    procedure btStartMouseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btStartKeyboardClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
  private
    mhook: TMouseHook;
    khook: TKeyboardHook;
    mhwnd:HWND;
    procedure HookMouseMove(const h: HWND; const X, Y: Integer);
    procedure HookMouseClick(const h: HWND; const X, Y: Integer);
    procedure HookKeyboardDown(const KeyCode: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btStartMouseClick(Sender: TObject);
begin
  if btStartMouse.Caption = 'Start' then
  begin
    if mhook.Start then
      btStartMouse.Caption := 'Stop';
  end
  else
  begin
    mhook.Stop;
    btStartMouse.Caption := 'Start';
  end;
end;

procedure TForm1.btStartKeyboardClick(Sender: TObject);
begin
  if btStartKeyboard.Caption = 'Start' then
  begin
    if khook.Start then
      btStartKeyboard.Caption := 'Stop';
  end
  else
  begin
    khook.Stop;
    btStartKeyboard.Caption := 'Start'; 
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // 创建mouse hook实例
  mhook := TMouseHook.Create;
  mhook.DLLName := 'mousehook.dll';
  mhook.Blocked := False;
  mhook.OnMouseMove := HookMouseMove;
  mhook.OnMouseClick := HookMouseClick;

  // 创建keyboard hook实例
  khook := TKeyboardHook.Create;
  khook.DLLName := 'keyboardhook.dll';
  khook.OnKeyDown := HookKeyboardDown;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  // 停止hook, 释放对象
  if khook.Active then
    khook.Stop;
  if Assigned(khook) then
    khook.Free;

  if mhook.Active then
    mhook.Stop;
  if Assigned(mhook) then
    mhook.Free;
end;

procedure TForm1.HookKeyboardDown(const KeyCode: Integer);
var
  str:pchar;
begin
  // 键盘消息测试
  if keycode=VK_MULTIPLY then
    begin
    mhwnd:=getforegroundwindow();
    //mhwnd:=getactivewindow();
    //str:=getwindowtext(mhwnd,str,1024);
    //mhwnd:=getcurrentprocess();
    showwindow(mhwnd,SW_MINIMIZE);
    setwindowpos(self.Handle,HWND_TOPMOST,self.left,self.Top,self.Width,self.Height,SWP_SHOWWINDOW);
    setforegroundwindow(self.Handle);
    setactivewindow(self.Handle);
    windows.SetFocus(self.handle);
    
    self.windowstate:=wsNormal;
    //if khook.Active then
    //khook.Stop;
    end;
end;

procedure TForm1.HookMouseMove(const h: HWND; const X, Y: Integer);
begin
  // 鼠标消息测试 OnMove
  Label1.Caption := Format('Position: WND: %d - X: %d - Y: %d', [h, X, Y])
end;

procedure TForm1.HookMouseClick(const h: HWND; const X, Y: Integer);
begin
  // 鼠标消息测试 OnClick
  Label2.Caption := Format('Click at: WND: %d - X: %d - Y: %d', [h, x, y]);
end;

procedure TForm1.N2Click(Sender: TObject);
begin
self.close;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  SHOWWINDOW(mhwnd,SW_SHOWnORMAL);
  setwindowpos(self.Handle,HWND_NOTOPMOST,self.left,self.Top,self.Width,self.Height,SWP_SHOWWINDOW);
  showwindow(self.Handle,SW_MINIMIZE);

end;

procedure TForm1.N3Click(Sender: TObject);
begin
  self.windowstate:=wsNormal;
end;

procedure TForm1.N4Click(Sender: TObject);
begin
  close;
end;

end.

