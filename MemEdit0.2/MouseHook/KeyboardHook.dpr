library KeyboardHook;

uses
  Windows, Messages;

const
  MappingFileName = '57D6A971_KeyboardHookDLL_442C0DB1';
  MSGKEYDOWN: PChar = 'MSGKEYDOWN57D6A971-049B-45AF-A8CD-37E0B706E036';
  MSGKEYUP: PChar = 'MSGKEYUP442C0DB1-3198-4C2B-A718-143F6E2D1760';

type
  TMappingMem = record
    Handle: DWORD;
    MsgID: DWORD;
    KeyCode: DWORD;
  end;
  PMappingMem = ^TMappingMem;

var
  MSG_KEYDOWN: UINT;
  MSG_KEYUP: UINT;
  hMappingFile: THandle;
  pMapMem: PMappingMem;
  khook: HHook;
  //CriticalSection: TRTLCriticalSection;

function KeyboardHookProc(iCode: Integer; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall
begin
  Result := 1;

  if iCode < 0 then
    Result := CallNextHookEx(khook, iCode, wParam, lParam);

  if iCode = HC_ACTION then
  begin
    pMapMem^.KeyCode := wParam;
    case ((lParam shr 30) and $F) of
      0:                                // Key down
        begin
          pMapMem^.MsgID := MSG_KEYDOWN;
          SendMessage(pMapMem^.Handle, pMapMem^.MsgID, 0, 0);
        end;
      1:                                // key up
        begin
          pMapMem^.MsgID := MSG_KEYUP;
          SendMessage(pMapMem^.Handle, pMapMem^.MsgID, 0, 0);
        end;
    end;
  end;
end;

function EnableKeyboardHook(hWindow: HWND): BOOL; stdcall;
begin
  Result := False;
  if khook <> 0 then
    Exit;
  pMapMem^.Handle := hWindow;
  khook := SetWindowsHookEx(WH_KEYBOARD, KeyboardHookProc, HInstance, 0);
  Result := khook <> 0;
end;

function DisableKeyboardHook: BOOL; stdcall;
begin
  if khook <> 0 then
  begin
    UnhookWindowshookEx(khook);
    khook := 0;
  end;
  Result := khook = 0;
end;

procedure DllMain(dwReason: DWORD);
begin
  case dwReason of
    DLL_PROCESS_ATTACH:
      begin
        //InitializeCriticalSection(CriticalSection);
        hMappingFile := OpenFileMapping(FILE_MAP_WRITE, False, MappingFileName);
        if hMappingFile = 0 then
        begin
          hMappingFile := CreateFileMapping($FFFFFFFF, nil, PAGE_READWRITE,
            0, SizeOf(TMappingMem), MappingFileName);
        end;
        if hMappingFile = 0 then
          MessageBox(0, 'cannot create share memory!', 'Error', MB_OK or MB_ICONERROR);

        pMapMem := MapViewOfFile(hMappingFile, FILE_MAP_WRITE or FILE_MAP_READ,
          0, 0, 0);
        if pMapMem = nil then
        begin
          CloseHandle(hMappingFile);
          MessageBox(0, 'cannot map share memory!', 'Error', MB_OK or MB_ICONERROR);
        end;
        khook := 0;
        MSG_KEYDOWN := RegisterWindowMessage(MSGKEYDOWN);
        MSG_KEYUP := RegisterWindowMessage(MSGKEYUP);
      end;
    DLL_PROCESS_DETACH:
      begin
        UnMapViewOfFile(pMapMem);
        CloseHandle(hMappingFile);
        if khook <> 0 then
          DisableKeyboardHook;
        //DeleteCriticalSection(CriticalSection);
      end;
    DLL_THREAD_ATTACH:
      begin
      end;
    DLL_THREAD_DETACH:
      begin
      end;
  end;
end;

exports
  EnableKeyboardHook,
  DisableKeyboardHook;

begin
  DisableThreadLibraryCalls(HInstance);
  DLLProc := @DLLMain;
  DLLMain(DLL_PROCESS_ATTACH);
end.

