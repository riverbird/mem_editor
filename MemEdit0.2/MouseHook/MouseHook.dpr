library MouseHook;

uses
  Windows, Messages;

const
  MappingFileName = '57D6A971_MouseHookDLL_442C0DB1';
  MSGMOUSEMOVE: PChar = 'MSGMOUSEMOVE57D6A971-049B-45AF-A8CD-37E0B706E036';
  MSGMOUSECLICK: PChar = 'MSGMOUSECLICK442C0DB1-3198-4C2B-A718-143F6E2D1760';

type
  TMappingMem = record
    Handle: DWORD;
    MsgID: DWORD;
    MouseStruct: TMOUSEHOOKSTRUCT;
  end;
  PMappingMem = ^TMappingMem;

var
  MSG_MOUSEMOVE: UINT;
  MSG_MOUSECLICK: UINT;
  hMappingFile: THandle;
  pMapMem: PMappingMem;
  mhook: HHook;
  fblocked: BOOL = True;
  //CriticalSection: TRTLCriticalSection;

function MouseHookProc(iCode: Integer; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall
begin
  if fblocked then
    Result := 1
  else
    Result := 0;

  if iCode < 0 then
    CallNextHookEx(mhook, iCode, wParam, lParam);

  case wParam of
    WM_MOUSEMOVE, WM_NCMouseMove:
      begin
        pMapMem^.MsgID := MSG_MOUSEMOVE;
        pMapMem^.MouseStruct := pMOUSEHOOKSTRUCT(lparam)^;
        SendMessage(pMapMem^.Handle, pMapMem^.MsgID, 0, 0);
      end;
    WM_LBUTTONDOWN, WM_NCLBUTTONDOWN:
      begin
        pMapMem^.MsgID := MSG_MOUSECLICK;
        pMapMem^.MouseStruct := pMOUSEHOOKSTRUCT(lparam)^;
        SendMessage(pMapMem^.Handle, pMapMem^.MsgID, 0, 0);
      end;
    WM_LBUTTONUP:
      begin
      end;
    WM_LBUTTONDBLCLK:
      begin
      end;
    WM_RBUTTONDOWN:
      begin
      end;
    WM_RBUTTONUP:
      begin
      end;
    WM_RBUTTONDBLCLK:
      begin
      end;
    WM_MBUTTONDOWN:
      begin
      end;
    WM_MBUTTONUP:
      begin
      end;
    WM_MBUTTONDBLCLK:
      begin
      end;
  end;
end;

function EnableMouseHook(hWindow: HWND; Blocked: BOOL): BOOL; stdcall;
begin
  Result := False;
  if mhook <> 0 then
    Exit;
  pMapMem^.Handle := hWindow;
  fblocked := Blocked;
  mhook := SetWindowsHookEx(WH_MOUSE, MouseHookProc, HInstance, 0);
  Result := mhook <> 0;
end;

function DisableMouseHook: BOOL; stdcall;
begin
  if mhook <> 0 then
  begin
    UnhookWindowshookEx(mhook);
    mhook := 0;
  end;
  Result := mhook = 0;
end;

procedure DllMain(dwReason: DWORD);
begin
  case dwReason of
    DLL_PROCESS_ATTACH:
      begin
        //InitializeCriticalSection(CriticalSection);
        hMappingFile := OpenFileMapping(FILE_MAP_WRITE, False, MappingFileName);
        if hMappingFile = 0 then
        begin
          hMappingFile := CreateFileMapping($FFFFFFFF, nil, PAGE_READWRITE,
            0, SizeOf(TMappingMem), MappingFileName);
        end;
        if hMappingFile = 0 then
          MessageBox(0, 'cannot create share memory!', 'Error', MB_OK or MB_ICONERROR);

        pMapMem := MapViewOfFile(hMappingFile, FILE_MAP_WRITE or FILE_MAP_READ,
          0, 0, 0);
        if pMapMem = nil then
        begin
          CloseHandle(hMappingFile);
          MessageBox(0, 'cannot map share memory!', 'Error', MB_OK or MB_ICONERROR);
        end;
        mhook := 0;
        MSG_MOUSEMOVE := RegisterWindowMessage(MSGMOUSEMOVE);
        MSG_MOUSECLICK := RegisterWindowMessage(MSGMOUSECLICK);
      end;
    DLL_PROCESS_DETACH:
      begin
        UnMapViewOfFile(pMapMem);
        CloseHandle(hMappingFile);
        if mhook <> 0 then
          DisableMouseHook;
        //DeleteCriticalSection(CriticalSection);
      end;
    DLL_THREAD_ATTACH:
      begin
      end;
    DLL_THREAD_DETACH:
      begin
      end;
  end;
end;

exports
  EnableMouseHook,
  DisableMouseHook;

begin
  DisableThreadLibraryCalls(HInstance);
  DLLProc := @DLLMain;
  DLLMain(DLL_PROCESS_ATTACH);
end.

