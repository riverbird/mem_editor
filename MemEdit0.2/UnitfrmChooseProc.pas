unit UnitfrmChooseProc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons ,TLHELP32_Hss, ImgList, UnitCommon,
  ExtCtrls, RxHook, math;

type
  TfrmChooseProc = class(TForm)
    GroupBox1: TGroupBox;
    BitBtnOK: TBitBtn;
    BitBtnCancel: TBitBtn;
    TreeViewProcList: TTreeView;
    Label1: TLabel;
    EditName: TEdit;
    ImageListProcIcons: TImageList;
    BitBtnRefProc: TBitBtn;
    ImageTemp: TImage;
    Image1: TImage;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtnOKClick(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure TreeViewProcListChange(Sender: TObject; Node: TTreeNode);
    procedure BitBtnRefProcClick(Sender: TObject);
    procedure EditNameChange(Sender: TObject);
    procedure EditNameKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    FProcessesID : Handle;
    FMissionName : string;
    IsOk  : Boolean;
  end;

var
  frmChooseProc: TfrmChooseProc=nil;

  function ExtractIcon(
    hist         : Handle ;
    lpfilemane   : LPCTSTR;
    nIndex       : UINT=0
    ):HIcon;stdcall;

implementation

{$R *.dfm}

function ExtractIcon; external 'shell32.dll' name 'ExtractIconA';

procedure TfrmChooseProc.FormCreate(Sender: TObject);
begin
  self.Left:=max(0,(screen.Width-self.Width) div 2);
  self.Top:=max(0,(screen.Height-self.Height) div 2);

  IsOk:=false;
  
  self.BitBtnRefProcClick(Sender);
  self.FMissionName:='';
end;

procedure TfrmChooseProc.BitBtnOKClick(Sender: TObject);
begin
  self.FMissionName:=self.EditName.Text;
  self.IsOk:=true;
  self.Close;
end;

procedure TfrmChooseProc.BitBtnCancelClick(Sender: TObject);
begin
  self.FProcessesID:=0;
  self.FMissionName:='';
  self.IsOk:=false;
  self.Close;
end;

procedure TfrmChooseProc.TreeViewProcListChange(Sender: TObject; Node: TTreeNode);
var
  HwndText : string;
begin
  if self.TreeViewProcList.Enabled then
  begin
    self.FProcessesID:=THandle(Node.Data);
    //HwndText:=Node.Text;
    HwndText:='新任务';
    if (length(HwndText)>=4) and
      (uppercase(copy(HwndText,length(HwndText)-3,4))='.EXE') then
        HwndText:=copy(HwndText,1,length(HwndText)-4);
    self.FMissionName:=HwndText;//任务名称
    self.EditName.Text:=HwndText;//任务名称
  end;
end;
//列举所有进程
procedure TfrmChooseProc.BitBtnRefProcClick(Sender: TObject);
var
  Snapshot  : TProcessList;
  i         : integer;
  pe: PROCESSENTRY32 ;
  TreeNode  : TTreeNode;
  NodeName  : string;
  AddCount  : integer;
  Icon      : TIcon;
  MyProcessesID : THandle;
  IsCanOpen : THandle;
begin
  self.TreeViewProcList.Enabled:=false;
  self.TreeViewProcList.Items.Clear;


  Snapshot:=TProcessList.Create();
  try
    ImageListProcIcons.Clear;
    ImageListProcIcons.AddIcon(self.ImageTemp.Picture.Icon);
    
    AddCount:=0;
    MyProcessesID:=windows.GetCurrentProcessId();
    for i:=Snapshot.Count-1 downto 0 do
    begin
      pe:=Snapshot.Items[i];
      if (MyProcessesID<>pe.th32ProcessID) then   //不显示自己
      begin
        IsCanOpen:=OpenProcess(PROCESS_VM_OPERATION or PROCESS_VM_READ
                      or PROCESS_VM_WRITE or PROCESS_QUERY_INFORMATION,true, pe.th32ProcessID);
        if IsCanOpen<>0 then  //能打开
        begin
          windows.CloseHandle(IsCanOpen);

          NodeName:=pe.szExeFile;//inttohex(pe.,8);
          NodeName:=ExtractFileName(NodeName);
          TreeNode:=self.TreeViewProcList.Items.AddChild(nil,NodeName);//+' '+inttostr(pe.th32ProcessID)
          TreeNode.Data:=pointer(pe.th32ProcessID);

          //{
          Icon:=TIcon.Create;
          Icon.Handle:=ExtractIcon(hinstance,pe.szExeFile,0);
          if Icon.Handle<>0 then
          begin
            inc(AddCount);
            self.ImageListProcIcons.AddIcon(Icon);
            TreeNode.ImageIndex:=AddCount;
            TreeNode.SelectedIndex:=AddCount;
          end
          else
          begin
            TreeNode.ImageIndex:=0;
            TreeNode.SelectedIndex:=0;
          end;
          Icon.Free;
          //}
        end; //end if
      end; //end if
    end;//end for i

  finally
    self.TreeViewProcList.Enabled:=true;
    Snapshot.Free;
  end;
end;

procedure TfrmChooseProc.EditNameChange(Sender: TObject);
begin
  self.EditName.Hint:=self.EditName.Text;
end;

procedure TfrmChooseProc.EditNameKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#13) or (Key=#10) then
  begin
    Key:=#0;
    self.BitBtnOKClick(sender);
  end;
end;

end.
