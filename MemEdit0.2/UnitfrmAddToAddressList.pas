unit UnitfrmAddToAddressList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, UnitAboutAddress, UnitCommon, math, ExtCtrls,
  Buttons;

  
type
  TfrmAddToAddressList = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    ComboBoxName: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    EditValue: TEdit;
    Label3: TLabel;
    EditMinMax: TEdit;
    Label4: TLabel;
    ComboBoxByteCount: TComboBox;
    EditAddress: TEdit;
    Label5: TLabel;
    RadioButtonAutoEdit: TRadioButton;
    RadioButtonSelfEdit: TRadioButton;
    RadioButtonHotKeyEdit: TRadioButton;
    HotKey: THotKey;
    CheckBoxWriteOnce: TCheckBox;
    Image1: TImage;
    Label6: TLabel;
    ButtonOK: TBitBtn;
    ButtonCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure ButtonOKClick(Sender: TObject);
    procedure RadioButtonAutoEditClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBoxByteCountChange(Sender: TObject);
    procedure EditValueChange(Sender: TObject);
    procedure EditMinMaxChange(Sender: TObject);
    procedure ComboBoxNameChange(Sender: TObject);
    procedure HotKeyChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    IsOk          : boolean;
    //FAdd          : boolean;
    FName         : string;
    FValue        : string;
    FAddress      : Pointer;
    FEditType     : TEditType;
    FHotKey       : TShortCut;
    FValueType    : TValueType;
    FValueTypeAll : TSetOfValueType;
  end;

var
  frmAddToAddressList: TfrmAddToAddressList=nil;

implementation

{$R *.dfm}

procedure TfrmAddToAddressList.FormCreate(Sender: TObject);
begin
  self.Left:=max(0,(screen.Width-self.Width) div 2);
  self.Top:=max(0,(screen.Height-self.Height) div 2);

  self.IsOk:=false;
  self.FAddress:=nil;
end;

procedure TfrmAddToAddressList.ButtonCancelClick(Sender: TObject);
begin
  self.FName:='';
  self.FValue:='';
  self.FAddress:=nil;
  self.FEditType:=etNoDefine;
  self.FHotKey:=0;
  self.FValueType:=vtNoDefine;
  self.IsOk:=false;
  self.Close;
end;

procedure TfrmAddToAddressList.ButtonOKClick(Sender: TObject);
var
  ErrorStr  : string;
  XInt : int64;
  //xint:integer;
  XFloat : Extended;
begin
  self.IsOk:=true;

  self.FName:=self.ComboBoxName.Text;
  self.FValue:=self.EditValue.Text;
  if self.FValue='' then
  begin
    ErrorStr:=' 数据不能为空!  ';
    self.IsOk:=false;
  end;

  try
    self.FAddress:=pointer(strtoint('$'+self.EditAddress.text));
  except
    ErrorStr:=' 地址值格式填写不正确!  (用16进制表示)  ';
    self.IsOk:=false;
  end;
  if self.FAddress=nil then
  begin
    ErrorStr:=' 地址值格式填写不正确! ';
    self.IsOk:=false;
  end;

  self.FHotKey:=self.HotKey.HotKey;
  if (self.FEditType=etHotKey) and (self.FHotKey=0) then
  begin
    ErrorStr:=' 请设定热键! ';
    self.IsOk:=false;
  end;

  try
    if self.ComboBoxByteCount.Text='单字节' then
    begin
      self.FValueType:=vtByte;
      xint:=strtoint64(self.EditValue.Text);
      if (xint<-128) or (xint>255)then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (-128..255)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='双字节' then
    begin
      self.FValueType:=vtWord;
      xint:=strtoint64(self.EditValue.Text);
      if (xint<-32768) or (xint>65535)then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (-32768..65535)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='四字节' then
    begin
      self.FValueType:=vtDWord;
      xint:=strtoint64(self.EditValue.Text);
      //if (xint<-2147483648) or (xint>4294967295)then
      if (xint<-2147483647) or (xint>$FFFFFFFF) then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (-2147483648..4294967295)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='八字节' then
    begin
      self.FValueType:=vtDDWord;
      xFloat:=strtoFloat(self.EditValue.Text);
      if (xFloat<-9223372036854775808.0) or (xFloat>9223372036854775807.0)then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (-9223372036854775808..9223372036854775807)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='单精度' then
    begin
      self.FValueType:=vtSingle;
      xFloat:=abs(strtoFloat(self.EditValue.Text));
      if (xFloat<1.5E-45) or (xFloat>3.4E38)then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (+/-1.5E-45..+/-3.4E38)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='双精度' then
    begin
      self.FValueType:=vtDouble;
      xFloat:=abs(strtoFloat(self.EditValue.Text));
      if (xFloat<5.0E-324) or (xFloat>1.7E308)then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (+/-5.0E-324..+/-1.7E308)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='扩展精度' then
    begin
      self.FValueType:=vtDouble;
      xFloat:=abs(strtoFloat(self.EditValue.Text));
      if (xFloat<3.6E-4951) or (xFloat>1.1E4932)then
      begin
        ErrorStr:=' 数据值超出了设定的数据类型所属的范围! (+/-3.6E-4951..+/-1.1E4932)';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='字符串' then
    begin
      self.FValueType:=vtAString;
      if self.FValue='' then
      begin
        ErrorStr:=' 字符串不能为空! ';
        self.IsOk:=false;
      end;
    end
    else if self.ComboBoxByteCount.Text='宽字符串' then
    begin
      self.FValueType:=vtWString;
      if self.FValue='' then
      begin
        ErrorStr:=' 宽字符串不能为空! ';
        self.IsOk:=false;
      end;
    end
    else
    begin
      ErrorStr:=' 请选择数据的类型! ';
      self.IsOk:=false;
    end;
  except
    ErrorStr:=' 数据值与数据类型不匹配! ';
    self.IsOk:=false;
  end;
  
  if self.IsOk then
  begin
    self.Close;
  end
  else
  begin
    messageBox(self.Handle,PChar(ErrorStr),'填写有错',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TfrmAddToAddressList.RadioButtonAutoEditClick(Sender: TObject);
begin
  if Sender=RadioButtonAutoEdit then
  begin
    self.HotKey.Enabled:=false;
    self.FEditType:=etAuto;
  end
  else if Sender=RadioButtonSelfEdit then
  begin
    self.HotKey.Enabled:=false;
    self.FEditType:=etSelf;
  end
  else if Sender=RadioButtonHotKeyEdit then
  begin
    self.HotKey.Enabled:=true;
    self.FEditType:=etHotKey;
  end;
end;

procedure TfrmAddToAddressList.FormShow(Sender: TObject);
begin
  self.IsOk:=false;
  self.EditValue.Text:=self.FValue;
  self.ComboBoxName.Text:=self.FName;
  self.EditAddress.Text:=inttohex(integer(self.FAddress),8);
  
  if vtByte in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('单字节');
  if vtWord in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('双字节');
  if vtDWord in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('四字节');
  if vtDDWord in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('八字节');
  if vtSingle in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('单精度');
  if vtDouble in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('双精度');
  if vtLDouble in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('扩展精度');
  if vtAString in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('字符串');
  if vtWString in self.FValueTypeAll then
    self.ComboBoxByteCount.Items.Add('宽字符串');

  if self.ComboBoxByteCount.Items.Count=0 then
  begin
    self.ComboBoxByteCount.Items.Add('单字节');
    self.ComboBoxByteCount.Items.Add('双字节');
    self.ComboBoxByteCount.Items.Add('四字节');
    self.ComboBoxByteCount.Items.Add('八字节');
    self.ComboBoxByteCount.Items.Add('单精度');
    self.ComboBoxByteCount.Items.Add('双精度');
    self.ComboBoxByteCount.Items.Add('扩展精度');
    self.ComboBoxByteCount.Items.Add('字符串');
    self.ComboBoxByteCount.Items.Add('宽字符串');
  end;

  
  if self.FValueTypeAll=[] then
  begin
    if vtByte = self.FValueType then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('单字节')
    else if vtWord  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('双字节')
    else if vtDWord  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('四字节')
    else if vtDDWord  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('八字节')
    else if vtSingle  = self.FValueType then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('单精度')
    else if vtDouble  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('双精度')
    else if vtLDouble  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('扩展精度')
    else if vtAString  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('字符串')
    else if vtWString  = self.FValueType  then
      self.ComboBoxByteCount.ItemIndex:=self.ComboBoxByteCount.Items.IndexOf('宽字符串');
  end
  else
  begin
    self.ComboBoxByteCount.ItemIndex:=0;
  end;
  self.ComboBoxByteCountChange(Sender);

  self.HotKey.Enabled:=false;
  if self.FEditType=etSelf then
    self.RadioButtonSelfEdit.Checked:=true
  else if self.FEditType=etHotKey then
  begin
    self.RadioButtonHotKeyEdit.Checked:=true;
    self.HotKey.Enabled:=true;
  end
  else
    self.RadioButtonAutoEdit.Checked:=true;

  self.EditMinMax.Hint:=self.EditMinMax.Text;
end;

procedure TfrmAddToAddressList.ComboBoxByteCountChange(Sender: TObject);
begin
  if self.ComboBoxByteCount.Text='单字节' then
     self.EditMinMax.Text:='-128..255'
  else if self.ComboBoxByteCount.Text='双字节' then
     self.EditMinMax.Text:='-32768..65535'
  else if self.ComboBoxByteCount.Text='四字节' then
     self.EditMinMax.Text:='-2147483648..4294967295'
  else if self.ComboBoxByteCount.Text='八字节' then
     self.EditMinMax.Text:='-9223372036854775808..9223372036854775807'
  else if self.ComboBoxByteCount.Text='单精度' then
     self.EditMinMax.Text:='+/-1.5E-45..+/-3.4E38'
  else if self.ComboBoxByteCount.Text='双精度' then
     self.EditMinMax.Text:='+/-5.0E-324..+/-1.7E308'
  else if self.ComboBoxByteCount.Text='扩展精度' then
     self.EditMinMax.Text:='+/-3.6E-4951..+/-1.1E4932'
  else if self.ComboBoxByteCount.Text='字符串' then
     self.EditMinMax.Text:='任意个字符'
  else if self.ComboBoxByteCount.Text='宽字符串' then
     self.EditMinMax.Text:='任意个字符'
  else
     self.EditMinMax.Text:='';
  self.EditMinMax.Hint:=self.EditMinMax.Text;
end;

procedure TfrmAddToAddressList.EditValueChange(Sender: TObject);
begin
  self.EditValue.Hint:=self.EditValue.Text;
end;

procedure TfrmAddToAddressList.EditMinMaxChange(Sender: TObject);
begin
  self.EditMinMax.Hint:=self.EditMinMax.Text;
end;

procedure TfrmAddToAddressList.ComboBoxNameChange(Sender: TObject);
begin
  self.ComboBoxName.Hint:=self.ComboBoxName.Text;
end;

procedure TfrmAddToAddressList.HotKeyChange(Sender: TObject);
begin
  self.HotKey.Hint:=HotKeyToStr(self.HotKey.HotKey);
end;

end.
