unit PassWord_Hss;

          //////////////////////////////////////////////////////
          //                                                  //
          //       密码处理函数库  作者：侯思松   2002年。    //
          //                                                  //
          //               HouSisong@263.net                  //
          //                                                  //
          //////////////////////////////////////////////////////

interface

uses Sysutils,windows;

  {
  说明:
    如果模块源代码被别人知道或解密,只要没有指定的密匙,也很难得到原数据。
    有特殊安全要求的用户请采用其他满足要求的加密算法。
  }


  //加密
  function  GetNewData(const Data:string;const Key:string):string;overload;
  Procedure GetNewData(const Data:array of byte;const DataLength:integer;
                           const Key:string;var NewData:array of byte);overload;
  //解密
  function  GetOldData(const Data:string;const Key:string):string;overload;
  Procedure GetOldData(const Data:array of byte;const DataLength:integer;
                           const Key:string;var OldData:array of byte);overload;


  // 转化密码Key
  function  TranslateKey(const Key :string):string; overload;
  function  TranslateKey(const Key :word):word; overload;
  

  //把数据转换为十六进制文本表示
  function  DataToHEX(const Data:string):string; overload;//长度会变为两倍
  Procedure DataToHEX(const Data:array of byte;const DataLength:integer;var HEX:string); overload;//长度会变为两倍
  //把十六进制文本表示的数据转化回来
  function  HEXToData(const HEX:string):string; overload;//长度会变为1/2
  Procedure HEXToData(const HEX:string;var OldData:array of byte); overload;//长度会变为1/2


  //字节 与 十六进制 之间转换
  Procedure ByteToHEX(const bX:byte;out HEXchar0,HEXchar1:Char);
  function  HEXToByte(const HEXchar0,HEXchar1:Char):byte;

//===================================================
{ //获取密码编辑框中的密码

  //vb6.0
  Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
  Function SetCapture Lib "user32" (ByVal hwnd As Long) As Long
  Function ClientToScreen Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
  Function WindowFromPoint Lib "user32" (ByVal xPoint As Long, ByVal yPoint As Long) As Long
  Function GetLastError Lib "kernel32" () As Long
  Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As String) As Long
  Function ReleaseCapture Lib "user32" () As Long
  Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hwnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long

  Const WM_GETTEXT = &HD
  Type POINTAPI =record
    x : integer ;
    y : integer ;
  End ;  }
{
Dim IsDragging As Boolean

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If IsDragging = False Then
    IsDragging = True
    Screen.MouseIcon = LoadPicture(App.Path + "\Dragging.ico")
    Screen.MousePointer = vbCustom
    '将以后的鼠标输入消息都发送到本程序窗口
    SetCapture (frmMain.hwnd)
  End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  If IsDragging = True Then
    Dim rtn As Long, curwnd As Long
    Dim tempstr As String
    Dim strlong As Long
    Dim point As POINTAPI
    point.x = x
    point.y = y
    '将客户坐标转化为屏幕坐标并显示在PointText文本框中
    If ClientToScreen(frmMain.hwnd, point) = 0 Then Exit Sub
    PointText.Text = Str(point.x) + "," + Str(point.y)
    '获得鼠标所在的窗口句柄并显示在hWndText文本框中
    curwnd = WindowFromPoint(point.x, point.y)
    hWndText.Text = Str(curwnd)
    '获得该窗口的类型并显示在WndClassText文本框中
    tempstr = Space(255)
    strlong = Len(tempstr)
    rtn = GetClassName(curwnd, tempstr, strlong)
    If rtn = 0 Then Exit Sub
    tempstr = Trim(tempstr)
    WndClassText.Text = tempstr
    '向该窗口发送一个WM_GETTEXT消息，以获得该窗口的文本，并显示在PasswordText文本框中
    tempstr = Space(255)
    strlong = Len(tempstr)
    rtn = SendMessage(curwnd, WM_GETTEXT, strlong, tempstr)
    tempstr = Trim(tempstr)
    PasswordText.Text = tempstr
  End If
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
  If IsDragging = True Then
    Screen.MousePointer = vbDefault
    IsDragging = False
    '释放鼠标消息抓取
    ReleaseCapture
  End If
End Sub

  }

implementation

  const C1:integer=6447407;   {公匙1}
  const C2:integer=5850529;   {公匙2}

var
  NumberToChar: array [0..15] of Char
      =('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
var
  CharToNumber: array ['0'..'F'] of BYTE
      =(0,1,2,3,4,5,6,7,8,9,-0,-0,-0,-0,-0,-0,-0,10,11,12,13,14,15);

Procedure ByteToHEX(const bX:byte;out HEXchar0,HEXchar1:Char);
begin
  HEXchar0:=NumberToChar[(bx shr 4)];
  HEXchar1:=NumberToChar[(bx and $F)];
end;

function  HEXToByte(const HEXchar0,HEXchar1:Char):byte;
var
  c1,c2 :byte;
begin
  c1:=CharToNumber[HEXchar0];
  c2:=CharToNumber[HEXchar1];
  result:=c1 shl 4+c2;
end;

function  DataToHEX(const Data:string):string;
var
  tempstr :string;
  i,L : integer;
  d   : integer;
begin
  L:=length(Data);
  setlength(tempStr,L*2);
  for i:=1 to L do
  begin
    d:=byte(Data[i]);
    ByteToHEX(d,tempstr[i*2-1],tempstr[i*2]);
  end;
  result:=tempstr;
end;

Procedure DataToHEX(const Data:array of byte;const DataLength:integer;var HEX:string); overload;//长度会变为两倍
var
  i       : integer;
begin
  setlength(HEX,DataLength*2);
  for i:=1 to DataLength do
  begin
    ByteToHEX(Data[i],HEX[i*2-1],HEX[i*2]);
  end;
end;

function  HEXToData(const HEX:string):string;
var
  tempstr :string;
  i,L : integer;
  d   : integer;
begin
  L:=length(Hex) div 2;
  setlength(tempStr,L);
  for i:=1 to L do
  begin
    d:=HEXToByte(Hex[i*2-1],Hex[i*2]);
    tempstr[i]:=char(d);
  end;
  result:=tempstr;
end;

Procedure HEXToData(const HEX:string;var OldData:array of byte); overload;//长度会变为1/2
var
  i,L : integer;
begin
  L:=length(Hex) div 2;
  for i:=1 to L do
  begin
    OldData[i]:=HexToByte(Hex[i*2-1],Hex[i*2]);
  end;
end;

function TranslateKey(const Key :word):word;
begin
  result:=Key * 58937+89821;
  result:=result xor c1 ;
  result:=result* 75217+45833;
  result:=result xor c2 ;
end;

function TranslateKey(const Key :string):string;
var KeyStr        :string;
    i,j,L,t,tTemp :integer;
    KeyTemp       :array of byte;
begin
    KeyStr:=Key+inttostr(length(Key));
    if length(KeyStr) mod 2=1 then KeyStr:=KeyStr+' ';
    L:=length(KeyStr);
    setlength(Keytemp,L);
    for i:=0 to L-1 do
    begin
        Keytemp[i]:=byte(KeyStr[i+1]);
    end;
    for i:=0 to L*4-1 do
    begin
        j:=i mod L;
        t:=Keytemp[j] mod L;
        tTemp:=Keytemp[j];
        Keytemp[j]:=(Keytemp[t] * 75217+45833);
        Keytemp[t]:=(tTemp * 58937+89821);
        Keytemp[j]:=TranslateKey(Keytemp[j]);
        Keytemp[t]:=TranslateKey(Keytemp[t]);
    end;
    setlength(result,L);
    for i:=0 to L-1 do
    begin
        result[i+1]:=char(Keytemp[i]);
    end;
end;


function GetNewData(const Data:string;const Key:string):string;
var i,j,L   :integer;
    KeyStr  :string;
    Keytemp :array of word;
begin
    result:=Data;
    KeyStr:=TranslateKey(Key);
    L:=length(KeyStr) div 2 ;
    setlength(Keytemp,L+1);
    for i:=0 to L-1 do
    begin
        Keytemp[i]:=byte(KeyStr[i*2+1])*256+byte(KeyStr[i*2+2]);
    end;
    Keytemp[L]:=0;
    for i:=1 to length(Data) do
    begin
        j:=i mod L;
        result[i]:=char(byte(Data[i]) xor (Keytemp[j] shr 8));
        Keytemp[j+1]:=word((byte(result[i])+Keytemp[j])*C1+C2);
        Keytemp[j+1]:=TranslateKey(Keytemp[j+1]);
    end;
end;

Procedure GetNewData(const Data:array of byte;const DataLength:integer;
                         const Key:string;var NewData:array of byte);
var i,j,L   :integer;
    KeyStr  :string;
    Keytemp :array of word;
begin
    KeyStr:=TranslateKey(Key);
    L:=length(KeyStr) div 2 ;
    setlength(Keytemp,L+1);
    for i:=0 to L-1 do
    begin
        Keytemp[i]:=byte(KeyStr[i*2+1])*256+byte(KeyStr[i*2+2]);
    end;
    Keytemp[L]:=0;
    for i:=0 to Datalength-1 do
    begin
        j:=i mod L;
        NewData[i]:=byte(Data[i]) xor (Keytemp[j] shr 8);
        Keytemp[j+1]:=word((byte(NewData[i])+Keytemp[j])*C1+C2);
        Keytemp[j+1]:=TranslateKey(Keytemp[j+1]);
    end;
end;

function GetOldData(const Data:string;const Key:string):string;
var i,j,L   :integer;
    KeyStr  :string;
    Keytemp :array of word;
begin
    result:=Data;
    KeyStr:=TranslateKey(Key);
    L:=length(KeyStr) div 2 ;
    setlength(Keytemp,L+1);
    for i:=0 to L-1 do
    begin
        Keytemp[i]:=byte(KeyStr[i*2+1])*256+byte(KeyStr[i*2+2]);
    end;
    Keytemp[L]:=0;
    for i:=1 to length(Data) do
    begin
        j:=i mod L;
        result[i]:=char(byte(Data[i]) xor (Keytemp[j] shr 8));
        Keytemp[j+1]:=word((byte(Data[i])+Keytemp[j])*C1+C2);
        Keytemp[j+1]:=TranslateKey(Keytemp[j+1]);
    end;
end;

Procedure GetOldData(const Data:array of byte;const DataLength:integer;
                         const Key:string;var OldData:array of byte);
var i,j,L   :integer;
    KeyStr  :string;
    Keytemp :array of word;
begin
    KeyStr:=TranslateKey(Key);
    L:=length(KeyStr) div 2 ;
    setlength(Keytemp,L+1);
    for i:=0 to L-1 do
    begin
        Keytemp[i]:=byte(KeyStr[i*2+1])*256+byte(KeyStr[i*2+2]);
    end;
    Keytemp[L]:=0;
    for i:=0 to Datalength-1 do
    begin
        j:=i mod L;
        OldData[i]:=(Data[i]) xor (Keytemp[j] shr 8);
        Keytemp[j+1]:=word((byte(Data[i])+Keytemp[j])*C1+C2);
        Keytemp[j+1]:=TranslateKey(Keytemp[j+1]);
    end;
end;

//==============================================================================

            {     密码处理函数库  作者：侯思松   2002年。     }

{密码处理函数库单元结束}

end.
