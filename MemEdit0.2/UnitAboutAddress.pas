unit UnitAboutAddress;

// HouSisong 2003.2

    ////////////////////////////////////////////////////////////////////////////
    //                 TMemEditMission    查找任务 类                         //
    //            负责具体的查找过滤任务, 进程内存读写操作                    //
    ////////////////////////////////////////////////////////////////////////////
    //           TMemEditAddressList    用户变量监视管理类                    //
    //              用于管理被用户选择监视的变量列表                          //
    ////////////////////////////////////////////////////////////////////////////
    //           TMemEditAddressList    用户变量监视管理类                    //
    //              用于管理被用户选择监视的变量列表                          //
    ////////////////////////////////////////////////////////////////////////////

    //管理程序设置  类
    //TMemEditSysSet = class ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes,Forms, math, UnitCommon,
  Memory_Hss, TLHELP32_Hss, CPUCounter_Hss, UnitfrmSystemSet,IniFiles,
  UnitMemSelect, UnitHookDllFunction, IdTunnelCommon;


  //现在实现的模糊查找中的内存监视算法还有待改进
  //  算法要求：监视目标程序中被修改的内存地址
  //  当前算法实现：保存被监视内存的原始拷贝
  //    现在的算法需要被监视地址使用内存数量的两倍(加上行要保存每个内存地址当前的可能数据类型)，这对一些大程序效率较低
  //    正在考虑的方案：使用CRC等校验技术; 把被监视内存区域分成小块，计算出每个小块的
  //    校验值,根据两次得到的校验值的不同来确认内存数据的改变(这种方法的缺点是有可能
  //    碰巧出现数据改变但校验值相同，而且监视的改变单位不能精确到字节),可以在模糊查找初期
  //    使用这种方法，当排除足够多的内存区后再采用以前的方法
  //    (欢迎提供新的方案或意见，HouSisong@263.net  2003.4)

  type
    //监视的内存地址处的可能类型 ,支持9种基本类型
    TValueType = (  vtNoDefine,vtByte,vtWord,vtDWord,vtDDWord,
                    vtSingle,vtDouble,vtLDouble,
                    vtAString,vtWString
                    ,vtTemp0,vtTemp1,vtTemp2,vtTemp3,vtTemp4,vtTemp5,vtTemp6,vtTemp7  //占位,使该类型的集合类型为32bit :)
                    );
    TSetOfValueType= set of TValueType; //可能类型的组合

    TarrayTMemoryBasicInformation = array of TMemoryBasicInformation;  //用来保存内存区域信息


    //监视的内存地址结构
    TValueAddress = record
      Address   : Pointer;
      ValueType : set of  TValueType;
    end;
    TArrayTValueAddress  = array of TValueAddress;

  type
    TEditType = (etNoDefine,etAuto,etSelf,etHotKey);   //被监视内存列表的数值修改类型

    TFindType = (ftNoDefine,ftInt,ftFloat,ftString,ftBlur);   //任务查找类型


  const   //模糊查找时，变量的可能类型 , 支持7种基本类型
    ftBlurByte    = 1 shl 0;
    ftBlurWord    = 1 shl 1;
    ftBlurDWord   = 1 shl 2;
    ftBlurDDWord  = 1 shl 3;
    ftBlurSingle  = 1 shl 4;
    ftBlurDouble  = 1 shl 5;
    ftBlurExtended= 1 shl 6;

  type
    TFiltrateSet = (fsSys,fsNo,fsAll);  //过滤设置
    const SysSetIniFileName  :string = 'MemEditSysSet.ini';

  type
    //管理程序的设置
    tagSysSet = record
      FindBufferSize    : DWord ;//= 64*1024;       //查找缓冲大小
      ShowAddressCount  : DWord ;//= 100;           //最大显示数目
      ListMemMaxSize    : DWord ;//= 32*1024*1024;  //允许的最大动态内存;

      CallHotKey        : TShortCut;                //呼叫热键
      IsOnHotStopProc   : Boolean;                  //呼叫时是否暂停程序

      ModuleSuffix      : array[0..3]of String[255];//要过滤的后缀列表
      FiltrateSet       : TFiltrateSet;             //过滤设置
      IsModuleSuffixAsk : Boolean;                  //第一次查询时是否要询问内存过滤方案

      WatchTypeInt      : TSetOfValueType;          //整数查找监视类型
      WatchTypeFloat    : TSetOfValueType;          //浮点数查找监视类型
      WatchTypeString   : TSetOfValueType;          //字符串查找监视类型
      WatchTypeBlur     : TSetOfValueType;          //模糊查找监视类型
      IsFindAsk         : Boolean;                  //开始查找前是否询问查询类型
    end;

    //管理程序设置  类
    TMemEditSysSet = class
    public
      SysSet            : tagSysSet;
      ModuleSuffix      : string;             //过滤条件
    public
      procedure  SetDefultHot();
      procedure  SetDefultBuffer();
      procedure  SetDefultType();
      procedure  SetDefultFiltrate();

      procedure  GetDefultSysSet(var xSysSet :tagSysSet);
      procedure  ApplySysSet(var xDecSysSet: tagSysSet;const xSysSet :tagSysSet);
      procedure  ShowSysSet(const xSysSet :tagSysSet);
      function   GetUserSet(var xSysSet :tagSysSet):Boolean;

      procedure  LoadFromIniSet();
      procedure  SaveToIniSet();

      procedure  ShowFormToSetAll();
      function   ShowFormToSetType(const FindType: TFindType):Boolean;
      function   ShowFormToSetModuleSuffix(const ProcessId: THandle):Boolean;
      procedure  RefreshModuleList();

      function   ToolGetFileNameSuffix(const FileName : string):string; //得到文件后缀名
      function   ToolIsFileSuffixInFiltrateSuffix(const FileName : string):Boolean; //文件后缀名是否在过滤后缀名单中
      function   ToolIsFileSuffixInFiltrateFileName(const FileName : string):Boolean; //文件后缀名是否在过滤文件名单中

    public
      function   ModuleSuffixToStr(const xSet:tagSysSet):string;
      procedure  StrToModuleSuffix(const xModuleSuffix :string;var xSet:tagSysSet);

      constructor Create();
      destructor  Destroy(); override;

    private
      iniFileName  : string;
      frmSystemSet : TfrmSystemSet;            //对话框窗口
      frmMemSelect : TfrmMemSelect;            //模块选择窗口

    end;
    var MemEditSysSet:TMemEditSysSet=nil;
    

  type
    TMemEditMission = class;

    //进度条显示 回调函数 类型
    TCallBackShowProgressBar = function (const position : extended; const Mission : TMemEditMission):boolean;

    ////////////////////////////////////////////////////////////////////////////
    //                 TMemEditMission    查找任务 类                         //
    //            负责具体的查找过滤任务, 进程内存读写操作                    //
    ////////////////////////////////////////////////////////////////////////////
    TMemEditMission = class (TObject)
      private
        FName                         : string;                 //任务名称

        FValueAddressList             : TArrayTValueAddress;    //查到满足条件的地址
        FValueAddressListCount        : DWord;                  //   满足条件的地址的个数
        TempFValueAddressListLength   : Dword;                  //   查到满足条件的地址数组的当前长度
        FValueAddressListIntValue     : int64;                  //查找整数任务时保存要查找的整数值
        FValueAddressListFloatValue   : Extended;               //查找浮点数任务时保存要查找的浮点数值
        FValueAddressListStringValue  : String;                 //查找字符串任务时保存要查找的字符串值
        FFindIndex                    : dword;                  //第几次查找

        FFindType                     : TFindType;              //任务类型
        FIsStartBlur                  : Boolean;                //任务是否模糊查找

        FMemSizeSum                   : integer;                //被查找进程的有效内存大小

        FIsRunning: boolean;                                    //任务是否正在进行查找任务

        procedure SetName(const Value: string);

      private
        FProcHandle  : THandle;                                 //进程句柄
        FProcessesID : THandle;                                 //进程ID

      //工具函数
        procedure ToolGetAppMemoryInf(const xProcHandle: THandle); // 得到进程内存分布
        procedure ToolAssignMemoryInf();//规范化进程内存信息
        function  ToolFindInitialize(const xProcessesID :THandle):Boolean; //初始化进程信息
        // 第一次查找整数时对单块内存区进行查找
        //procedure ToolGetIntAddressList(const XProcHandle: THandle;const xIntValue:int64;
         //           MInf: TMemoryBasicInformation; xCallBack  : TCallBackShowProgressBar=nil;PositionFirst: Extended=0;PositionEnd : Extended=0);
        // 第一次查找浮点数时对单块内存区进行查找
        procedure ToolGetFloatAddressList(const xProcHandle: THandle;const xFloatValue:Extended;
                    MInf: TMemoryBasicInformation;const floatStr:string;const myValueType:TSetOfValueType;xCallBack  : TCallBackShowProgressBar=nil;PositionFirst: Extended=0;PositionEnd : Extended=0);
        // 第一次查找字符串时对单块内存区进行查找
        procedure ToolGetStringAddressList(const xProcHandle: THandle;const xStringValue:string;
                    MInf: TMemoryBasicInformation; xCallBack  : TCallBackShowProgressBar=nil;PositionFirst: Extended=0;PositionEnd : Extended=0);
        // 第n>=2次模糊查找，查找的是" + , - , = , ! "
        procedure ToolBlurAutoFind(const cCmd: char;
                         xCallBack: TCallBackShowProgressBar=nil);

        // 找到一个地址后增加地址列表
        procedure ToolAddressListAdd(var xValueAddress: TValueAddress);
        procedure ToolAddressListAddV(const PAddress:Pointer;const xValueType:TSetOfValueType);register;
        procedure ToolAddressListAddVTool(const PAddress:Pointer;const xValueType:TSetOfValueType);register;
        procedure ToolChangeFValueAddressListLength();

        // 工具：向一个进程写入整数
        function  ToolWriteIntValueToProc(const xProcessesID :THandle;const xAddress:Pointer;const xValue:int64;const xValueType:TValueType):Boolean;
        // 工具：向一个进程写入浮点数
        function  ToolWriteFloatValueToProc(const xProcessesID :THandle;const xAddress:Pointer;const xValue:extended;const xValueType:TValueType):Boolean;
        // 工具：向一个进程写入字符串
        function  ToolWriteStringValueToProc(const xProcessesID :THandle;const xAddress:Pointer;const xValue:string;const xValueType:TValueType):Boolean;
        // 模糊查找后，重新记录当前进程的内存数据
        procedure ToolAssignValue(); //载入值
        // 删除无效的地址记录
        procedure ToolDelNilAddress(); //去除多余的 Address=nil

      public
        FMemBlurValueCopy             : array of byte;  // 模糊查找时拷贝的进程内存数据
        FMemBlurValueCopyTrueCount    : integer;        //    其中的有效地址数量
        FMemBlurValueCopyType         : array of byte;  // 模糊查找时拷贝的进程内存数据对应的可能数据类型
        FMemoryInf                    : TarrayTMemoryBasicInformation; //当前任务中进程内存信息

        Hint0,Hint1                   : string;  //状态显示信息

        property  Name      : string read FName write SetName;
        property  FindIndex : dword read FFindIndex;
        property  ProcessesID : THandle read FProcessesID;
        property  FindType  : TFindType read FFindType;
        property  ValueAddressListCount : Dword read  FValueAddressListCount;
        property  ValueAddressList   : TArrayTValueAddress read FValueAddressList;
        property  MemSizeSum : integer read FMemSizeSum;
        property  IsRunning  : boolean read FIsRunning ;

        procedure Clear();    //清除任务
        // 查找整数任务：第一次查找
        function  FirstToFindInt(const xProcessesID :THandle;const xValue: int64;
                    const xSetofWatch:TSetOfValueType;xCallBack  : TCallBackShowProgressBar=nil):Boolean;
        // 查找浮点数任务：第一次查找
        function  FirstToFindFloat(const xProcessesID :THandle;const xValue: Extended;
                    const floatStr:string;const xSetofWatch:TSetOfValueType; xCallBack  : TCallBackShowProgressBar=nil):Boolean;
        // 查找字符串任务：第一次查找
        function  FirstToFindString(const xProcessesID :THandle;const xValue: string;
                    const xSetofWatch:TSetOfValueType; xCallBack  : TCallBackShowProgressBar=nil):Boolean;
        // 模糊查找任务：第一次查找
        function  FirstToFindBlur(const xProcessesID :THandle;
                    const xSetofWatch:TSetOfValueType; xCallBack  : TCallBackShowProgressBar=nil):Boolean;
        // 查找整数任务：第n>=2次查找
        function  ToFindInt(const xValue: int64; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
        // 查找浮点数任务：第n>=2次查找
        function  ToFindFloat(const xValue: Extended;const floatStr:string=''; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
        // 查找字符串任务：第n>=2次查找
        function  ToFindString(const xValue: string; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
        // 模糊查找任务：第n>=2次查找，查找的是一个具体的值 或>、<一个具体的值
        function  ToBlurFindAValue(var sCommand:string; xCallBack  : TCallBackShowProgressBar=nil):Boolean;  //低阶查找一个值
        // 模糊查找任务：第n>=2次查找，查找的是" + , - , = , ! "
        function  ToBlurFind(var Cmd:Char; xCallBack  : TCallBackShowProgressBar=nil):Boolean;
        // 每次模糊查找完成后，把查到的有效地址放入"地址列表"中，用于显示
        procedure OnFindBlurAfter();


        // 创建新的任务
        constructor Create(const xProcessesID:THandle;const msName: string=''); overload;
        // 删除任务
        destructor  Destroy; override;

      //工具函数
        function  ToolGetProcHandle(const xProcessesID :THandle) : THandle; // 由进程ID得到进程句柄
        procedure ToolCloseProcHandle(var xProcHandle: THandle); // 关闭进程句柄
        // 向进程写入值
        function  ToolWriteValueToProc(const xProcessesID :THandle;const xAddress:Pointer;
                    const xValue:string;const xValueType:TValueType):Boolean;overload;
        // 按进程ID从进程读出值
        function  ToolReadValueFromProcessesID(const xProcessesID :THandle;const xAddress:Pointer;
                    var xValue:string;const xValueType:TSetOfValueType):Boolean;overload;
        // 按进程句柄从进程读出所有可能值，并转化为字符串表示
        function  ToolReadValueFromProcHandle(const xProc : THandle;const xAddress:Pointer;
                    var xValue:string;const xValueType:TSetOfValueType):Boolean;overload;

    end;

  type
    ////////////////////////////////////////////////////////////////////////////
    //                 TMissionList   管理多个任务的列表 类                   //
    ////////////////////////////////////////////////////////////////////////////
    TMissionList = Class (TObject)
    private
      FList    : TList;
      function  GetCount():integer;
      function  GetListValue(const Index: integer): TMemEditMission;
      
    public
      property  Count : integer read GetCount;   //任务数目
      property  Items[const Index:integer] : TMemEditMission read GetListValue;
      function  AddMission(const xProcessesID:THandle;const msName: string=''):integer;
      procedure DelMission(const Index:integer);
      
    public
      constructor Create();
      destructor  Destroy(); override;
    end;


  type
    //被用户选择监视的变量的属性结构 用于文件保存和打开
    TAddressListSave = packed record
      MsExeName   : string [255];   //程序名称
      MsName      : string [100];   //变量对应的任务名称
      ValueName   : string [100];   //变量名称
      ValueAddress: Pointer;        //变量内存地址
      Value       : string [255];   //变量值
      ValueType   : TValueType;     //变量类型
      EditType    : TEditType;      //修改类型
      HotKey      : TShortCut;      //热键
      ProcessesID : THandle;        //任务进程ID
      IsWriteOk   : Boolean;        //读写状态
      ID          : DWord;          //变量被分配的唯一ID，用以标识该变量
    end;
    
  type
    //被用户选择监视的变量的属性结构
    TAddressList = class
      MsName      : string;         //变量对应的任务名称
      ValueName   : string;         //变量名称
      ValueAddress: Pointer;        //变量内存地址
      Value       : string;         //变量值
      ValueType   : TValueType;     //变量类型
      EditType    : TEditType;      //修改类型
      HotKey      : TShortCut;      //热键
      ProcessesID : THandle;        //任务进程ID
      IsWriteOk   : Boolean;        //读写状态
      ID          : DWord;          //变量被分配的唯一ID，用以标识该变量
      procedure Assign   (XValue:TAddressList); overload;        //拷贝函数
      procedure Assign   (XValue:TAddressListSave);overload;
      procedure AssignTo (var XValue:TAddressList); overload;    //传值函数
      procedure AssignTo (var XValue:TAddressListSave); overload;
    end;


  type
    ////////////////////////////////////////////////////////////////////////////
    //           TMemEditAddressList    用户变量监视管理类                    //
    //              用于管理被用户选择监视的变量列表                          //
    ////////////////////////////////////////////////////////////////////////////
    TMemEditAddressList = class (TObject)
    private
      FList: TList;                       //用以管理 变量列表
      FID  : DWord;                       //用于给变量分配唯一ID ,因为变量经常添加删除，
                                              //所以用序号标识变量不行，只能使用唯一ID的方式
      FMission  :TMemEditMission;         //利用TMission打开关闭进程

      function  GetCount: integer;
      function  GetValue(index: integer): TAddressList;
      procedure SetValue(index: integer; const xValue: TAddressList);
      procedure DelAddress(index: integer);

    public
      property  Count : integer read GetCount;   //变量数目
      property  Items[index: integer] : TAddressList read GetValue write SetValue; //变量数组
      function  AddAddress(AddressList: TAddressList;WriteItOnce :Boolean=false):integer; //增加变量
      procedure DelAddressByID(ID: DWord);   //按变量唯一ID标识删除变量
      procedure Clear();                     //清空变量列表

      procedure SetValueByID(const xValue: TAddressList);  //按变量唯一ID标识设置变量值
      function  WriteProcAValue(index: integer):Boolean;   //把序号对应的变量的值写入对应进程
      function  WriteProcAValueByID(ID: DWord):Boolean;    //把ID对应的变量的值写入对应进程
      procedure WriteProcAllValue();                       //所有变量写入
      procedure WriteProcValueAuto();                      //把变量修改类型为etAuto的值写入对应进程
      function  ReadProcAValue(index: integer):Boolean;    //把序号对应的变量从对应进程读出值
      function  ReadProcAValueByID(ID: DWord):Boolean;     //把ID对应的变量从对应进程读出值
      procedure ReadProcAllValue();                        //所有变量读出

      constructor Create();
      destructor  Destroy; override;

    end;

implementation
  uses UnitfrmMain;
    var
      SysInfo: TSystemInfo;

{ TMemEditMission }

procedure TMemEditMission.ToolAssignMemoryInf();
var
  i,Count  : integer;
  Sum      : integer;

  ModuleList :TModuleList;
  ModuleCount  : integer;

  procedure AssignModule();
  var
    i  : integer;
  begin

    ModuleCount:=0;
    for i:=0 to ModuleList.Count-1 do
    begin
      with ModuleList.Items[i] do
      begin
        if ((not MemEditSysSet.SysSet.IsModuleSuffixAsk) and (MemEditSysSet.ToolIsFileSuffixInFiltrateSuffix(szExePath)))
          or ((MemEditSysSet.SysSet.IsModuleSuffixAsk) and (MemEditSysSet.ToolIsFileSuffixInFiltrateFileName(szExePath)))
        then
        begin
          ModuleList.Items[ModuleCount]:=ModuleList.Items[i];
          inc(ModuleCount);
        end;

      end;
    end;
  end;

  function  IsDelModule(const ModuleAddress : pointer):boolean;
  var
    i      : integer;
  begin
    for i:=0 to ModuleCount-1 do
    begin
      with ModuleList.Items[i] do
      begin
        if (DWord(ModuleAddress)>=DWord(modBaseAddr))
        and (DWord(ModuleAddress)<(DWord(modBaseAddr)+Dword(modBaseSize))) then
        begin
          result:=true;
          exit;
        end;
      end;
    end;
    result:=false;
  end;

  //var  t0 : int64;
begin
  //功能
  //删除无效区域
  //合并连续区域
  //
  //t0:=CPUCycleCounter();
  ModuleList:=TModuleList.Create(self.FProcessesID);

  try
    AssignModule();

    Count:=0;
    for i:=0 to length(FMemoryInf)-1 do
    begin
      with FMemoryInf[i] do
      begin
        if    (State=MEM_COMMIT)                  //提交
          and (RegionSize<>0)
          and ( (Protect and                                            //可读写的条件
                  (PAGE_READWRITE or PAGE_EXECUTE_READWRITE or PAGE_WRITECOPY or PAGE_EXECUTE_WRITECOPY)
                  //WRITECOPY只对第一次查询有意义，如果数据发生写操作对应区域的属性就会变为READWRITE(系统将共享数据变为私有数据)
                )<>0 )
          //and (    //进程可分配区域系统限制
              //  (dword(BaseAddress)>=DWord(SysInfo.lpMinimumApplicationAddress))
              //and
              //  (dword(BaseAddress)+RegionSize<=Dword(SysInfo.lpMaximumApplicationAddress))
              //)
          and (
                not IsDelModule(BaseAddress) //按用户选择排除一些模块 (比如系统的动态连接库)
              )
          then
        begin     //保留该区域
          FMemoryInf[Count]:=FMemoryInf[i];
          inc(Count);
        end;
      end;//end with
    end;
    setlength(FMemoryInf,Count);

    // 合并连续区域 (真正的连续区域,物理上和逻辑上的连续,必须合并才更准确) ,
    //   以前只考虑了物理上的,而造成小的BUG,查了好久,才找到这里来
    Count:=0;
    for i:=1 to length(FMemoryInf)-1 do
    begin
      if (FMemoryInf[Count].BaseAddress=FMemoryInf[i].AllocationBase)//逻辑上
      and ((dword(FMemoryInf[Count].BaseAddress)+FMemoryInf[Count].RegionSize)
           =dword(FMemoryInf[i].BaseAddress))  //物理上连续
         then
      begin
        inc(FMemoryInf[Count].RegionSize,FMemoryInf[i].RegionSize);
      end
      else
      begin
        inc(Count);
        FMemoryInf[Count]:=FMemoryInf[i];
      end;
    end;
    setlength(FMemoryInf,Count);

    Sum:=0;
    for i:=0 to high(FMemoryInf) do
    begin
      inc(sum,FMemoryInf[i].RegionSize);
    end;
    self.FMemSizeSum:=Sum; //得到内存大小总和
    //messageBox(0,pchar(floattostr(Sum/1024.0/1024.0)),'',0);
  finally
    ModuleList.Free;
  end;
  //t0:=CPUCycleCounter()-t0;
  //messagebox(0,pchar(inttostr(t0)),'',0);
end;

function TMemEditMission.FirstToFindInt(const xProcessesID :THandle;const xValue: int64 ;
  const xSetofWatch:TSetOfValueType; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  i  : integer;
  PositionFirst,PositionEnd : Extended;
  myxIntValue : int64;
  myValueType  : TSetOfValueType;
  xIntValue: int64;
  ProcValue  : array of byte;
  LValue     : dword;
  LGetValue  : dword;
  FindBufferSize : integer;

  procedure  ToolGetIntAddressList(const xProcHandle: THandle;
    const xIntValue: int64; MInf: TMemoryBasicInformation; xCallBack  : TCallBackShowProgressBar=nil;PositionFirst: Extended=0;PositionEnd : Extended=0);
  var
    pProcValue : Pointer;
    PValue     : Pointer;
    xValueAddress: TValueAddress;
    i,j         : integer;
    TempDword   : dword;
    TempDwordH  : dword;
    TempDwordSelf   : dword;
    LValue     : integer;
    LGetValue  : DWord;
    IsGetBound : Boolean;
  begin

    for j:=0 to ((integer(Minf.RegionSize)-8-1) div (FindBufferSize)) do
    begin
      LValue:=integer(Minf.RegionSize)-j*FindBufferSize;
      if LValue>FindBufferSize+8 then
      begin
        LValue:=FindBufferSize+8;
        IsGetBound:=false;
      end
      else
        IsGetBound:=true;
      pProcValue:=Pointer(integer(MInf.BaseAddress)+j*FindBufferSize);
      PValue:=@ProcValue[0];
      if (LValue<=0) or ( not
         windows.ReadProcessMemory(xProcHandle,pProcValue,
         PValue,LValue,LGetValue)) then
      begin
        //messagebox(0,'','',0);
      end
      else
      begin
        if not xCallBack((PositionEnd-PositionFirst)*(j*integer(FindBufferSize))/Minf.RegionSize+PositionFirst,self) then
        begin
          exit;
        end;
        application.ProcessMessages;

        if LValue<>integer(LGetValue) then
          LValue:=LGetValue;

        if vtByte in myValueType then
        begin
            begin
              {//只测试Byte
              for i:=0 to LValue -8-1 do
              begin
                if Byte(myxIntValue)=PByte(PValue)^ then
                begin
                      ToolAddressListAddVTool(pProcValue,[vtByte,vtWord,vtDword,vtDDword])
                end;
                inc(dword(PValue));
                inc(dword(pProcValue));
              end;//}

              //{
              TempDword:=DWord(myxIntValue);
              TempDwordSelf:=DWord(self);
              asm  //只测试Byte  汇编 

                    push    eax
                    push    edx
                    push    ecx
                    push    ebx
                    push    edi
                    push    esi

                    mov     ecx,TempDword
                    mov     edi,PValue
                    mov     esi,pProcValue

                    mov     ebx,LValue
                    sub     ebx,9
                    test    ebx,ebx
                    jl      @EndFor
                    inc     ebx

                 @StartFor:
                    cmp     cl,[edi]
                    jnz     @IfElse

                      mov     ecx,$1E  //[vtByte,vtWord,vtDword,vtDDword]
                      mov     edx,esi
                      mov     eax,TempDwordSelf
                      call    TMemEditMission.ToolAddressListAddV
                      mov     ecx,TempDword

                   @IfElse:
                      inc     edi
                      inc     esi
                      dec     ebx
                      jnz     @StartFor
                 @EndFor:

                    mov     PValue,edi
                    mov     pProcValue,esi

                    pop     esi
                    pop     edi
                    pop     ebx
                    pop     ecx
                    pop     edx
                    pop     eax


              end;
              //}

              { //对所有可能类型全部测试
              xValueAddress.ValueType:=[vtByte];
              for i:=0 to LValue -8-1 do
              begin
                if Byte(myxIntValue)=PByte(PValue)^ then
                begin
                    TempDword:=PDWord(PValue)^;
                    xValueAddress.Address:=pProcValue;
                    if Word(myxIntValue)<>Word(TempDword) then
                      ToolAddressListAdd(xValueAddress)
                    else
                    begin
                      if Dword(myxIntValue)=TempDword then
                      begin
                        if  myxIntValue=PInt64(PValue)^ then
                          xValueAddress.ValueType:=[vtByte,vtWord,vtDword,vtDDword]
                        else
                          xValueAddress.ValueType:=[vtByte,vtWord,vtDWord]
                      end
                      else
                        xValueAddress.ValueType:=[vtByte,vtWord];
                      ToolAddressListAdd(xValueAddress);
                      xValueAddress.ValueType:=[vtByte];
                    end;
                end;
                inc(dword(PValue));
                inc(dword(pProcValue));
              end;//}

              if IsGetBound then
              begin
                for i:=max(0,LValue -8) to LValue -8 do
                begin
                  if Byte(myxIntValue)=PByte(PValue)^ then
                  begin
                    xValueAddress.ValueType:=[vtByte];
                    TempDword:=PDWord(PValue)^;
                    xValueAddress.Address:=pProcValue;
                    if Word(myxIntValue)<>Word(TempDword) then
                      ToolAddressListAdd(xValueAddress)
                    else
                    begin
                      if Dword(myxIntValue)=TempDword then
                      begin
                        if  myxIntValue=PInt64(PValue)^ then
                          xValueAddress.ValueType:=[vtByte,vtWord,vtDword,vtDDword]
                        else
                          xValueAddress.ValueType:=[vtByte,vtWord,vtDWord]
                      end
                      else
                        xValueAddress.ValueType:=[vtByte,vtWord];
                      ToolAddressListAdd(xValueAddress);
                      xValueAddress.ValueType:=[vtByte];
                    end;
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
                for i:=max(0,LValue -8+1) to LValue -4 do
                begin
                  if (Byte(myxIntValue))=PByte(PValue)^ then
                  begin
                      xValueAddress.Address:=pProcValue;
                      xValueAddress.ValueType:=[vtByte];
                      if Word(myxIntValue)=PWord(PValue)^ then
                      begin
                         if DWord(myxIntValue)=PDWord(PValue)^ then
                           xValueAddress.ValueType:=[vtByte,vtWord,vtDWord]
                         else
                           xValueAddress.ValueType:=[vtByte,vtWord];
                      end;
                      ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
                for i:=max(0,LValue -4+1) to LValue -2 do
                begin
                  xValueAddress.ValueType:=[vtByte];
                  if (Byte(myxIntValue))=PByte(PValue)^ then
                  begin
                      xValueAddress.Address:=pProcValue;
                      if Word(myxIntValue)=PWord(PValue)^ then
                        xValueAddress.ValueType:=[vtByte,vtWord];
                      ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
                for i:=max(0,LValue -2+1) to LValue -1 do
                begin
                  xValueAddress.ValueType:=[vtByte];
                  if (Byte(myxIntValue))=PByte(PValue)^ then
                  begin
                      xValueAddress.Address:=pProcValue;
                      ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
              end;
            end;

        end
        else if vtWord in myValueType then
        begin
            begin
              //{
              TempDword:=DWord(myxIntValue);
              TempDwordSelf:=DWord(self);
              asm  //只测试Word  汇编

                    push    eax
                    push    edx
                    push    ecx
                    push    ebx
                    push    edi
                    push    esi

                    mov     ecx,TempDword
                    mov     edi,PValue
                    mov     esi,pProcValue

                    mov     ebx,LValue
                    sub     ebx,9
                    test    ebx,ebx
                    jl      @EndFor
                    inc     ebx

                 @StartFor:
                    cmp     cx,[edi]
                    jnz     @IfElse

                      mov     ecx,$1C  //[vtWord,vtDword,vtDDword]
                      mov     edx,esi
                      mov     eax,TempDwordSelf
                      call    TMemEditMission.ToolAddressListAddV
                      mov     ecx,TempDword

                   @IfElse:
                    inc     edi
                    inc     esi
                    dec     ebx
                    jnz     @StartFor
                 @EndFor:

                    mov     PValue,edi
                    mov     pProcValue,esi

                    pop     esi
                    pop     edi
                    pop     ebx
                    pop     ecx
                    pop     edx
                    pop     eax

              end;
              //}

              { //对所有可能类型全部测试
              xValueAddress.ValueType:=[vtWord];
              for i:=0 to LValue -8-1 do
              begin
                TempDword:=PDWord(PValue)^;
                if Word(myxIntValue)=Word(TempDword) then
                begin
                    xValueAddress.Address:=pProcValue;
                    if DWord(myxIntValue)<>TempDword then
                      ToolAddressListAdd(xValueAddress)
                    else
                    begin
                      if  myxIntValue=PInt64(PValue)^ then
                        xValueAddress.ValueType:=[vtWord,vtDword,vtDDword]
                      else
                        xValueAddress.ValueType:=[vtWord,vtDword];
                      ToolAddressListAdd(xValueAddress);
                      xValueAddress.ValueType:=[vtWord];
                    end;
                end;
                inc(dword(PValue));
                inc(dword(pProcValue));
              end; //}

              if IsGetBound then
              begin
                for i:= LValue -8 to LValue-8 do
                begin
                  TempDword:=PDWord(PValue)^;
                  if Word(myxIntValue)=Word(TempDword) then
                  begin
                      xValueAddress.Address:=pProcValue;
                      if DWord(myxIntValue)<>TempDword then
                        ToolAddressListAdd(xValueAddress)
                      else
                      begin
                        if  myxIntValue=PInt64(PValue)^ then
                          xValueAddress.ValueType:=[vtWord,vtDword,vtDDword]
                        else
                          xValueAddress.ValueType:=[vtWord,vtDword];
                        ToolAddressListAdd(xValueAddress);
                        xValueAddress.ValueType:=[vtWord];
                      end;
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
                for i:= LValue -8+1 to LValue-4 do
                begin
                  xValueAddress.ValueType:=[vtWord];
                  if Word(myxIntValue)=PWord(PValue)^ then
                  begin
                      xValueAddress.Address:=pProcValue;
                      if Dword(myxIntValue)=PDword(PValue)^ then
                        xValueAddress.ValueType:=[vtWord,vtDword];
                      ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
                for i:= LValue -4+1 to LValue-2 do
                begin
                  xValueAddress.ValueType:=[vtWord];
                  if Word(myxIntValue)=PWord(PValue)^ then
                  begin
                      xValueAddress.Address:=pProcValue;
                      ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
              end;
            end;
        end
        else if vtDWord in myValueType then
        begin
            begin
              //{
              TempDword:=DWord(myxIntValue);
              TempDwordSelf:=DWord(self);
              asm  //只测试DWord  汇编

                    push    eax
                    push    edx
                    push    ecx
                    push    ebx
                    push    edi
                    push    esi

                    mov     ecx,TempDword
                    mov     edi,PValue
                    mov     esi,pProcValue

                    mov     ebx,LValue
                    sub     ebx,9
                    test    ebx,ebx
                    jl      @EndFor
                    inc     ebx

                 @StartFor:
                    cmp     ecx,[edi]
                    jnz     @IfElse

                      mov     ecx,$18  //[vtDword,vtDDword]
                      mov     edx,esi
                      mov     eax,TempDwordSelf
                      call    TMemEditMission.ToolAddressListAddV
                      mov     ecx,TempDword

                   @IfElse:
                    inc     edi
                    inc     esi
                    dec     ebx
                    jnz     @StartFor
                 @EndFor:

                    mov     PValue,edi
                    mov     pProcValue,esi

                    pop     esi
                    pop     edi
                    pop     ebx
                    pop     ecx
                    pop     edx
                    pop     eax

              end;
              //}

              { //对所有可能类型全部测试
              xValueAddress.ValueType:=[vtDWord];
              for i:=0 to LValue -8-1 do
              begin
                if Dword(myxIntValue)=Pdword(PValue)^ then
                begin
                  xValueAddress.Address:=pProcValue;
                  if myxIntValue=PInt64(PValue)^ then
                  begin
                    xValueAddress.ValueType:=[vtDWord,vtDDWord];
                    ToolAddressListAdd(xValueAddress);
                    xValueAddress.ValueType:=[vtDWord];
                  end
                  else
                    ToolAddressListAdd(xValueAddress)
                end;
                inc(dword(PValue));
                inc(dword(pProcValue));
              end; //}
              if IsGetBound then
              begin
                for i:=LValue -8 to LValue -8 do
                begin
                  if Dword(myxIntValue)=Pdword(PValue)^ then
                  begin
                    xValueAddress.Address:=pProcValue;
                    if myxIntValue=PInt64(PValue)^ then
                    begin
                      xValueAddress.ValueType:=[vtDWord,vtDDWord];
                      ToolAddressListAdd(xValueAddress);
                      xValueAddress.ValueType:=[vtDWord];
                    end
                    else
                      ToolAddressListAdd(xValueAddress)
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
                for i:=LValue -8+1 to LValue -4 do
                begin
                  if Dword(myxIntValue)=Pdword(PValue)^ then
                  begin
                    xValueAddress.Address:=pProcValue;
                    ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
              end;
            end;
        end
        else if vtDDWord in myValueType then
        begin
            begin
              //{
              TempDword:=DWord(myxIntValue);
              TempDwordH:=myxIntValue shr 32;
              TempDwordSelf:=DWord(self);
              asm  // 汇编

                    push    eax
                    push    edx
                    push    ecx
                    push    ebx
                    push    edi
                    push    esi

                    mov     ecx,TempDword
                    mov     edi,PValue
                    mov     esi,pProcValue

                    mov     ebx,LValue
                    sub     ebx,9
                    test    ebx,ebx
                    jl      @EndFor
                    inc     ebx
                    
                 @StartFor:
                    cmp     ecx,[edi]
                    jnz     @IfElse
                    mov     ecx,TempDwordH
                    cmp     ecx,[edi+$04]
                    mov     ecx,TempDword
                    jnz     @IfElse

                      mov     ecx,$10  //[vtDDword]
                      mov     edx,esi
                      mov     eax,TempDwordSelf
                      call    TMemEditMission.ToolAddressListAddV

                   @IfElse:
                    inc     edi
                    inc     esi
                    dec     ebx
                    jnz     @StartFor
                 @EndFor:

                    mov     PValue,edi
                    mov     pProcValue,esi

                    pop     esi
                    pop     edi
                    pop     ebx
                    pop     ecx
                    pop     edx
                    pop     eax

              end;
              //}

              {
              xValueAddress.ValueType:=[vtDDWord];
              for i:=0 to LValue -8-1 do
              begin
                if (myxIntValue)=Pint64(PValue)^ then
                begin
                  xValueAddress.Address:=pProcValue;
                  ToolAddressListAdd(xValueAddress);
                end;
                inc(dword(PValue));
                inc(dword(pProcValue));
              end;
              //}
              if IsGetBound then
              begin
                for i:=LValue -8 to LValue -8 do
                begin
                  if (myxIntValue)=Pint64(PValue)^ then
                  begin
                    xValueAddress.Address:=pProcValue;
                    ToolAddressListAdd(xValueAddress);
                  end;
                  inc(dword(PValue));
                  inc(dword(pProcValue));
                end;
              end;
            end;
        end;// end if

      end; //end read
    end;//end for j
  end;


begin


  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.Hint0:=' 正在进行第一次查找整数的任务 ';
    self.Hint1:='';

    self.FIsRunning:=true;
  end;

  self.FFindType:=ftInt;

  xIntValue:=xValue;
  if xIntValue>=0 then
  begin
    if xIntValue>=int64(1) shl 32 then
      myValueType:=[vtDDWord]
    else if xIntValue>=(1 shl 16) then
      myValueType:=[vtDWord,vtDDword]
    else if xIntValue>=(1 shl 8) then
      myValueType:=[vtWord,vtDWord,vtDDword]
    else
      myValueType:=[vtByte,vtWord,vtDWord,vtDDword];
  end
  else
  begin
    if xIntValue<-(int64(1) shl 31) then
      myValueType:=[vtDDWord]
    else if xIntValue<-(1 shl 15) then
      myValueType:=[vtDWord,vtDDWord]
    else if xIntValue<-(1 shl 7) then
      myValueType:=[vtWord,vtDWord,vtDDword]
    else
      myValueType:=[vtByte,vtWord,vtDWord,vtDDword];
  end;
  myValueType:=myValueType*xSetofWatch;
  if myValueType=[] then
  begin

    self.Hint0:=' 输入的整数数据不满足"整数监视类型"设定,请检查系统设置。 ';
    self.Hint1:='';

    self.FIsRunning:=false;
    result:=false;
    Application.MessageBox(PChar(self.Hint0),'警告',MB_ICONWARNING+MB_OK);
    exit;
  end;

  try

  if not self.ToolFindInitialize(xProcessesID) then
    result:=false
  else
  begin

    myxIntValue:=(xIntValue);
    //myxInt64Value:=xIntValue;
    FValueAddressListIntValue:=xIntValue;

    FindBufferSize:=MemEditSysSet.SysSet.FindBufferSize;
    setlength(ProcValue,FindBufferSize+256);  //查找缓冲

    self.FIsStartBlur:=false;
    self.FValueAddressListIntValue:=xValue;
    FValueAddressListCount:=0;
    setlength(self.FValueAddressList,0);
    TempFValueAddressListLength:=0;
    PositionFirst:=5.0;
    xCallBack(PositionFirst,self);

    try
      for i:=low(FMemoryInf) to high(FMemoryInf) do
      begin
        PositionEnd:=PositionFirst+95.0*(FMemoryInf[i].RegionSize)/self.FMemSizeSum;
        ToolGetIntAddressList(self.FProcHandle,xValue,FMemoryInf[i],xCallBack,PositionFirst,PositionEnd);
        PositionFirst:=PositionEnd;
        if not xCallBack(PositionFirst,self) then
        begin
          ToolCloseProcHandle(self.FProcHandle);
          self.FIsRunning:=false;
          result:=true;
          exit;
        end;
        Application.ProcessMessages;
      end;
    except
      application.MessageBox('  找到太多的地址了,内存溢出,查找任务结束!  ','出错',MB_ICONERROR+MB_OK);
    end;
    setlength(self.FValueAddressList,self.FValueAddressListCount);
    TempFValueAddressListLength:=self.FValueAddressListCount;
    if FValueAddressListCount>0 then
    begin
      // 检测确定前面部分(ShowAddressCount条)的真实类型
      for i:=0 to min(TempFValueAddressListLength,MemEditSysSet.SysSet.ShowAddressCount)-1 do
      begin
        with self.FValueAddressList[i] do
        begin
          ValueType:=ValueType*myValueType;
          if (vtDDWord in ValueType) then
          begin
            LValue:=8;
            windows.ReadProcessMemory(self.FProcHandle,Address,
                 @ProcValue[0],LValue,LGetValue);
            if Word(xValue)<>PWord(@ProcValue[0])^ then
              ValueType:=ValueType*[vtByte]
            else if Dword(xValue)<>PDword(@ProcValue[0])^ then
              ValueType:=ValueType*[vtByte,vtWord]
            else if xValue<>PInt64(@ProcValue[0])^ then
              ValueType:=ValueType*[vtByte,vtWord,vtDWord]
            else
              ValueType:=ValueType*[vtByte,vtWord,vtDWord,vtDDWord];
          end
          else if (vtDWord in ValueType) then
          begin
            LValue:=4;
            windows.ReadProcessMemory(self.FProcHandle,Address,
                 @ProcValue[0],LValue,LGetValue);
            if Word(xValue)<>PWord(@ProcValue[0])^ then
              ValueType:=ValueType*[vtByte]
            else if Dword(xValue)<>PDword(@ProcValue[0])^ then
              ValueType:=ValueType*[vtByte,vtWord]
            else
              ValueType:=ValueType*[vtByte,vtWord,vtDWord]
          end
          else if (vtWord in self.FValueAddressList[i].ValueType) then
          begin
            LValue:=2;
            windows.ReadProcessMemory(self.FProcHandle,self.FValueAddressList[i].Address,
                 @ProcValue[0],LValue,LGetValue);
            if Word(xValue)<>PWord(@ProcValue[0])^ then
              ValueType:=ValueType*[vtByte]
            else
              ValueType:=ValueType*[vtByte,vtWord];
          end
          else
            ValueType:=[vtByte];
        end;//end with
      end;

      self.FFindIndex:=1;
      result:=true;
    end
    else
      result:=false;
  end;
  finally
    //setlength(ProcValue,0);  //查找缓冲

    ToolCloseProcHandle(self.FProcHandle);
    self.FIsRunning:=false;
    self.Hint0:=' 第一次查找整数的任务完成 ';
    self.Hint1:='';
  end;
end;

function TMemEditMission.ToFindInt(const xValue: int64; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  Proc : int64;
  PAddIndex ,i: integer;
  L         : dword;
  PositionFirst : Extended;
  myValueType   : TSetofValueType;
  Ptemp         : pointer;
begin
  if self.FFindType<>ftInt then
  begin
    result:=false;
    exit;
  end;

  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.Hint0:=' 正在进行查找整数的任务 ';
    self.Hint1:='';
    self.FIsRunning:=true;
  end;

  try

  if not self.ToolFindInitialize(self.FProcHandle) then
  begin
    result:=false;
  end
  else
  begin

    //得到可能类型
    if xValue>=0 then
    begin
      if xValue>=int64(1) shl 32 then
        myValueType:=[vtDDWord]
      else if xValue>=(1 shl 16) then
        myValueType:=[vtDWord,vtDDWord]
      else if xValue>=(1 shl 8) then
        myValueType:=[vtWord,vtDWord,vtDDWord]
      else
        myValueType:=[vtByte,vtWord,vtDWord,vtDDWord];
    end
    else
    begin
      if xValue<-(int64(1) shl 31) then
        myValueType:=[vtDDWord]
      else if xValue<-(1 shl 15) then
        myValueType:=[vtDWord,vtDDWord]
      else if xValue<-(1 shl 7) then
        myValueType:=[vtWord,vtDWord,vtDDWord]
      else
        myValueType:=[vtByte,vtWord,vtDWord,vtDDWord];
    end;
    myValueType:=myValueType*MemEditSysSet.SysSet.WatchTypeInt;


    self.FIsStartBlur:=false;
    self.FValueAddressListIntValue:=xValue;
    PAddIndex:=0;
    for i:=low(self.FValueAddressList) to high(FValueAddressList) do
    begin
      if i and (1024*16-1)=0 then
      begin
        PositionFirst:=100.0*(i)/length(FValueAddressList);
        xCallBack(PositionFirst,self);
        Application.ProcessMessages;
      end;

      with  FValueAddressList[i] do
      begin
        ValueType:=ValueType*myValueType;
        if vtDDword in ValueType then
        begin
          if windows.ReadProcessMemory(self.FProcHandle,Address,pointer(@Proc),8,L) then
          begin
            if Byte(Proc)<>Byte(xValue) then
              Continue
            else if Word(Proc)<>Word(xValue) then
              ValueType:=ValueType*[vtByte]
            else if Dword(Proc)<>Dword(xValue) then
              ValueType:=ValueType*[vtByte,vtWord]
            else if (Proc)<>(xValue) then         
              ValueType:=ValueType*[vtByte,vtWord,vtDword];
          end;
        end
        else if vtDword in ValueType then
        begin
          if windows.ReadProcessMemory(self.FProcHandle,Address,pointer(@Proc),4,L) then
          begin
            if Byte(Proc)<>Byte(xValue) then
              Continue
            else if Word(Proc)<>Word(xValue) then
              ValueType:=ValueType*[vtByte]
            else if Dword(Proc)<>Dword(xValue) then
              ValueType:=ValueType*[vtByte,vtWord];
          end;
        end
        else if vtword in ValueType then
        begin
          if windows.ReadProcessMemory(self.FProcHandle,Address,pointer(@Proc),2,L) then
          begin
            if Byte(Proc)<>Byte(xValue) then
              Continue
            else if Word(Proc)<>Word(xValue) then
              ValueType:=ValueType*[vtByte];
          end;
        end
        else if vtByte in ValueType then
        begin
          if windows.ReadProcessMemory(self.FProcHandle,Address,pointer(@Proc),1,L) then
          begin
            if Byte(Proc)<>Byte(xValue) then
              Continue;
          end;
        end;

        if ValueType<>[] then
        begin
          //FValueAddressList[PAddIndex]:=FValueAddressList[i];
          Ptemp:=@FValueAddressList[PAddIndex];
          PDWord(Ptemp)^:=DWord(Address);
          PDWord(Dword(Ptemp)+4)^:=DWord(ValueType);
          inc(PAddIndex);
        end;


      end;  //end with  FValueAddressList[i]
    end; //end for
    setlength(FValueAddressList,PAddIndex);
    self.FValueAddressListCount:=PAddIndex;
    self.TempFValueAddressListLength:=PAddIndex;
    inc(self.FFindIndex);
    result:=true;
  end;
  
  finally
  ToolCloseProcHandle(self.FProcHandle);
  self.FIsRunning:=false;
  self.Hint0:=' 查找整数的任务完成 ';
  self.Hint1:='';
  end;
end;

procedure TMemEditMission.SetName(const Value: string);
begin
  FName := Value;
end;

procedure  TMemEditMission.ToolCloseProcHandle(var xProcHandle: THandle);
begin
  if xProcHandle<>0 then
  begin
    windows.CloseHandle(xProcHandle);
    xProcHandle:=0;
  end;
end;

procedure TMemEditMission.ToolGetAppMemoryInf(
  const xProcHandle: THandle);
var
  newP  : dword;
  MbInf : TMemoryBasicInformation;
  Count : integer;
  OldBasse : dword;
  Length   : integer;

begin
  //
  newP:=0;
  Count:=0;
  Length:=128;
  setlength(self.FMemoryInf,Length);
  OldBasse:=$FFFFFFFF;
  MbInf.BaseAddress:=nil;
  while newP<$FFFFFFFF-$FF do
  begin
     windows.VirtualQueryEx(xProcHandle,pointer(newP),MbInf,sizeof(_MEMORY_BASIC_INFORMATION));
     if (dword(MbInf.BaseAddress)<>OldBasse) and (MbInf.RegionSize>0) then
     begin
       OldBasse:=dword(MbInf.BaseAddress);
       newP:=dword(MbInf.BaseAddress)+MbInf.RegionSize+$F;
       if Count<Length then
       begin
         FMemoryInf[Count]:=MbInf;
         inc(Count);
       end
       else
       begin
         inc(Length,128);
         setlength(self.FMemoryInf,Length);
         FMemoryInf[Count]:=MbInf;
         inc(Count);
       end;
     end
     else
       break;
  end;
end;

function TMemEditMission.ToolGetProcHandle(const xProcessesID :THandle): THandle;
begin
  try
    result:=OpenProcess(PROCESS_VM_OPERATION or PROCESS_VM_READ
          or PROCESS_VM_WRITE or PROCESS_QUERY_INFORMATION,true, xProcessesID);
  except
    result:=0;
  end;
end;



function TMemEditMission.ToolFindInitialize(const xProcessesID :THandle): Boolean;
begin
  //
  self.ToolCloseProcHandle(self.FProcHandle);
  self.FProcHandle:=self.ToolGetProcHandle(self.FProcessesID);
  if self.FProcHandle=0 then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.ToolGetAppMemoryInf(self.FProcHandle);
    self.ToolAssignMemoryInf();
    result:=length(self.FMemoryInf)>0;
  end;
end;


procedure TMemEditMission.ToolChangeFValueAddressListLength;
begin
  //重新设置FValueAddressList的大小

  if TempFValueAddressListLength*8+8>=MemEditSysSet.SysSet.ListMemMaxSize then
  begin
    OutOfMemoryError();
    exit;
  end;
  TempFValueAddressListLength:=round((FValueAddressListCount*1.382))+1024*64;
  if TempFValueAddressListLength*8+102>=MemEditSysSet.SysSet.ListMemMaxSize then
    TempFValueAddressListLength:=(MemEditSysSet.SysSet.ListMemMaxSize) div (8);
  try
    setlength(FValueAddressList,TempFValueAddressListLength);
  except
    OutOfMemoryError();
  end;
end;

procedure TMemEditMission.ToolAddressListAdd(var xValueAddress: TValueAddress);
begin
  if FValueAddressListCount<TempFValueAddressListLength then
  begin
    FValueAddressList[FValueAddressListCount]:=xValueAddress;
    inc(FValueAddressListCount);
  end
  else
  begin
    self.ToolChangeFValueAddressListLength();
    FValueAddressList[FValueAddressListCount]:=xValueAddress;
    inc(FValueAddressListCount);
    application.ProcessMessages;
  end;
end;

procedure TMemEditMission.ToolAddressListAddVTool(const PAddress:Pointer;const xValueType:TSetOfValueType);register;
begin  //ToolAddressListAddV的辅助函数
  if FValueAddressListCount<TempFValueAddressListLength then
  begin
    with FValueAddressList[FValueAddressListCount] do
    begin
      Address:=PAddress;
      ValueType:=xValueType;
    end;
    inc(FValueAddressListCount);
  end
  else
  begin
    self.ToolChangeFValueAddressListLength();
    FValueAddressList[FValueAddressListCount].Address:=PAddress;
    FValueAddressList[FValueAddressListCount].ValueType:=xValueType;
    inc(FValueAddressListCount);
    application.ProcessMessages;
  end;
end;

procedure TMemEditMission.ToolAddressListAddV(const PAddress: Pointer;
  const xValueType: TSetOfValueType);register;
  {
begin
  if FValueAddressListCount<TempFValueAddressListLength then
  begin
    inc(FValueAddressListCount);
    with FValueAddressList[FValueAddressListCount] do
    begin
      Address:=PAddress;
      ValueType:=xValueType;
    end;
  end
  else
  begin
    ToolAddressListAddVTool(PAddress,xValueType);
  end;
  //}

  //改写为汇编
  asm
        push    ebx
        mov     ebx,[eax+FValueAddressListCount]
        cmp     ebx,[eax+TempFValueAddressListLength]
        jnb     @ElseIf

          push    esi
          inc     [eax+FValueAddressListCount]
          mov     esi,[eax+FValueAddressList]
          lea     ebx,[esi+ebx*8]
          mov     [ebx],edx
          mov     [ebx+4],ecx
          pop     esi
          pop     ebx
          ret

      @ElseIf:
        pop     ebx
        jmp     TMemEditMission.ToolAddressListAddVTool
  //}
end;

constructor TMemEditMission.Create(const xProcessesID : THandle;const msName: string='');
begin
  inherited Create;
  self.Clear;
  self.FProcessesID:=xProcessesID;
  self.FName:=msName;

end;

destructor TMemEditMission.Destroy;
begin
  self.Clear;
  self.ToolCloseProcHandle(self.FProcHandle);
  inherited;
end;

function TMemEditMission.ToolWriteValueToProc(const xProcessesID : THandle;
  const xAddress: Pointer; const xValue: string;
  const xValueType: TValueType): Boolean;
begin
    if xValuetype in [vtByte,vtWord,vtDWord,vtDDWord] then
      result:=self.ToolWriteIntValueToProc(xProcessesID,xAddress,strToInt64(xValue),xValuetype)
    else if xValuetype in [vtSingle,vtDouble,vtLDouble] then
      result:=self.ToolWriteFloatValueToProc(xProcessesID,xAddress,strToFloat(xValue),xValuetype)
    else if xValuetype in [vtAString,vtWString] then
      result:=self.ToolWriteStringValueToProc(xProcessesID,xAddress,xValue,xValuetype)
    else
      result:=false;
end;

function TMemEditMission.ToolWriteIntValueToProc(const xProcessesID :THandle;
  const xAddress: Pointer; const xValue: int64;
  const xValueType: TValueType): Boolean;
var
  ProcHandle    : THandle;
  PValue        : array [0..63] of byte;
  ValueCount,L  : DWord;
begin
  if not (xValueType in [vtByte,vtWord,vtDWord,vtDDWord]) then
  begin
    result:=false;
    exit;
  end;

  ProcHandle:=self.ToolGetProcHandle(xProcessesID);
  if ProcHandle<>0 then
  begin
    ValueCount:=0;
    if xValueType=vtByte then
    begin
      PByte(@PValue[0])^:=xValue;
      ValueCount:=1;
    end
    else if xValueType=vtWord then
    begin
      PWord(@PValue[0])^:=xValue;
      ValueCount:=2;
    end
    else if xValueType=vtDWord then
    begin
      PDWord(@PValue[0])^:=xValue;
      ValueCount:=4;
    end
    else if xValueType=vtDDWord then
    begin
      PInt64(@PValue[0])^:=xValue;
      ValueCount:=8;
    end;
    result:=windows.WriteProcessMemory(ProcHandle,xAddress,@PValue[0],ValueCount,L);
    windows.CloseHandle(ProcHandle);
  end
  else
    result:=false;
end;

function TMemEditMission.ToolWriteFloatValueToProc(const xProcessesID :THandle;
  const xAddress: Pointer; const xValue: extended;
  const xValueType: TValueType): Boolean;
var
  ProcHandle    : THandle;
  PValue        : array [0..16] of byte;
  ValueCount,L  : DWord;
begin
  if not (xValueType in [vtSingle,vtDouble,vtLDouble]) then
  begin
    result:=false;
    exit;
  end;

  ProcHandle:=self.ToolGetProcHandle(xProcessesID);
  if ProcHandle<>0 then
  begin
    ValueCount:=0;
    if xValueType=vtSingle then
    begin
      PSingle(@PValue[0])^:=xValue;
      ValueCount:=4;
    end
    else if xValueType=vtDouble then
    begin
      PDouble(@PValue[0])^:=xValue;
      ValueCount:=8;
    end
    else if xValueType=vtLDouble then
    begin
      PExtended(@PValue[0])^:=xValue;
      ValueCount:=10;
    end;
    result:=windows.WriteProcessMemory(ProcHandle,xAddress,@PValue[0],ValueCount,L);
    windows.CloseHandle(ProcHandle);
  end
  else
    result:=false;
end;

function TMemEditMission.ToolWriteStringValueToProc(const xProcessesID :THandle;
  const xAddress: Pointer; const xValue: string;
  const xValueType: TValueType): Boolean;
var
  ProcHandle    : THandle;
  PValue        : Pointer;
  AStr          : string;
  WStr          : WideString;
  ValueCount,L  : DWord;
begin
  if not (xValueType in [vtAstring,vtWString]) then
  begin
    result:=false;
    exit;
  end;

  ProcHandle:=self.ToolGetProcHandle(xProcessesID);
  if ProcHandle<>0 then
  begin
    ValueCount:=0;
    PValue:=nil;
    if xValueType=vtAstring then
    begin
      AStr:=xValue;
      PValue:=@AStr[1];
      ValueCount:=length(AStr);
    end
    else if xValueType=vtWString then
    begin
      WStr:=xValue;
      PValue:=@WStr[1];
      ValueCount:=length(WStr)*2;
    end;
    result:=windows.WriteProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L);
    windows.CloseHandle(ProcHandle);
  end
  else
    result:=false;
end;

function TMemEditMission.ToolReadValueFromProcessesID(const xProcessesID : THandle;
  const xAddress: Pointer; var xValue: string;
  const xValueType:TSetOfValueType): Boolean;
var
  ProcHandle    : THandle;
begin
  xValue:='';
  if (xValueType=[]) or (xValueType=[vtNoDefine]) then
  begin
    result:=false;
    exit;
  end;

  ProcHandle:=self.ToolGetProcHandle(xProcessesID);
  if ProcHandle<>0 then
  begin
    result:=self.ToolReadValueFromProcHandle(ProcHandle,xAddress,xValue,xValueType);
    windows.CloseHandle(ProcHandle);
  end
  else
    result:=false;
end;

function TMemEditMission.ToolReadValueFromProcHandle(const xProc: THandle;
  const xAddress: Pointer; var xValue: string;
  const xValueType: TSetOfValueType): Boolean;

var
  ProcHandle    : THandle;
  PValue        : Pointer;
  WStr          : WideString;
  ValueCount,L,i: DWord;
  oldValue      : string;
  tempString    : string;
  ValueList     : array [0..11] of  string;
  TempxArrayByteData    : array [0..256+10] of byte; //用作临时缓冲区

  Index         :integer;

  procedure ValueListAdd(sValue:String);
  var
    i  : integer;
  begin
    for i:=0 to Index-1 do
    begin
      if (ValueList[i]<>'') and (ValueList[i]=sValue) then
        exit;
    end;
    ValueList[Index]:=sValue;
    inc(index);
  end;

begin
  for i:=0 to high(ValueList) do
    ValueList[i]:='';
  Index:=0;

  oldValue:=xValue;
  xValue:='';
  ProcHandle:=xProc;
  PValue:=@TempxArrayByteData[0];
  try
    if vtByte in xValueType then
    begin
      ValueCount:=1;
      if windows.readProcessMemory(xProc,xAddress,PValue,ValueCount,L) then
      begin
        if Pshortint(PValue)^<0 then
          ValueListAdd(inttostr(PByte(PValue)^)+'('+inttostr(Pshortint(PValue)^)+')')
        else
          ValueListAdd(inttostr(PByte(PValue)^));
      end;
    end;
    if vtWord in xValueType then
    begin
      ValueCount:=2;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        if Psmallint(PValue)^<0 then
          ValueListAdd(inttostr(PWord(PValue)^)+'('+inttostr(Psmallint(PValue)^)+')')
        else
          ValueListAdd(inttostr(PWord(PValue)^));
      end;
    end;
    if vtDWord in xValueType then
    begin
      ValueCount:=4;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        if Pinteger(PValue)^<0 then
          ValueListAdd(inttostr(PDWord(PValue)^)+'('+inttostr(Pinteger(PValue)^)+')')
        else
          ValueListAdd(inttostr(PDWord(PValue)^));
      end;
    end;
    if vtDDWord in xValueType then
    begin
      ValueCount:=8;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        ValueListAdd(inttostr(PInt64(PValue)^));
      end;
    end;
    if vtSingle in xValueType then
    begin
      ValueCount:=4;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        if not IsNanFloat(PSingle(PValue)) then
        begin
          ValueListAdd(floattostr(PSingle(PValue)^));
        end;
      end;
    end;
    if vtDouble in xValueType then
    begin
      ValueCount:=8;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        if not IsNanFloat(PDouble(PValue)) then
        begin
          ValueListAdd(floattostr(PDouble(PValue)^));
        end;
      end;
    end;
    if vtLDouble in xValueType then
    begin
      ValueCount:=10;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        if not IsNanFloat(PExtended(PValue)) then
        begin
          ValueListAdd(floattostr(PExtended(PValue)^));
        end;
      end;
    end;
    if vtAString in xValueType then
    begin
      ValueCount:=256;//length(OldValue);
      TempxArrayByteData[ValueCount-2]:=0;TempxArrayByteData[ValueCount-1]:=0;
      TempxArrayByteData[ValueCount]:=0;
      TempxArrayByteData[ValueCount+1]:=0;TempxArrayByteData[ValueCount+2]:=0;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        tempString:=(PChar(PValue));
        ValueListAdd(tempString);
      end;
    end;
    if vtWString in xValueType then
    begin
      WStr:=OldValue;
      ValueCount:=256;//length(WStr)*2;
      TempxArrayByteData[ValueCount-2]:=0;TempxArrayByteData[ValueCount-1]:=0;
      TempxArrayByteData[ValueCount]:=0;
      TempxArrayByteData[ValueCount+1]:=0;TempxArrayByteData[ValueCount+2]:=0;
      if windows.readProcessMemory(ProcHandle,xAddress,PValue,ValueCount,L) then
      begin
        tempString:=PWChar(PValue);
        ValueListAdd(tempString);
      end;
    end;

    result:=true;
  finally
    xValue:='';
    for i:=0 to high(ValueList) do
    begin
      if ValueList[i]<>'' then
      begin
        if xValue<>'' then
          xValue:=xValue+' 或 '+ValueList[i]
        else
          xValue:=ValueList[i]; 
      end;
    end;
  end;
end;

function TMemEditMission.FirstToFindFloat(const xProcessesID :THandle;
  const xValue: Extended;const floatStr:string;const xSetofWatch:TSetOfValueType; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  i  : integer;
  PositionFirst,PositionEnd : Extended;
  myExtended : Extended;
  MyValueType  : TSetOfValueType;
 // FindBufferSize : integer;
begin

  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行第一次查找浮点数的任务 ';
    self.Hint1:='';
  end;

  self.FFindType:=ftFloat ;

  //FindBufferSize:=MemEditSysSet.SysSet.FindBufferSize;

  myExtended:= xValue;
  if myExtended=0.0 then
    MyValueType:=[vtSingle,vtDouble,vtLDouble]
  else if (abs(MyExtended)>MaxDouble) or (abs(MyExtended)<MinDouble) then
    MyValueType:=[vtLDouble]
  else if (abs(MyExtended)>MaxSingle) or (abs(MyExtended)<MinSingle) then
    MyValueType:=[vtDouble,vtLDouble]
  else
    MyValueType:=[vtSingle,vtDouble,vtLDouble];
  MyValueType:=MyValueType*MemEditSysSet.SysSet.WatchTypeFloat;
  if myValueType=[] then
  begin

    self.Hint0:=' 输入的浮点数数据不满足"浮点数监视类型"设定,请检查系统设置。 ';
    self.Hint1:='';

    self.FIsRunning:=false;
    result:=false;
    Application.MessageBox(PChar(self.Hint0),'警告',MB_ICONWARNING+MB_OK);
    exit;
  end;

  try

  if not self.ToolFindInitialize(xProcessesID) then
    result:=false
  else
  begin
    self.FIsStartBlur:=false;
    self.FValueAddressListFloatValue:=xValue;
    FValueAddressListCount:=0;
    setlength(self.FValueAddressList,0);
    self.TempFValueAddressListLength:=0;
    PositionFirst:=5.0;
    xCallBack(PositionFirst,self);
    Application.ProcessMessages;
    for i:=low(FMemoryInf) to high(FMemoryInf) do
    begin
      PositionEnd:=PositionFirst+95.0*(FMemoryInf[i].RegionSize)/self.FMemSizeSum;
      self.ToolGetFloatAddressList(self.FProcHandle,xValue,FMemoryInf[i],floatStr,myValueType,xCallBack,PositionFirst,PositionEnd);
      PositionFirst:=PositionEnd;
      if not xCallBack(PositionFirst,self) then
      begin
        ToolCloseProcHandle(self.FProcHandle);
        self.FIsRunning:=false;
        result:=true;
        exit;
      end;

      Application.ProcessMessages;
    end;

    setlength(self.FValueAddressList,self.FValueAddressListCount);
    self.TempFValueAddressListLength:=FValueAddressListCount;
    if FValueAddressListCount>0 then
    begin
      self.FFindIndex:=1;
      result:=true;
    end
    else
      result:=false;
  end;
  finally
  ToolCloseProcHandle(self.FProcHandle);
  self.FIsRunning:=false;
  self.Hint0:=' 第一次查找浮点数的任务完成 ';
  self.Hint1:='';
  end;
end;

procedure TMemEditMission.ToolGetFloatAddressList(
  const xProcHandle: THandle; const xFloatValue: Extended;
  MInf: TMemoryBasicInformation;const floatStr:string;
  const myValueType:TSetOfValueType;
  xCallBack  : TCallBackShowProgressBar=nil;
  PositionFirst: Extended=0;PositionEnd : Extended=0);
var
  ProcValue  : array of byte;
  pProcValue : Pointer;
  PValue     : Pointer;
  LValue     : dword;
  LGetValue  : dword;
  myExtended   : Extended;
  myDX         : Extended;
  i,j          : integer;
  FindBufferSize : DWORD;
  IsGetBound   : boolean;
begin

  FindBufferSize:=MemEditSysSet.SysSet.FindBufferSize;

  myExtended:=xFloatValue;
  
  if floatStr='' then
    myDX:=abs(xFloatValue*0.0001)
  else
    myDX:=GetFloatDX(floatStr);

  setlength(ProcValue,FindBufferSize+10+2);  //查找缓冲
  for j:=0 to ((Minf.RegionSize-10-1) div (FindBufferSize)) do
  begin
    LValue:=integer(Minf.RegionSize)-j*integer(FindBufferSize);
    if LValue>FindBufferSize+10 then
    begin
      LValue:=FindBufferSize+10;
      IsGetBound:=false;
    end
    else
      IsGetBound:=true;

    if (LValue<>0) and
       windows.ReadProcessMemory(xProcHandle,Pointer(Dword(MInf.BaseAddress)+Dword(j)*(FindBufferSize)),
       @ProcValue[0],LValue,LGetValue) then
    begin
      if not xCallBack((PositionEnd-PositionFirst)*(j*integer(FindBufferSize))/Minf.RegionSize+PositionFirst,self) then
      begin
        exit;
      end;
      application.ProcessMessages;

      PValue:=@ProcValue[0];
      pProcValue:=Pointer(Dword(MInf.BaseAddress)+Dword(j)*(FindBufferSize));
      LValue:=min(LGetValue,FindBufferSize);

        if xFloatValue=0.0 then
        begin
            for i:=0 to LValue -10 do
            begin
              if PByte(PValue)^=0 then
              begin
                if PDword(PValue)^=0 then
                begin
                  if  (vtSingle in MyValueType) then ToolAddressListAddV(pProcValue,[vtSingle]);
                  if PDword(Dword(PValue)+4)^=0 then
                  begin
                    if (vtdouble in MyValueType) then ToolAddressListAddV(pProcValue,[vtDouble]);
                    if (vtLdouble in MyValueType) and (Pword(Dword(PValue)+8)^=0) then
                      ToolAddressListAddV(pProcValue,[vtLDouble]);
                  end;
                end;
              end;
              inc(dword(PValue));
              inc(dword(pProcValue));
            end;
        end
        else //0以外的浮点数
        begin
        
          //一共有7种组合, 这里展开了3种作特殊处理
          if MyValueType = [vtSingle] then
          begin
            for i:=0 to LValue -10 do
            begin
              if (not IsNanFloat(PSingle(PValue))) and (IsFloatEQ(myExtended,PSingle(PValue)^,myDx)) then
              begin
                ToolAddressListAddV(pProcValue,[vtSingle]);
              end;
              inc(dword(PValue));
              inc(dword(pProcValue));
            end;
          end
          else if MyValueType= [vtDouble] then
          begin
            for i:=0 to LValue -10 do
            begin
              if (not IsNanFloat(PDouble(PValue))) and (IsFloatEQ(myExtended,PDouble(PValue)^,myDx)) then
              begin
                ToolAddressListAddV(pProcValue,[vtDouble]);
              end;
              inc(dword(PValue));
              inc(dword(pProcValue));
            end;
          end
          else if MyValueType = [vtLDouble] then
          begin
            for i:=0 to LValue -10 do
            begin
              if (not IsNanFloat(PExtended(PValue))) and (IsFloatEQ(myExtended,PExtended(PValue)^,myDx)) then
              begin
                ToolAddressListAddV(pProcValue,[vtLDouble]);
              end;
              inc(dword(PValue));
              inc(dword(pProcValue));
            end;
          end
          else //其它情况
          begin
            for i:=0 to LValue -10 do
            begin
              if (vtSingle in MyValueType) and (not IsNanFloat(PSingle(PValue))) and (IsFloatEQ(myExtended,PSingle(PValue)^,myDx)) then
              begin
                ToolAddressListAddV(pProcValue,[vtSingle]);
              end;
              if (vtDouble in MyValueType) and  (not IsNanFloat(PDouble(PValue))) and (IsFloatEQ(myExtended,PDouble(PValue)^,myDx)) then
              begin
                ToolAddressListAddV(pProcValue,[vtDouble]);
              end;
              if (vtLDouble in MyValueType) and  (not IsNanFloat(PExtended(PValue))) and (IsFloatEQ(myExtended,PExtended(PValue)^,myDx)) then
              begin
                ToolAddressListAddV(pProcValue,[vtLDouble]);
              end;
              inc(dword(PValue));
              inc(dword(pProcValue));
            end;
          end;

        end;

        if (IsGetBound) then
        begin
          for i:=LValue -10+1 to LValue-8 do
          begin
            if (vtSingle in MyValueType) and  (not IsNanFloat(PSingle(PValue))) and (IsFloatEQ(myExtended,PSingle(PValue)^,myDx)) then
            begin
              ToolAddressListAddV(pProcValue,[vtSingle]);
            end;
            if (vtDouble in MyValueType) and  (not IsNanFloat(PDouble(PValue))) and (IsFloatEQ(myExtended,PDouble(PValue)^,myDx)) then
            begin
              ToolAddressListAddV(pProcValue,[vtDouble]);
            end;
            inc(dword(PValue));
            inc(dword(pProcValue));
          end;
          for i:=LValue -8+1 to LValue-4 do
          begin
            if (vtSingle in MyValueType) and  (not IsNanFloat(PSingle(PValue))) and (IsFloatEQ(myExtended,PSingle(PValue)^,myDx)) then
            begin
              ToolAddressListAddV(pProcValue,[vtSingle]);
            end;
            inc(dword(PValue));
            inc(dword(pProcValue));
          end;
       end;
    end;  // end read
  end; //end for j
end;


procedure TMemEditMission.ToolGetStringAddressList(
  const xProcHandle: THandle; const xStringValue: string;
  MInf: TMemoryBasicInformation; xCallBack  : TCallBackShowProgressBar=nil;
  PositionFirst: Extended=0;PositionEnd : Extended=0);
var
  ProcValue  : array of byte;
  pProcValue : Pointer;
  PValue     : Pointer;
  LValue     : dword;
  LGetValue  : dword;
  i,j          : integer;
  xWStrValue   : WideString;
  xChar        : byte;
  xACount,xWCount      : integer;
  xWChar       : word;
  FindBufferSize : DWORD;
  IsGetBound   : boolean;
begin

  FindBufferSize := MemEditSysSet.SysSet.FindBufferSize;

  //FValueAddressListCount;
  LValue:=Minf.RegionSize;
  setlength(ProcValue,LValue);
  xWStrValue:=xStringValue;
  xChar:=byte(xStringValue[1]);
  xWChar:=PWord(@xWStrValue[1])^;
  xACount:=length(xStringValue);
  xWCount:=length(xWStrValue);


  setlength(ProcValue,FindBufferSize+256+10);  //查找缓冲
  for j:=0 to ((Minf.RegionSize-256-1) div (FindBufferSize)) do
  begin
    LValue:=integer(Minf.RegionSize)-j*integer(FindBufferSize);
    if LValue>FindBufferSize+256 then
    begin
      LValue:=FindBufferSize+256;
      IsGetBound:=false;
    end
    else
      IsGetBound:=true;

    if (LValue<>0) and
       windows.ReadProcessMemory(xProcHandle,Pointer(Dword(MInf.BaseAddress)+Dword(j)*(FindBufferSize)),
       @ProcValue[0],LValue,LGetValue) then
    begin
      if not xCallBack((PositionEnd-PositionFirst)*(j*integer(FindBufferSize))/Minf.RegionSize+PositionFirst,self) then
      begin
        exit;
      end;
      application.ProcessMessages;

      PValue:=@ProcValue[0];
      pProcValue:=Pointer(Dword(MInf.BaseAddress)+Dword(j)*(FindBufferSize));
      LValue:=min(LGetValue,FindBufferSize);

      i:=0;

      if (vtAString in MemEditSysSet.SysSet.WatchTypeString)
        and (vtWString in MemEditSysSet.SysSet.WatchTypeString)  then
      begin
        while i<integer(LValue)-256 do
        begin
          if PWord(PValue)^=Word(xWChar) then
          begin
            if IsDataEQW(PValue,@xWStrValue[1],xWCount) then
            begin
              ToolAddressListAddV(pProcValue,[vtWString]);
            end;
          end;
          if Pbyte(PValue)^=BYTE(xChar) then
          begin
            if IsDataEQ(PValue,@xStringValue[1],xACount) then
            begin
              ToolAddressListAddV(pProcValue,[vtAString]);
            end;
          end;
          inc(dword(PValue));
          inc(dword(pProcValue));
          inc(i);
        end; //end while
      end
      else if (vtAString in MemEditSysSet.SysSet.WatchTypeString)  then
      begin
        while i<integer(LValue)-256 do
        begin
          if Pbyte(PValue)^=BYTE(xChar) then
          begin
            if IsDataEQ(PValue,@xStringValue[1],xACount) then
            begin
              ToolAddressListAddV(pProcValue,[vtAString]);
            end;
          end;
          inc(dword(PValue));
          inc(dword(pProcValue));
          inc(i);
        end; //end while
      end
      else if (vtWString in MemEditSysSet.SysSet.WatchTypeString)  then
      begin
        while i<integer(LValue)-256 do
        begin
          if PWord(PValue)^=Word(xWChar) then
          begin
            if IsDataEQW(PValue,@xWStrValue[1],xWCount) then
            begin
              ToolAddressListAddV(pProcValue,[vtWString]);
            end;
          end;
          inc(dword(PValue));
          inc(dword(pProcValue));
          inc(i);
        end; //end while
      end;

      if (IsGetBound) then
      begin
        i:=integer(LValue)-256;
        while i<integer(LValue)-min(xACount,xWCount*2) do
        begin
          if (vtAString in MemEditSysSet.SysSet.WatchTypeString) and
             (Pbyte(PValue)^=BYTE(xChar)) then
          begin
            if (i<(integer(LValue)-xACount))
              and IsDataEQ(PValue,@xStringValue[1],xACount) then
            begin
              ToolAddressListAddV(pProcValue,[vtAString]);
            end;
          end;
          if  (vtWString in MemEditSysSet.SysSet.WatchTypeString) and
            (PWord(PValue)^=Word(xWChar)) then
          begin
            if (i<(integer(LValue)-xWCount*2))
              and IsDataEQW(PValue,@xWStrValue[1],xWCount) then
            begin
              ToolAddressListAddV(pProcValue,[vtWString]);
            end;
          end;
          inc(dword(PValue));
          inc(dword(pProcValue));
          inc(i);
        end; //end while
      end;

    end;  //end read
  end; //end for j
end;

function TMemEditMission.ToFindFloat(const xValue: Extended;const floatStr:string=''; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  Proc : array [0..9] of byte;
  PValue : Pointer;
  PTemp  : pointer;
  PAddIndex,i: integer;
  LOld,L       : dword;
  myExtended,myDx  : Extended;
  PositionFirst : Extended;
begin
  if self.FFindType<>ftFloat then
  begin
    result:=false;
    exit;
  end;


  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行查找浮点数的任务 ';
    self.Hint1:='';
  end;

  try

  if not self.ToolFindInitialize(self.FProcessesID) then
  begin
    result:=false;
  end
  else
  begin
    self.FIsStartBlur:=false;
    self.FValueAddressListFloatValue:=xValue;
    myExtended:=xValue;
    if floatStr='' then
      myDX:=abs(xValue*0.0001)
    else
      myDX:=GetFloatDX(floatStr);
    PValue:= @Proc[0];
    LOld:=0;

    PAddIndex:=0;
    for i:=low(self.FValueAddressList) to high(FValueAddressList) do
    begin
      if i and (1024*8-1)=0 then
      begin
        PositionFirst:=100.0*(i)/length(FValueAddressList);
        if not xCallBack(PositionFirst,self) then
        begin
          ToolCloseProcHandle(self.FProcHandle);
          self.FIsRunning:=false;
          result:=true;
          exit;
        end;
        Application.ProcessMessages;
      end;

      with  FValueAddressList[i] do
      begin
        if vtSingle in ValueType then   //只有一种可能
          LOld:=4
        else if vtDouble in ValueType then
          LOld:=8
        else if vtLDouble in ValueType then
          LOld:=10
        else
          continue;

        if windows.ReadProcessMemory(self.FProcHandle,Address,PValue,LOld,L) then
        begin

          case LOld of
            4: if (IsNanFloat(PSingle(PValue))) or (not IsFloatEQ(myExtended,PSingle(PValue)^,myDx)) then
                 continue;
            8: if (IsNanFloat(PDouble(PValue))) or (not IsFloatEQ(myExtended,PDouble(PValue)^,myDx)) then
                 continue;
            else
               if (IsNanFloat(PExtended(PValue))) or (not IsFloatEQ(myExtended,PExtended(PValue)^,myDx)) then
                 continue;
          end;
          //FValueAddressList[PAddIndex]:=FValueAddressList[i];
          PTemp:=@FValueAddressList[PAddIndex];
          PDword(PTemp)^:=Dword(Address);
          PDword(Dword(PTemp)+4)^:=Dword(ValueType);
          inc(PAddIndex);
        end;

      end;//end with FValueAddressList[i]

    end;
    setlength(FValueAddressList,PAddIndex);
    self.FValueAddressListCount:=PAddIndex;
    self.TempFValueAddressListLength:=PAddIndex;
    inc(self.FFindIndex);
    result:=true;
  end;

  finally
  ToolCloseProcHandle(self.FProcHandle);
  self.FIsRunning:=false;
  self.Hint0:=' 查找浮点数的任务完成 ';
  self.Hint1:='';
  end;
end;

function TMemEditMission.FirstToFindString(const xProcessesID :THandle;const xValue: string;
   const xSetofWatch:TSetOfValueType;xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  i  : integer;
  PositionFirst,PositionEnd : Extended;
begin

  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行第一次查找字符串的任务 ';
    self.Hint1:='';
  end;

  self.FFindType:=ftString;

  try

  if not self.ToolFindInitialize(xProcessesID) then
    result:=false
  else
  begin
    self.FIsStartBlur:=false;
    self.FValueAddressListStringValue:=xValue;
    FValueAddressListCount:=0;
    setlength(self.FValueAddressList,0);
    self.TempFValueAddressListLength:=0;
    PositionFirst:=5.0;
    xCallBack(PositionFirst,self);
    Application.ProcessMessages;
    for i:=low(FMemoryInf) to high(FMemoryInf) do
    begin
      PositionEnd:=PositionFirst+95.0*(FMemoryInf[i].RegionSize)/self.FMemSizeSum;
      self.ToolGetStringAddressList(self.FProcHandle,xValue,FMemoryInf[i],xCallBack,PositionFirst,PositionEnd);
      PositionFirst:=PositionEnd;
      if not xCallBack(PositionFirst,self) then
      begin
        ToolCloseProcHandle(self.FProcHandle);
        self.FIsRunning:=false;
        result:=true;
        exit;
      end;
      Application.ProcessMessages;
    end;

    setlength(self.FValueAddressList,self.FValueAddressListCount);
    self.TempFValueAddressListLength:=FValueAddressListCount;
    if FValueAddressListCount>0 then
    begin
      self.FFindIndex:=1;
      result:=true;
    end
    else
      result:=false;
  end;

  finally
  ToolCloseProcHandle(self.FProcHandle);
  self.FIsRunning:=false;
  self.Hint0:=' 第一次查找字符串的任务完成 ';
  self.Hint1:='';
  end;
end;

function TMemEditMission.ToFindString(const xValue: string; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  Proc : array [0..256] of byte;
  PValue : Pointer;
  PxAValue : Pointer;
  PxWValue : Pointer;
  xWValue  : WideString;
  PAddIndex,i: integer;
  LOld,L  : dword;
  LengthAStr,LengthWStr  : integer;
  PositionFirst : Extended;
begin
  if self.FFindType<>ftString then
  begin
    result:=false;
    exit;
  end;

  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行查找字符串的任务 ';
    self.Hint1:='';
  end;

  try

  if not self.ToolFindInitialize(self.FProcessesID) then
  begin
    result:=false;
  end
  else
  begin
    self.FIsStartBlur:=false;
    self.FValueAddressListStringValue:=xValue;

    PValue:= @Proc[0];
    PxAValue:=@xValue[1];
    xWValue:=xValue;
    PxWValue:=@xWValue[1];
    PAddIndex:=0;
    LengthAStr:=length(xValue);
    LengthWStr:=length(xWValue);
    LOld:=max(LengthAStr,LengthWStr*2);
    for i:=low(self.FValueAddressList) to high(FValueAddressList) do
    begin
      if i and (1024*8-1)=0 then
      begin
        PositionFirst:=100.0*(i)/length(FValueAddressList);
        if not xCallBack(PositionFirst,self) then
        begin
          ToolCloseProcHandle(self.FProcHandle);
          self.FIsRunning:=false;
          result:=true;
          exit;
        end;
        Application.ProcessMessages;
      end;
      
      with  FValueAddressList[i] do
      begin
        if windows.ReadProcessMemory(self.FProcHandle,Address,PValue,LOld,L) then
        begin
          if vtAString in ValueType then
          begin
            if (integer(L)<LengthAStr) or (not IsDataEQ(PxAValue,PValue,LengthAStr)) then
              exclude(ValueType,vtAString);
          end;
          if vtWString in ValueType then
          begin
            if (integer(L)<LengthWStr*2) or (not IsDataEQW(PxWValue,PValue,LengthWStr)) then
              exclude(ValueType,vtWString);
          end;

          if ValueType<>[] then
          begin
            FValueAddressList[PAddIndex]:=FValueAddressList[i];
            inc(PAddIndex);
          end;
        end;
      end;//end with FValueAddressList[i]
    end;
    setlength(FValueAddressList,PAddIndex);
    self.FValueAddressListCount:=PAddIndex;
    self.TempFValueAddressListLength:=FValueAddressListCount;
    inc(self.FFindIndex);
    result:=true;
  end;

  finally
  ToolCloseProcHandle(self.FProcHandle);
  self.FIsRunning:=false;
  self.Hint0:=' 查找字符串的任务完成 ';
  self.Hint1:='';
  end;

end;

procedure TMemEditMission.Clear;
begin
  self.FFindType:=ftNoDefine;
  setlength(self.FValueAddressList,0);
  self.TempFValueAddressListLength:=0;
  self.FValueAddressListIntValue:=0;
  self.FValueAddressListFloatValue:=0;
  self.FValueAddressListStringValue:='';
  self.FValueAddressListCount:=0;
  setlength(self.FMemoryInf,0);
  self.FMemSizeSum:=0;
  self.FFindIndex:=0;
  self.FIsStartBlur:=false;
  setlength(FMemBlurValueCopy,0);
  setlength(FMemBlurValueCopyType,0);
  self.FMemBlurValueCopyTrueCount:=0;
  self.FIsRunning:=false;
  self.ToolCloseProcHandle(self.FProcHandle);
  self.Hint0:=' 任务处于待命状态 ';
  self.Hint1:='';
end;

function TMemEditMission.ToBlurFind(var Cmd: Char; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
begin
  if (not (Cmd in['?','+','-','=','!']))
  or (not (self.FFindType=ftBlur))  then
  begin
    result:=false;
    exit;
  end;

  if not (self.FIsStartBlur) then
  begin
    if self.FFindType=ftBlur then
    begin
      result:=false;
      exit;
    end
    else
      Cmd:='?';
  end;

  
  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行模糊查找的任务 ';
    self.Hint1:='';
  end;

  try

  inc(self.FFindIndex);

  case Cmd of
      '?':
        begin
          self.ToolAssignValue;
          self.FIsStartBlur:=true;
          result:=true;
        end;
      else
        begin
          self.ToolBlurAutoFind(Cmd, xCallBack);
          self.ToolAssignValue;
          result:=true;
        end;
  end;
  finally
  self.FIsRunning:=false;
  self.Hint0:=' 模糊查找任务完成 ';
  self.Hint1:='';
  end;
end;

procedure TMemEditMission.ToolAssignValue;
var
  i,j: integer;
  readCount : integer;
  L  : DWord;
  pcopy : integer;
begin
  self.FProcHandle:=self.ToolGetProcHandle(self.FProcessesID);

  if self.FFindType=ftBlur then
  begin
    pcopy:=0;
    for  i:=0 to length(self.FMemoryInf)-1 do
    begin
      readCount:=self.FMemoryInf[i].RegionSize;
      if windows.ReadProcessMemory(self.FProcHandle,self.FMemoryInf[i].BaseAddress,
        @FMemBlurValueCopy[pcopy],readCount,L) then
      begin
        //
      end
      else
      begin
        for j:=0 to ReadCount-1 do
          FMemBlurValueCopyType[pCopy+j]:=0; //01111111
      end;
      inc(pCopy,readCount);
    end;
    OnFindBlurAfter();
  end;
  self.ToolCloseProcHandle(self.FProcHandle);
  self.ToolDelNilAddress;
end;

procedure TMemEditMission.ToolDelNilAddress;
var
  i,L : integer;
begin
  //del nil address;
  if self.FFindType in [ftInt,ftFloat] then
  begin
    L:=0;
    for i:=0 to length(FValueAddressList)-1 do
    begin
      if FValueAddressList[i].Address<>nil then
      begin
        FValueAddressList[L]:=FValueAddressList[i];
        inc(L);
      end;
    end;
    setlength(FValueAddressList,L);
    self.TempFValueAddressListLength:=L;
    self.FValueAddressListCount:=L;
  end;
end;

procedure TMemEditMission.ToolBlurAutoFind(const cCmd: char; xCallBack  : TCallBackShowProgressBar=nil);
var
  i,j  : integer;
  readCount : integer;
  L  : DWord;
  Pdata,PCopy,PType: Pointer;
  bCopy : BYTE;
  TempData : array of Byte;
  PositionFirst : Extended;

begin
  if self.FFindType=ftBlur then
  begin
    inc(self.FFindIndex);
    self.FProcHandle:=self.ToolGetProcHandle(self.FProcessesID);
    readCount:=0;
    FMemBlurValueCopyTrueCount:=0;
    PositionFirst:=5.0;
    xCallBack(PositionFirst,self);
    Application.ProcessMessages;
    for i:= 0 to length(self.FMemoryInf)-1 do
    begin
      setlength(TempData,self.FMemoryInf[i].RegionSize);
      PositionFirst:=PositionFirst+95.0*(FMemoryInf[i].RegionSize)/self.FMemSizeSum;
      if not xCallBack(PositionFirst,self) then
      begin
        ToolCloseProcHandle(self.FProcHandle);
        self.FIsRunning:=false;
        exit;
      end;
      Application.ProcessMessages;
      if windows.ReadProcessMemory(self.FProcHandle,self.FMemoryInf[i].BaseAddress,
        @TempData[0],self.FMemoryInf[i].RegionSize,L) then
      begin
        PData:=@TempData[0];
        PCopy:=@self.FMemBlurValueCopy[readCount];
        PType:=@self.FMemBlurValueCopyType[readCount];
        if cCmd='+' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            if BCopy<>0 then
            begin
              if (BCopy and ftBlurByte) <>0 then
              begin
                if PByte(PData)^<=PByte(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurByte);
              end;
              if (BCopy and ftBlurWord) <>0 then
              begin
                if PWord(PData)^<=PWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurWord);
              end;
              if (BCopy and ftBlurDWord) <>0 then
              begin
                if PDWord(PData)^<=PDWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDWord);
              end;
              if (BCopy and ftBlurDDWord) <>0 then
              begin
                if Pint64(PData)^<=Pint64(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDDWord);
              end;
              
              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(Psingle(PCopy))
                  or (Psingle(PData)^<=PSingle(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (PDouble(PData)^<=PDouble(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (PExtended(PData)^<=PExtended(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end
        else if cCmd='-' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            if BCopy<>0 then
            begin
              if (BCopy and ftBlurByte) <>0 then
              begin
                if PByte(PData)^>=PByte(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurByte);
              end;
              if (BCopy and ftBlurWord) <>0 then
              begin
                if PWord(PData)^>=PWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurWord);
              end;
              if (BCopy and ftBlurDWord) <>0 then
              begin
                if PDWord(PData)^>=PDWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDWord);
              end;
              if (BCopy and ftBlurDDWord) <>0 then
              begin
                if Pint64(PData)^>=Pint64(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDDWord);
              end;
              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(Psingle(PCopy))
                  or (Psingle(PData)^>=PSingle(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (PDouble(PData)^>=PDouble(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (PExtended(PData)^>=PExtended(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end
        else if cCmd='=' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            if BCopy<>0 then
            begin
              if (BCopy and ftBlurByte) <>0 then
              begin
                if PByte(PData)^<>PByte(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurByte);
              end;
              if (BCopy and ftBlurWord) <>0 then
              begin
                if PWord(PData)^<>PWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurWord);
              end;
              if (BCopy and ftBlurDWord) <>0 then
              begin
                if PDWord(PData)^<>PDWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDWord);
              end;
              if (BCopy and ftBlurDDWord) <>0 then
              begin
                if Pint64(PData)^<>Pint64(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDDWord);
              end;
              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(Psingle(PCopy))
                  or (Psingle(PData)^<>PSingle(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (PDouble(PData)^<>PDouble(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (PExtended(PData)^<>PExtended(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end
        else if cCmd='!' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            if BCopy<>0 then
            begin
              if (BCopy and ftBlurByte) <>0 then
              begin
                if PByte(PData)^=PByte(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurByte);
              end;
              if (BCopy and ftBlurWord) <>0 then
              begin
                if PWord(PData)^=PWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurWord);
              end;
              if (BCopy and ftBlurDWord) <>0 then
              begin
                if PDWord(PData)^=PDWord(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDWord);
              end;
              if (BCopy and ftBlurDDWord) <>0 then
              begin
                if Pint64(PData)^=Pint64(PCopy)^ then
                  BCopy:=BCopy and (not ftBlurDDWord);
              end;
              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(Psingle(PCopy))
                  or (Psingle(PData)^=PSingle(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (PDouble(PData)^=PDouble(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (PExtended(PData)^=PExtended(PCopy)^) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end;//end if cCmd=...

      end
      else
      begin
        PType:=@self.FMemBlurValueCopyType[readCount];
        for j:=0 to self.FMemoryInf[i].RegionSize-1 do
        begin
          PByte(PType)^:=0;
          inc(DWord(PType));
        end;
      end;
      inc(readCount,self.FMemoryInf[i].RegionSize);
    end;
    self.ToolCloseProcHandle(self.FProcHandle);
  end;
end;


function TMemEditMission.FirstToFindBlur(const xProcessesID :THandle;const xSetofWatch:TSetOfValueType; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  pcopy,i,j,readCount : integer;
  L : DWord;
  watch : byte;
  Temp    :Dword;
  PositionFirst : Extended;
  xWatch  :Byte;
begin

  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行模糊查找初始化任务 ';
    self.Hint1:='';
  end;


  xWatch:=0;
  if vtByte in xSetofWatch then
    xWatch:=xWatch or ftBlurByte;
  if vtWord in xSetofWatch then
    xWatch:=xWatch or ftBlurWord;
  if vtDWord in xSetofWatch then
    xWatch:=xWatch or ftBlurDWord;
  if vtDDWord in xSetofWatch then
    xWatch:=xWatch or ftBlurDDWord;
  if vtSingle in xSetofWatch then
    xWatch:=xWatch or ftBlurSingle;
  if vtDouble in xSetofWatch then
    xWatch:=xWatch or ftBlurDouble;
  if vtLDouble in xSetofWatch then
    xWatch:=xWatch or ftBlurEXtended;

  try

  if self.ToolFindInitialize(xProcessesID) then
  begin
    self.FIsStartBlur:=true;
    self.FFindType:=ftBlur;
    setlength(FMemBlurValueCopyType,0);
    setlength(FMemBlurValueCopy,0);
    setlength(FMemBlurValueCopyType,self.FMemSizeSum);
    setlength(FMemBlurValueCopy,self.FMemSizeSum);
    self.FMemBlurValueCopyTrueCount:=0;
    pcopy:=0;
    PositionFirst:=0.5;
    xCallBack(PositionFirst,self);
    Application.ProcessMessages;
    for  i:=0 to length(self.FMemoryInf)-1 do
    begin
      readCount:=self.FMemoryInf[i].RegionSize;
      PositionFirst:=PositionFirst+95.0*(FMemoryInf[i].RegionSize)/self.FMemSizeSum;
      if not xCallBack(PositionFirst,self) then
        begin
          ToolCloseProcHandle(self.FProcHandle);
          self.FIsRunning:=false;
          result:=true;
          exit;
        end;

      Application.ProcessMessages;
      if windows.ReadProcessMemory(self.FProcHandle,self.FMemoryInf[i].BaseAddress,
        @FMemBlurValueCopy[pcopy],readCount,L) then
      begin
        inc(FMemBlurValueCopyTrueCount,ReadCount);
        watch:=xWatch;   //一般为 01111111

        //for j:=0 to ReadCount-10 do
        //  FMemBlurValueCopyType[pCopy+j]:=watch;
        // 汇编优化
        Temp:=DWord(Self);
        asm

              xor     ecx,ecx
              mov     cl,watch
              mov     edx,ecx
              mov     ch,dl
              shl     ecx,16
              mov     cl,dl
              mov     ch,dl

              mov     edx,Temp
              mov     edx,[edx+FMemBlurValueCopyType]
              add     edx,pCopy

              mov     eax,ReadCount
              sub     eax,10
              shr     eax,2
              test    eax,eax
              jl      @EndFor
              inc     eax

            @StratFor:
              mov     [edx],ecx
              add     edx,4
              dec     eax
              jnz     @StratFor
            @endFor:

        end;
        for j:=((ReadCount-10) div 4)*4+1 to ReadCount-10 do
          FMemBlurValueCopyType[pCopy+j]:=watch;

          
        watch:=Watch and (not ftBlurExtended);
        for j:=ReadCount-9 to ReadCount-8 do
          FMemBlurValueCopyType[pCopy+j]:=watch;
        watch:=Watch and  (not ftBlurDouble) and (not ftBlurDDWord);
        for j:=ReadCount-7 to ReadCount-4 do
          FMemBlurValueCopyType[pCopy+j]:=watch ;
        watch:=Watch and  (not ftBlurSingle) and (not ftBlurDWord);
        for j:=ReadCount-3 to ReadCount-2 do
          FMemBlurValueCopyType[pCopy+j]:=watch;
        watch:=Watch and  (not ftBlurWord);
        for j:=ReadCount-1 to ReadCount-1 do
          FMemBlurValueCopyType[pCopy+j]:=watch;
      end;
      inc(pCopy,readCount);
    end;
    self.FFindIndex:=1;
    result:=true;
  end
  else
    result:=false;

  finally
  ToolCloseProcHandle(self.FProcHandle);
  self.FIsRunning:=false;
  self.Hint0:=' 模糊查找初始化任务完成 ';
  self.Hint1:='';
  end;
end;

procedure TMemEditMission.OnFindBlurAfter;
var
  i,j   : integer;
  BCopy  : BYTE;
  BlurValueType : TSetOfValueType;
  readCount,Count : integer;
  xValueAddress: TValueAddress;
  PType  : Pointer;
begin
  FValueAddressListCount:=0;
  setlength(self.FValueAddressList,0);
  TempFValueAddressListLength:=0;

  //
  readCount:=0;
  Count:=0;
  for i:= 0 to length(self.FMemoryInf)-1 do
  begin

    PType:=@self.FMemBlurValueCopyType[readCount];
    for j:=0 to self.FMemoryInf[i].RegionSize-1 do
    begin
      BCopy:=PByte(PType)^;
      if BCopy<>0 then
      begin

        BlurValueType:=[];
        if (BCopy and ftBlurByte) >0 then
        begin
          Include(BlurValueType,vtByte);
        end;
        if (BCopy and ftBlurWord) >0 then
        begin
          Include(BlurValueType,vtWord);
        end;
        if (BCopy and ftBlurDWord) >0 then
        begin
          Include(BlurValueType,vtDWord);
        end;
        if (BCopy and ftBlurDDWord) >0 then
        begin
          Include(BlurValueType,vtDDWord);
        end;
        if (BCopy and ftBlurSingle) >0 then
        begin
          Include(BlurValueType,vtSingle);
        end;
        if (BCopy and ftBlurDouble) >0 then
        begin
          Include(BlurValueType,vtDouble);
        end;
        if (BCopy and ftBlurExtended) >0 then
        begin
          Include(BlurValueType,vtLDouble);
        end;

        xValueAddress.ValueType:=BlurValueType;
        xValueAddress.Address:=Pointer(DWord(self.FMemoryInf[i].BaseAddress)+Dword(j));
        self.ToolAddressListAdd(xValueAddress);

        inc(Count);
        if Count>=integer(MemEditSysSet.SysSet.ShowAddressCount) then break;
      end;
      if Count>=integer(MemEditSysSet.SysSet.ShowAddressCount) then break;
      inc(DWord(PType));
    end;
    if Count>=integer(MemEditSysSet.SysSet.ShowAddressCount) then break;
    inc(readCount,self.FMemoryInf[i].RegionSize);
  end;
  self.FValueAddressListCount:=Count;
  setlength(self.FValueAddressList,self.FValueAddressListCount);
  TempFValueAddressListLength:=self.FValueAddressListCount;
end;

function TMemEditMission.ToBlurFindAValue(var sCommand: string; xCallBack  : TCallBackShowProgressBar=nil): Boolean;
var
  i,j  : integer;
  readCount : integer;
  L  : DWord;
  Pdata,PCopy,PType: Pointer;
  bCopy : BYTE;
  TempData : array of Byte;
  XValueInt : int64;
  XValueFloat,XValueDX: Extended;
  xWarch :Byte;
  PositionFirst : Extended;
  cCmd   :char;
  xValue: string;
begin

  if self.FIsRunning then
  begin
    result:=false;
    exit;
  end
  else
  begin
    self.FIsRunning:=true;
    self.Hint0:=' 正在进行模糊查找任务 ';
    self.Hint1:='';
  end;

  try

  cCmd := #0;
  for i:=1 to length(sCommand) do
  begin
    if sCommand[i] = '>' then
    begin
      cCmd   := '>';
      xValue := copy(sCommand,i+1,length(sCommand)-i);
      break;
    end
    else if sCommand[i] = '<' then
    begin
      cCmd   := '<';
      xValue := copy(sCommand,i+1,length(sCommand)-i);
      break;
    end
    else if (sCommand[i] in ['0'..'9','.','+','-']) then
    begin
      cCmd   := '=';
      xValue := copy(sCommand,i,length(sCommand)-i+1);
      break;
    end
    else if (sCommand[i] in [' ']) then
    begin
      continue;
    end
    else
    begin
      self.FIsRunning:=false;
      result:=false;
      exit;
    end;
  end;

  if self.FFindType=ftBlur then
  begin
    try
      XValueInt:=10;
      if StrIsInt(xValue) then
      begin
        XValueInt:=STrtoint64(xValue);
        XValueFloat:=StrToFloat(xValue);
        if XValueFloat=0 then
          XValueDX:=abs(XValueFloat*0.0001)
        else
          XValueDX:=GetFloatDX(xValue);

        xWarch:=127;

      end
      else if StrIsFloat(xValue) then
      begin
        XValueFloat:=StrToFloat(xValue);
        if XValueFloat=0 then
          XValueDX:=abs(XValueFloat*0.0001)
        else
          XValueDX:=GetFloatDX(xValue);

        xWarch:=ftBlurSingle or ftBlurDouble or ftBlurExtended;
      end
      else
      begin
        self.FIsRunning:=false;
        result:=false;
        exit;
      end;
    except
      self.FIsRunning:=false;
      result:=false;
      exit;
    end;
    self.FProcHandle:=self.ToolGetProcHandle(self.FProcessesID);
    readCount:=0;
    FMemBlurValueCopyTrueCount:=0;
    PositionFirst:=5.0;
    xCallBack(PositionFirst,self);
    Application.ProcessMessages;
    for i:= 0 to length(self.FMemoryInf)-1 do
    begin
      setlength(TempData,self.FMemoryInf[i].RegionSize);
      PositionFirst:=PositionFirst+95.0*(FMemoryInf[i].RegionSize)/self.FMemSizeSum;
      if not xCallBack(PositionFirst,self) then
      begin
        ToolCloseProcHandle(self.FProcHandle);
        self.FIsRunning:=false;
        result:=true;
        exit;
      end;

      Application.ProcessMessages;
      if windows.ReadProcessMemory(self.FProcHandle,self.FMemoryInf[i].BaseAddress,
        @TempData[0],self.FMemoryInf[i].RegionSize,L) then
      begin
        PData:=@TempData[0];
        PCopy:=@self.FMemBlurValueCopy[readCount];
        PType:=@self.FMemBlurValueCopyType[readCount];
        if cCmd= '=' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            BCopy:=BCopy and xWarch;
            if BCopy<>0 then
            begin
              if PByte(PData)^=BYTE(XValueInt) then
              begin
                if (BCopy and ftBlurByte) <>0 then
                begin
                  if PByte(PData)^<>(XValueInt) then
                    BCopy:=BCopy and (not (ftBlurByte));
                end;
                if (BCopy and ftBlurWord) <>0 then
                begin
                  if PWord(PData)^<>(XValueInt) then
                    BCopy:=BCopy and (not (ftBlurWord));
                end;
                if (BCopy and ftBlurDWord) <>0 then
                begin
                  if PDWord(PData)^<>(XValueInt) then
                    BCopy:=BCopy and (not (ftBlurDWord));
                end;
                if (BCopy and ftBlurDDWord) <>0 then
                begin
                  if Pint64(PData)^<>(XValueInt) then
                    BCopy:=BCopy and (not ftBlurDDWord);
                end;
              end
              else
              begin
                BCopy:=BCopy and (not ( ftBlurByte or ftBlurWord or ftBlurDWord or ftBlurDDWord));
              end;

              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(PSingle(PCopy))
                  or (not IsFloatEQ(xValueFloat,Psingle(PData)^,xValueDX)) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (not IsFloatEQ(xValueFloat,PDouble(PData)^,xValueDX)) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (not IsFloatEQ(xValueFloat,PExtended(PData)^,xValueDX)) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end
        else if cCmd = '>' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            BCopy:=BCopy and xWarch;
            if BCopy<>0 then
            begin
              if (BCopy and ftBlurByte) <>0 then
              begin
                if PByte(PData)^<=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurByte);
              end;
              if (BCopy and ftBlurWord) <>0 then
              begin
                if PWord(PData)^<=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurWord);
              end;
              if (BCopy and ftBlurDWord) <>0 then
              begin
                if PDWord(PData)^<=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurDWord);
              end;
              if (BCopy and ftBlurDDWord) <>0 then
              begin
                if Pint64(PData)^<=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurDDWord);
              end;

              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(Psingle(PCopy))
                  or (xValueFloat>=Psingle(PData)^) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (xValueFloat>=PDouble(PData)^) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (xValueFloat>=PExtended(PData)^) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end
        else if cCmd='<' then
        begin
          for j:=0 to self.FMemoryInf[i].RegionSize-1 do
          begin
            BCopy:=PByte(PType)^;
            BCopy:=BCopy and xWarch;
            if BCopy<>0 then
            begin

              if (BCopy and ftBlurByte) <>0 then
              begin
                if PByte(PData)^>=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurByte);
              end;
              if (BCopy and ftBlurWord) <>0 then
              begin
                if PWord(PData)^>=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurWord);
              end;
              if (BCopy and ftBlurDWord) <>0 then
              begin
                if PDWord(PData)^>=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurDWord);
              end;
              if (BCopy and ftBlurDDWord) <>0 then
              begin
                if Pint64(PData)^>=(XValueInt) then
                  BCopy:=BCopy and (not ftBlurDDWord);
              end;

              if (BCopy and ftBlurSingle) <>0 then
              begin
                if IsNanFloat(Psingle(PData)) or IsNanFloat(Psingle(PCopy))
                  or (xValueFloat<=Psingle(PData)^) then
                    BCopy:=BCopy and (not ftBlurSingle);
              end;
              if (BCopy and ftBlurDouble) <>0 then
              begin
                if IsNanFloat(PDouble(PData)) or IsNanFloat(PDouble(PCopy))
                  or (xValueFloat<=PDouble(PData)^) then
                    BCopy:=BCopy and (not ftBlurDouble);
              end;
              if (BCopy and ftBlurExtended) <>0 then
              begin
                if IsNanFloat(PExtended(PData)) or IsNanFloat(PExtended(PCopy))
                  or (xValueFloat<=PExtended(PData)^) then
                    BCopy:=BCopy and (not ftBlurExtended);
              end;
              PByte(PType)^:=BCopy;
              if BCopy<>0 then inc(FMemBlurValueCopyTrueCount);
            end;
            inc(DWord(PData));
            inc(DWord(PType));
            inc(DWord(PCopy));
          end;
        end;//end if cCom =...
      end
      else
      begin
        PType:=@self.FMemBlurValueCopyType[readCount];
        for j:=0 to self.FMemoryInf[i].RegionSize-1 do
        begin
          PByte(PType)^:=0;
          inc(DWord(PType));
        end;
      end;
      inc(readCount,self.FMemoryInf[i].RegionSize);
    end;
    self.ToolCloseProcHandle(self.FProcHandle);
    inc(self.FFindIndex);
    self.ToolAssignValue;
    result:=true;
  end
  else
    result:=false;
  finally
  ToolCloseProcHandle(self.FProcHandle); //
  self.FIsRunning:=false;
    self.Hint0:=' 模糊查找任务完成 ';
    self.Hint1:='';
  end;
end;

{ TMemEditAddressList }

function TMemEditAddressList.AddAddress(AddressList: TAddressList;WriteItOnce :Boolean=false): integer;
var
  xPAddressList : TAddressList;
   i   : integer;
   IsHaved :integer;
   wnStr   : string;
begin
  IsHaved:=-1;
  for i:=0 to self.Count-1 do
  begin
    if (self.Items[i].ProcessesID=AddressList.ProcessesID)
      and (self.Items[i].ValueAddress=AddressList.ValueAddress) then
    begin
      IsHaved:=i;
      break;
    end;
  end;

  if IsHaved>=0 then
  begin
    wnStr:= '  列表中已经存在该监视地址,要替换吗?  '+#13+#10
           +#13+#10
           +'     被替换的的监视数据:  '+#13+#10
           +'         所值任务 : '+self.Items[IsHaved].MsName+#13+#10
           +'         数值名称 : '+self.Items[IsHaved].ValueName+#13+#10
           +'         数值地址 : '+inttoHex(integer(self.Items[IsHaved].ValueAddress),8)+#13+#10
           +'         修改数值 : '+self.Items[IsHaved].Value+#13+#10;
    if windows.MessageBox(Application.Handle,PChar(wnStr),
      '警告',MB_OKCANCEL+MB_ICONWARNING)<>ID_OK then
    begin
      result:=-1;
      exit;
    end
    else
    begin
      self.Items[IsHaved].Assign(AddressList);
      result:=IsHaved;
      if WriteItOnce then self.WriteProcAValue(result);
    end;
  end
  else
  begin
    xPAddressList:=TAddressList.Create;
    xPAddressList.Assign(AddressList);
    xPAddressList.ID:=self.FID;
    inc(self.FID);
    result:=self.FList.Add(xPAddressList);
    if WriteItOnce then self.WriteProcAValue(result);
  end;
end;

procedure TMemEditAddressList.Clear;
var
  i : integer;
begin
  for i:=0 to self.Count-1 do
    TAddressList(self.FList.Items[i]).Free;
  self.FList.Clear;
end;

constructor TMemEditAddressList.Create;
begin
  inherited;
  self.FID:=0;
  self.FList:=TList.Create;
  FMission:=TMemEditMission.Create();
end;

procedure TMemEditAddressList.DelAddress(index: integer);
begin
  FMission.Free;
  TAddressList(self.FList.Items[index]).Free;
  self.FList.Delete(index);
end;

procedure TMemEditAddressList.DelAddressByID(ID: DWord);
var
  i : integer;
begin
  for i:=0 to self.Count-1 do
  begin
    if (self.Items[i].ID=ID) then
    begin
      self.DelAddress(i);
      exit;
    end;
  end;
end;

destructor TMemEditAddressList.Destroy;
begin
  self.Clear;
  self.FList.Free;
  inherited;
end;

function TMemEditAddressList.GetCount():integer;
begin
  result:=self.FList.Count;
end;

function TMemEditAddressList.GetValue(index: integer): TAddressList;
begin
  result:=TAddressList(self.FList.Items[index]);
end;

procedure TMemEditAddressList.ReadProcAllValue;
var
  i : integer;
begin
    for i:=0 to self.Count-1 do
    begin
      self.ReadProcAValue(i);
    end;
end;

function TMemEditAddressList.ReadProcAValue(index: integer): Boolean;
var
  hProc  : THandle;
  i      : integer;
  Data   : array [0..64] of DWord;
  PData  : Pointer;
  LGet   : Dword;
  Wstr   : WideString;
begin

  with self.Items[index] do
  begin
    hProc:= FMission.ToolGetProcHandle(ProcessesID);
    try
      PData:=@Data[0];
      IsWriteOk:=false;
      case Valuetype of
        vtByte:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,1,LGet) then
            begin
              Value:=inttostr(PByte(PData)^);
              if PshortInt(PData)^<0 then
                Value:=Value+'('+inttostr(PshortInt(PData)^)+')';
              IsWriteOk:=true;
            end;
          end;
        vtWord:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,2,LGet) then
            begin
              Value:=inttostr(PWord(PData)^);
              if PSmallInt(PData)^<0 then
                Value:=Value+'('+inttostr(PSmallInt(PData)^)+')';
              IsWriteOk:=true;
            end;
          end;
        vtDWord:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,4,LGet) then
            begin
              Value:=inttostr(PDWord(PData)^);
              if PInteger(PData)^<0 then
                Value:=Value+'('+inttostr(PInteger(PData)^)+')';
              IsWriteOk:=true;
            end;
          end;
        vtDDWord:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,8,LGet) then
            begin
              Value:=inttostr(PInt64(PData)^);
              IsWriteOk:=true;
            end;
          end;
        vtSingle:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,4,LGet) then
            begin
              if not IsNanFloat(PSingle(PData)) then
              begin
                Value:=FloatToStr(PSingle(PData)^);
                IsWriteOk:=true;
              end;
            end;
          end;
        vtDouble:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,8,LGet) then
            begin
              if not IsNanFloat(PDouble(PData)) then
              begin
                Value:=FloatToStr(PDouble(PData)^);
                IsWriteOk:=true;
              end;
            end;
          end;
        vtLDouble:
          begin
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,10,LGet) then
            begin
              if not IsNanFloat(PExtended(PData)) then
              begin
                Value:=FloatToStr(PExtended(PData)^);
                IsWriteOk:=true;
              end;
            end;
          end;
        vtAString:
          begin
            for i:=low(DAta) to High(Data) do
            begin
              Data[i]:=0;
            end;
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,256,LGet) then
            begin
              Value:=PChar(PData)^;
              IsWriteOk:=true;
            end;
          end;
        vtWString:
          begin
            for i:=low(Data) to High(Data) do
            begin
              Data[i]:=0;
            end;
            if windows.ReadProcessMemory(hProc,ValueAddress,PData,256,LGet) then
            begin
              Wstr:=PWChar(PData)^;
              Value:=Wstr;
              IsWriteOk:=true;
            end;
          end;
      end;
      result:=IsWriteOk;
      FMission.ToolCloseProcHandle(hProc);
    except
      IsWriteOk:=false;
      result:=IsWriteOk;
      FMission.ToolCloseProcHandle(hProc);
    end;
  end;
end;

function TMemEditAddressList.ReadProcAValueByID(ID: DWord): Boolean;
var
  i : integer;
begin
  result:=false;
  for i:=0 to self.Count-1 do
  begin
    if (self.Items[i].ID=ID) then
    begin
      result:=self.ReadProcAValue(i);
      exit;
    end;
  end;
end;

procedure TMemEditAddressList.SetValue(index: integer;
  const xValue: TAddressList);
begin
  TAddressList(self.FList.Items[index]).Assign(xValue);
end;

procedure TMemEditAddressList.SetValueByID(const xValue: TAddressList);
var
  i : integer;
begin
  for i:=0 to self.Count-1 do
  begin
    if (self.Items[i].ID=xValue.ID) then
    begin
      self.SetValue(i,xValue);
      exit;
    end;
  end;
end;

procedure TMemEditAddressList.WriteProcAllValue;
var
  i : integer;
begin
    for i:=0 to self.Count-1 do
    begin
      self.WriteProcAValue(i);
    end;
end;

function TMemEditAddressList.WriteProcAValue(index: integer): Boolean;
var
  hProc  : THandle;
  Data   : array [0..64] of DWord;
  PData  : Pointer;
  LGet   : Dword;
  Wstr   : WideString;

  function DelBracket(const strInt: string):string;
  var
    i  : integer;
  begin
    for i:=1 to length(strInt) do
    begin
      if strInt[i]='(' then
      begin
        result:=copy(strInt,1,i-1);
        exit;
      end;
    end;
    result:=strInt;
  end;

begin

  with self.Items[index] do
  begin
    hProc:= FMission.ToolGetProcHandle(ProcessesID);
    try
      PData:=@Data[0];
      IsWriteOk:=false;
      case Valuetype of
        vtByte:
          begin
            PByte(PData)^:=strtoint(DelBracket(Value));
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,1,LGet);
          end;
        vtWord:
          begin
            PWord(PData)^:=strtoint(DelBracket(Value));
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,2,LGet);
          end;
        vtDWord:
          begin
            PDWord(PData)^:=strtoint(DelBracket(Value));
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,4,LGet);
          end;
        vtDDWord:
          begin
            PInt64(PData)^:=strtoint(Value);
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,8,LGet);
          end;
        vtSingle:
          begin
            PSingle(PData)^:=strtofloat(Value);
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,4,LGet);
          end;
        vtDouble:
          begin
            PDouble(PData)^:=strtofloat(Value);
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,8,LGet);
          end;
        vtLDouble:
          begin
            PExtended(PData)^:=strtofloat(Value);
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,PData,10,LGet);
          end;
        vtAString:
          begin
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,@Value[1],length(Value),LGet);
          end;
        vtWString:
          begin
            Wstr:=Value;
            IsWriteOk:=windows.WriteProcessMemory(hProc,ValueAddress,@Wstr[1],length(Wstr)*2,LGet);
          end;
      end;
      result:=IsWriteOk;
      FMission.ToolCloseProcHandle(hProc);
    except
      IsWriteOk:=false;
      result:=IsWriteOk;
      FMission.ToolCloseProcHandle(hProc);
    end;
  end;
end;

function TMemEditAddressList.WriteProcAValueByID(ID: DWord): Boolean;
var
  i : integer;
begin
  result:=false;
  for i:=0 to self.Count-1 do
  begin
    if (self.Items[i].ID=ID) then
    begin
      result:=self.WriteProcAValue(i);
      exit;
    end;
  end;
end;

procedure TMemEditAddressList.WriteProcValueAuto;
var
  i : integer;
begin
  try
    if self.FList.Count>0 then
    begin
      for i:=0 to self.Count-1 do
      begin
        if (self.Items[i].EditType=etAuto) then
        begin
          self.WriteProcAValue(i);
        end;
      end;
    end;
  except
  end;
end;

{ TAddressList }

procedure TAddressList.Assign(XValue: TAddressList);
begin
  self.MsName:=xValue.MsName;
  self.ValueName:=xValue.ValueName;
  self.ValueAddress:=xValue.ValueAddress;
  self.Value:=xValue.Value;
  self.ValueType:=xValue.ValueType;
  self.EditType:=xValue.EditType;
  self.HotKey:=xValue.HotKey;
  self.ProcessesID:=xValue.ProcessesID;
  self.IsWriteOk:=xValue.IsWriteOk;
end;
procedure TAddressList.Assign(XValue: TAddressListSave);
begin
  self.MsName:=xValue.MsName;
  self.ValueName:=xValue.ValueName;
  self.ValueAddress:=xValue.ValueAddress;
  self.Value:=xValue.Value;
  self.ValueType:=xValue.ValueType;
  self.EditType:=xValue.EditType;
  self.HotKey:=xValue.HotKey;
  self.ProcessesID:=xValue.ProcessesID;
  self.IsWriteOk:=xValue.IsWriteOk;
end;

procedure TAddressList.AssignTo(var XValue: TAddressList);
begin
  XValue.Assign(self);
end;

procedure TAddressList.AssignTo(var XValue: TAddressListSave);
begin
  XValue.MsName:=self.MsName;
  XValue.ValueName:=self.ValueName;
  XValue.ValueAddress:=self.ValueAddress;
  XValue.Value:=self.Value;
  XValue.ValueType:=self.ValueType;
  XValue.EditType:=self.EditType;
  XValue.HotKey:=self.HotKey;
  XValue.ProcessesID:=self.ProcessesID;
  XValue.IsWriteOk:=self.IsWriteOk;
  XValue.ID:=self.ID;
end;

{ TMissionList }

function  TMissionList.AddMission(const xProcessesID: THandle;
  const msName: string):integer;
begin
  result:=self.FList.Add(TMemEditMission.Create(xProcessesID,msName));
end;

constructor TMissionList.Create;
begin
  inherited;
  self.FList:=Tlist.Create;
end;

procedure TMissionList.DelMission(const Index:integer);
begin
  TMemEditMission(self.FList[Index]).Free;
  self.FList.Delete(Index);
end;

destructor TMissionList.Destroy;
var
  i : integer;
begin
  for i:=0 to self.Count-1 do
  begin
    TMemEditMission(self.FList[i]).Free;
  end;
  self.FList.Free;
  inherited;
end;

function TMissionList.GetCount: integer;
begin
  result:=self.FList.Count;
end;

function TMissionList.GetListValue(const Index: integer): TMemEditMission;
begin
  result:=TMemEditMission(self.FList[Index]);
end;

{ TMemEditSysSet }

constructor TMemEditSysSet.Create;
var
  str  : string;
begin
  inherited;
  self.frmSystemSet:=nil;
  SELF.frmMemSelect:=nil; 
  str:=ExtractFilePath(Application.ExeName);
  self.iniFileName:=str+SysSetIniFileName;
  self.GetDefultSysSet(self.SysSet);
  self.LoadFromIniSet();
end;

destructor TMemEditSysSet.Destroy;
begin
  self.SaveToIniSet();
  if self.frmSystemSet<>nil then
    self.frmSystemSet.Free;
  inherited;
end;

procedure TMemEditSysSet.LoadFromIniSet;
var
  xFile  : file of  tagSysSet;
  ts     : tagSysSet;
begin
   AssignFile(xFile,self.iniFileName);
   try
     Reset(xFile);
     Read(xFile,ts);
     CloseFile(xFile);
     self.SysSet:=ts;
   except
   end;
end;

procedure TMemEditSysSet.SaveToIniSet;
var
  xFile  : file of  tagSysSet;
begin
   AssignFile(xFile,self.iniFileName);
   try
     ReWrite(xFile);
     Write(xFile,self.SysSet);
   finally
     CloseFile(xFile);
   end;
end;

procedure TMemEditSysSet.SetDefultHot;
var
  SSet  : tagSysSet;
begin
  SSet:=self.SysSet;

  SSet.CallHotKey       :=StrToHotKey('Ctrl+Num *');
  SSet.IsOnHotStopProc  :=false;

  self.ShowSysSet(SSet);

end;

procedure TMemEditSysSet.SetDefultBuffer;
var
  SSet  : tagSysSet;
begin
  SSet:=self.SysSet;

  SSet.FindBufferSize   :=1024*64;
  SSet.ShowAddressCount :=100;
  SSet.ListMemMaxSize   :=32*1024*1024;

  self.ShowSysSet(SSet);

end;

procedure TMemEditSysSet.SetDefultFiltrate;
var
  SSet  : tagSysSet;
begin
  SSet:=self.SysSet;

  self.StrToModuleSuffix( '*.DLL'   +#13+#10
                        + '*.OCX'   +#13+#10
                        + '*.BPL'   +#13+#10
                        + '*.DRV'   +#13+#10
                        + '*.SYS'   +#13+#10
                        + '*.VXD'   +#13+#10
                        + '*.NLS'   +#13+#10,SSet);
  SSet.FiltrateSet      :=fsSys;
  SSet.IsModuleSuffixAsk     :=false;
  self.ShowSysSet(SSet);

end;

procedure TMemEditSysSet.SetDefultType;
var
  SSet  : tagSysSet;
begin
  SSet:=self.SysSet;

  SSet.WatchTypeInt     :=[vtByte,vtWord,vtDword,vtDDWord];
  SSet.WatchTypeFloat   :=[vtSingle,vtDouble,vtLDouble];
  SSet.WatchTypeString  :=[vtAString,vtWString];
  SSet.WatchTypeBlur    :=[vtByte,vtWord,vtDword,vtSingle,vtDouble];
  SSet.IsFindAsk        :=false;

  self.ShowSysSet(SSet);

end;

procedure TMemEditSysSet.GetDefultSysSet(var xSysSet :tagSysSet);
var
  SSet  : tagSysSet;
begin
  SSet.FindBufferSize   :=1024*64;
  SSet.ShowAddressCount :=100;
  SSet.ListMemMaxSize   :=32*1024*1024;

  SSet.CallHotKey       :=StrToHotKey('Ctrl+Num *');
  SSet.IsOnHotStopProc  :=false;

  self.StrToModuleSuffix( '*.DLL'   +#13+#10
                        + '*.OCX'   +#13+#10
                        + '*.BPL'   +#13+#10
                        + '*.DRV'   +#13+#10
                        + '*.SYS'   +#13+#10
                        + '*.VXD'   +#13+#10
                        + '*.NLS'   +#13+#10,SSet);
  SSet.FiltrateSet      :=fsSys;
  SSet.IsModuleSuffixAsk     :=false;

  SSet.WatchTypeInt     :=[vtByte,vtWord,vtDword,vtDDWord];
  SSet.WatchTypeFloat   :=[vtSingle,vtDouble,vtLDouble];
  SSet.WatchTypeString  :=[vtAString,vtWString];
  SSet.WatchTypeBlur    :=[vtByte,vtWord,vtDword,vtSingle,vtDouble];
  SSet.IsFindAsk        :=false;

  xSysSet:=SSet;

end;

procedure TMemEditSysSet.ApplySysSet(var xDecSysSet: tagSysSet;const xSysSet: tagSysSet);
var
  ts : tagSysSet;
begin
  ts:=xSysSet;

  if ts.FindBufferSize<1024 then
    ts.FindBufferSize:=1024
  else if ts.FindBufferSize>1024*1024*32 then
    ts.FindBufferSize:=1024*1024*1;

  if ts.ShowAddressCount<5 then
    ts.ShowAddressCount:=5
  else if ts.ShowAddressCount>10000 then
    ts.ShowAddressCount:=100;

  if ts.ListMemMaxSize<1024*1024*1 then
    ts.ListMemMaxSize:=1024*1024*1
  else if ts.ListMemMaxSize>1024*1024*1024 then
    ts.ListMemMaxSize:=1024*1024*30;

  if ts.CallHotKey=0 then
    ts.CallHotKey:=StrToHotKey('Ctrl+Num *');

  if ts.WatchTypeInt=[] then
    ts.WatchTypeInt:=[vtByte,vtWord,vtDword,vtDDWord];

  if ts.WatchTypeFloat=[] then
    ts.WatchTypeFloat:=[vtSingle,vtDouble,vtLDouble];

  if ts.WatchTypeString=[] then
    ts.WatchTypeString:= [vtAString,vtWString];

  if ts.WatchTypeBlur=[] then
    ts.WatchTypeBlur:=[vtByte,vtWord,vtDword,vtSingle,vtDouble];

  xDecSysSet:=ts;
  
end;

function TMemEditSysSet.ModuleSuffixToStr(const xSet: tagSysSet): string;
var
  i  : integer;
  str: string;
begin
  str:='';
  for i:= low(xSet.ModuleSuffix) to high(xSet.ModuleSuffix) do
  begin
    str:=str+xSet.ModuleSuffix[i];
  end;
  result:=str;
end;

procedure TMemEditSysSet.StrToModuleSuffix(const xModuleSuffix: string;
  var xSet: tagSysSet);
var
  i  : integer;
  str: string;
  L  : integer;
begin
  str:=xModuleSuffix;
  for i:= low(xSet.ModuleSuffix) to high(xSet.ModuleSuffix) do
  begin
    L:=length(str);
    if L>=256 then
    begin
      xSet.ModuleSuffix[i]:=copy(str,1,256);
      str:=copy(str,257,L-256);
    end
    else if L>0 then
    begin
      xSet.ModuleSuffix[i]:=str;
      str:='';
    end
    else
      xSet.ModuleSuffix[i]:='';
  end;
end;

procedure TMemEditSysSet.ShowSysSet(const xSysSet: tagSysSet);
begin
  if (self.frmSystemSet<>nil) then
  begin
    //
    self.frmSystemSet.HotKeyApp.HotKey:=xSysSet.CallHotKey;
    self.frmSystemSet.RadioButtonProcStop.Checked:=xSysSet.IsOnHotStopProc;
    self.frmSystemSet.RadioButtonProcNoStop.Checked:=not xSysSet.IsOnHotStopProc;
    self.frmSystemSet.EditFindBuffuer.Text:=inttostr(xSysSet.FindBufferSize div 1024);
    self.frmSystemSet.EditShowAddressMaxCount.Text:=inttostr(xSysSet.ShowAddressCount);
    self.frmSystemSet.EditFindMaxMem.Text:=inttostr(xSysSet.ListMemMaxSize div (1024*1024));
    self.frmSystemSet.MemoModuleSuffix.Text:=self.ModuleSuffixToStr(xSysSet);
    self.frmSystemSet.RadioButtonFiltrateNO.Checked:=(xSysSet.FiltrateSet=fsNo);
    self.frmSystemSet.RadioButtonFiltrateSys.Checked:=(xSysSet.FiltrateSet=fsSys);
    self.frmSystemSet.RadioButtonFiltrateAll.Checked:=(xSysSet.FiltrateSet=fsAll);
    self.frmSystemSet.CheckBoxModuleSuffixAsk.Checked:=xSysSet.IsModuleSuffixAsk;

    self.frmSystemSet.CheckBoxFindAsk.Checked:=xSysSet.IsFindAsk;

    self.frmSystemSet.CheckBoxTypeIntByte.Checked:=(vtByte in xSysSet.WatchTypeInt);
    self.frmSystemSet.CheckBoxTypeIntWord.Checked:=(vtWord in xSysSet.WatchTypeInt);
    self.frmSystemSet.CheckBoxTypeIntDWord.Checked:=(vtDWord in xSysSet.WatchTypeInt);
    self.frmSystemSet.CheckBoxTypeIntDDWord.Checked:=(vtDDWord in xSysSet.WatchTypeInt);

    self.frmSystemSet.CheckBoxTypeFloatSingle.Checked:=(vtSingle in xSysSet.WatchTypeFloat);
    self.frmSystemSet.CheckBoxTypeFloatDouble.Checked:=(vtDouble in xSysSet.WatchTypeFloat);
    self.frmSystemSet.CheckBoxTypeFloatExtended.Checked:=(vtLDouble in xSysSet.WatchTypeFloat);

    self.frmSystemSet.CheckBoxTypeStringAString.Checked:=(vtAString in xSysSet.WatchTypeString);
    self.frmSystemSet.CheckBoxTypeStringWString.Checked:=(vtWString in xSysSet.WatchTypeString);


    self.frmSystemSet.CheckBoxTypeBlurByte.Checked:=(vtByte in xSysSet.WatchTypeBlur);
    self.frmSystemSet.CheckBoxTypeBlurWord.Checked:=(vtWord in xSysSet.WatchTypeBlur);
    self.frmSystemSet.CheckBoxTypeBlurDWord.Checked:=(vtDWord in xSysSet.WatchTypeBlur);
    self.frmSystemSet.CheckBoxTypeBlurDDWord.Checked:=(vtDDWord in xSysSet.WatchTypeBlur);
    self.frmSystemSet.CheckBoxTypeBlurSingle.Checked:=(vtSingle in xSysSet.WatchTypeBlur);
    self.frmSystemSet.CheckBoxTypeBlurDouble.Checked:=(vtDouble in xSysSet.WatchTypeBlur);
    self.frmSystemSet.CheckBoxTypeBlurExtended.Checked:=(vtLDouble in xSysSet.WatchTypeBlur);

  end;
end;

function TMemEditSysSet.GetUserSet(var xSysSet: tagSysSet):Boolean;
var
  ts  : tagSysSet;
  vt  : TSetOfValueType;
  ErrorText : string;
  ErrorTextEX : string;
begin
  result:=false;

  if (self.frmSystemSet<>nil) then
  begin
    self.GetDefultSysSet(ts);

    ErrorText:='';
    try
      ts.CallHotKey := self.frmSystemSet.HotKeyApp.HotKey;
      ErrorText:='';
      ts.IsOnHotStopProc:=self.frmSystemSet.RadioButtonProcStop.Checked;

      ErrorText:='查找时使用的缓冲交换区的大小';
      ts.FindBufferSize:=strtoint(self.frmSystemSet.EditFindBuffuer.Text)*1024;
      ErrorText:='查找到的数据地址最大显示数目';
      ts.ShowAddressCount:= strtoint( self.frmSystemSet.EditShowAddressMaxCount.Text);
      ErrorText:='查找时允许使用的最大动态内存';
      ts.ListMemMaxSize  := strtoint(self.frmSystemSet.EditFindMaxMem.Text)*1024*1024;

      ErrorText:='要过滤的模块后缀名';
      self.StrToModuleSuffix(self.frmSystemSet.MemoModuleSuffix.Text,ts);
      ErrorText:='';
      if self.frmSystemSet.RadioButtonFiltrateNO.Checked then
        ts.FiltrateSet:=fsNo
      else if self.frmSystemSet.RadioButtonFiltrateAll.Checked then
        ts.FiltrateSet:=fsAll
      else
        ts.FiltrateSet:=fsSys;
      ts.IsModuleSuffixAsk:=self.frmSystemSet.CheckBoxModuleSuffixAsk.Checked;

      ts.IsFindAsk := self.frmSystemSet.CheckBoxFindAsk.Checked;

      vt:=[];
      if self.frmSystemSet.CheckBoxTypeIntByte.Checked then
        include(vt,vtByte);
      if self.frmSystemSet.CheckBoxTypeIntWord.Checked then
        include(vt,vtWord);
      if self.frmSystemSet.CheckBoxTypeIntDWord.Checked then
        include(vt,vtDWord);
      if self.frmSystemSet.CheckBoxTypeIntDDWord.Checked then
        include(vt,vtDDWord);
      if vt=[] then
      begin
        ErrorText:='整数监视类型';
        ErrorTextEX:='整数监视类型不能为空';
      end;
      ts.WatchTypeInt:=vt;

      vt:=[];
      if self.frmSystemSet.CheckBoxTypeFloatSingle.Checked then
        include(vt,vtSingle);
      if self.frmSystemSet.CheckBoxTypeFloatDouble.Checked then
        include(vt,vtDouble);
      if self.frmSystemSet.CheckBoxTypeFloatExtended.Checked then
        include(vt,vtLDouble);
      if vt=[] then
      begin
        ErrorText:='浮点数监视类型';
        ErrorTextEX:='浮点数监视类型不能为空';
      end;
      ts.WatchTypeFloat:=vt;

      vt:=[];
      if self.frmSystemSet.CheckBoxTypeStringAString.Checked then
        include(vt,vtAString);
      if self.frmSystemSet.CheckBoxTypeStringWString.Checked then
        include(vt,vtWString);
      if vt=[] then
      begin
        ErrorText:='字符串监视类型';
        ErrorTextEX:='字符串监视类型不能为空';
      end;
      ts.WatchTypeString:=vt;

      vt:=[];
      if self.frmSystemSet.CheckBoxTypeBlurByte.Checked then
        include(vt,vtByte);
      if self.frmSystemSet.CheckBoxTypeBlurWord.Checked then
        include(vt,vtWord);
      if self.frmSystemSet.CheckBoxTypeBlurDWord.Checked then
        include(vt,vtDWord);
      if self.frmSystemSet.CheckBoxTypeBlurDDWord.Checked then
        include(vt,vtDDWord);
      if self.frmSystemSet.CheckBoxTypeBlurSingle.Checked then
        include(vt,vtSingle);
      if self.frmSystemSet.CheckBoxTypeBlurDouble.Checked then
        include(vt,vtDouble);
      if self.frmSystemSet.CheckBoxTypeBlurExtended.Checked then
        include(vt,vtLDouble);
      if vt=[] then
      begin
        ErrorText:='模糊查找监视类型';
        ErrorTextEX:='模糊查找监视类型不能为空';
      end;
      ts.WatchTypeBlur:=vt;

      self.ApplySysSet(xSysSet,ts); //成功提交
      result:=true;
    except
      result:=false;
      if ErrorText='' then
        application.MessageBox('  设置参数时发生错误,参数设置取消!  ','出错',MB_ICONERROR+MB_OK)
      else
        application.MessageBox(Pchar('  项目: "'+ErrorText+'" 填写有误! '+#13+#10
                           +'  ('+ErrorTextEX+')  '),'出错',MB_ICONERROR+MB_OK);
    end;
  end;
end;

procedure TMemEditSysSet.ShowFormToSetAll;
begin
  if self.frmSystemSet<>nil then self.frmSystemSet.Free;
  self.frmSystemSet:=TfrmSystemSet.Create(nil);
  self.frmSystemSet.PSysSet:=Self;
  self.ShowSysSet(self.SysSet);
  self.frmSystemSet.PageControlType.ActivePageIndex:=0;
  self.frmSystemSet.PageControlMain.ActivePageIndex:=0;
  self.frmSystemSet.ShowModal;
  self.frmSystemSet.Free;
  self.frmSystemSet:=nil;
end;

function TMemEditSysSet.ShowFormToSetType(const FindType: TFindType):Boolean;
begin

  if not ((self.SysSet.IsFindAsk) and (FindType in [ftInt,ftFloat,ftString,ftBlur]))  then
  begin
    result:=true;
    exit;
  end;

  if self.frmSystemSet<>nil then self.frmSystemSet.Free;
  self.frmSystemSet:=TfrmSystemSet.Create(nil);
  try
    self.frmSystemSet.PSysSet:=Self;
    self.ShowSysSet(self.SysSet);
    self.frmSystemSet.PageControlMain.ActivePageIndex:=2;
    case FindType of
      ftInt    : self.frmSystemSet.PageControlType.ActivePageIndex:=0;
      ftFloat  : self.frmSystemSet.PageControlType.ActivePageIndex:=1;
      ftString : self.frmSystemSet.PageControlType.ActivePageIndex:=2;
      ftBlur   : self.frmSystemSet.PageControlType.ActivePageIndex:=3;
    end;
    self.frmSystemSet.ShowModal;
    Result:= self.frmSystemSet.IsOkClose;
  finally
    self.frmSystemSet.Free;
    self.frmSystemSet:=nil;
  end;
end;

function TMemEditSysSet.ShowFormToSetModuleSuffix(const ProcessId: THandle): Boolean;
var
  i  : integer;
begin     
  self.ModuleSuffix:='';
  if not self.SysSet.IsModuleSuffixAsk then
  begin
    self.ModuleSuffix:=self.ModuleSuffixToStr(self.SysSet);
    result:=true;
  end
  else
  begin
    if self.frmMemSelect<>nil then self.frmMemSelect.Free;
    self.frmMemSelect:=TfrmMemSelect.Create(nil);
    self.frmMemSelect.ProcessId:=ProcessID;
    self.frmMemSelect.PTSysSet:=self;
    self.RefreshModuleList;
    try
      self.frmMemSelect.ShowModal;
      result:=self.frmMemSelect.IsOkClose;
      
      if Result then
      begin
        self.ModuleSuffix:=ExtractFilePath(Application.ExeName)+MyDLLName+#13+#10;
        for i:=0 to self.frmMemSelect.RxCheckListBoxModuleList.Items.Count-1 do
        begin
         if not self.frmMemSelect.RxCheckListBoxModuleList.Checked[i] then
         begin
           self.ModuleSuffix:=self.ModuleSuffix+#13+#10
             +self.frmMemSelect.RxCheckListBoxModuleList.Items[i];
         end;
        end;
      end;
      
    finally
      self.frmMemSelect.Free;
      self.frmMemSelect:=nil;
    end;
  end;
end;

procedure TMemEditSysSet.RefreshModuleList;
var
  ml      : TModuleList;
  i       : integer;
  index   : integer;
  ModouleName : string;

begin    
  self.frmMemSelect.RxCheckListBoxModuleList.Clear;
  ml:=TModuleList.Create(self.frmMemSelect.ProcessId);
  self.frmMemSelect.RxCheckListBoxModuleList.Visible:=false;
  try
    for i:=0 to ml.Count-1 do
    begin
      ModouleName:=ml.Items[i].szExePath;
      if (length(ModouleName)>1) and
        (uppercase(ModouleName)<>uppercase(ExtractFilePath(Application.ExeName)+MyDLLName))
      then
      begin
        index:=self.frmMemSelect.RxCheckListBoxModuleList.Items.Add(ModouleName
          +'  ('+inttostr(round(ml.Items[i].modBaseSize/1024.0))+'K)');
        self.frmMemSelect.RxCheckListBoxModuleList.Checked[Index]:=not self.ToolIsFileSuffixInFiltrateSuffix(ModouleName);
      end;
    end;
  finally
    self.frmMemSelect.RxCheckListBoxModuleList.Visible:=true;
    ml.Free;
  end;
end;

function TMemEditSysSet.ToolGetFileNameSuffix(
  const FileName: string): string;
var
  i : integer;
begin
  for i:=length(FileName) downto 1 do
  begin
    if FileName[i]='.' then
    begin
      result:=copy(FileName,i+1,length(FileName)-i);
      exit;
    end;
  end;
  result:='';
end;

function TMemEditSysSet.ToolIsFileSuffixInFiltrateFileName(
  const FileName: string): Boolean;
var
  List  : string;
  i    : integer;
  st      : string;
begin
  st:=FileName;
  List:=self.ModuleSuffix;

  if (st='') or (List='') then
  begin
    result:=false;
    exit;
  end
  else
  begin
    st:=Uppercase(st);
    List:=uppercase(List+#13+#10
                    +ExtractFilePath(Application.ExeName)+MyDLLName+#13+#10);
    i:=Pos(st,List);
    result:=false;
    while i<>0 do
    begin
      if  ((i=1) or(List[i-1] in [';',' ',#13,#10,#0]))
        and (List[i+length(st)] in [';',' ',#13,#10,#0]) then
      begin
        result:=true;
        break;
      end
      else
      begin
        List:=copy(List,i+1,Length(List)-i);
        i:=Pos(st,List);
      end;
    end;
  end;

end;

var
  SysPath : string='';
function TMemEditSysSet.ToolIsFileSuffixInFiltrateSuffix(
  const FileName: string): Boolean;
var
  List  : string;
  i,L   : integer;
  st      : string;
begin
  if (uppercase(FileName)=uppercase(ExtractFilePath(Application.ExeName)+MyDLLName)) then
  begin
    result:=true;
    exit;
  end;

  if self.SysSet.FiltrateSet=fsNo then
    result:=false
  else
  begin
    st:=self.ToolGetFileNameSuffix(FileName);
    if self.ModuleSuffix='' then
      self.ModuleSuffix:=self.ModuleSuffixToStr(self.SysSet);
    List:=self.ModuleSuffix;
    if (st='') or (List='') then
    begin
      result:=false;
      exit;
    end
    else
    begin
      st:=Uppercase(st);
      List:=uppercase(List);
      i:=Pos(st,List);
      result:=false;
      while i<>0 do
      begin
        if ((i=1) or(List[i-1]='.')) and (List[i+length(st)] in [';',' ',#13,#10,#0]) then
        begin
          result:=true;
          break;
        end
        else
        begin
          List:=copy(List,i+1,Length(List)-i);
          i:=Pos(st,List);
        end;
      end;
    end;

    if result and (self.SysSet.FiltrateSet=fsSYS) then
    begin
      if SysPath='' then
      begin
        setlength(SysPath,260);
        windows.GetSystemDirectory(Pchar(SysPath),260);
        setlength(SysPath,strlen(PChar(SysPath)));
        SysPath:=uppercase(SysPath);
        L:=Length(SysPath);
        if SysPath[L]<>'\' then begin SysPath:=SysPath+'\'; inc(L); end;
      end
      else
        L:=length(SysPath);
      if (length(FileName)<=L) or
        (SysPath<>uppercase(copy(FileName,1,L))) then
        result:=false;
    end;

  end;
end;


initialization
  windows.GetSystemInfo(SysInfo);
  math.SetPrecisionMode(pmExtended);
end.
