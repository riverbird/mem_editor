
unit TLHELP32_Hss;

//******************************************************************************/
//*                                                                             *
//* TLHELP32_Hss.pas -  WIN32 tool help functions, types, and definitions       *
//*                                                                             *
//* Version 1.0                                                                 *
//*                                                                             *
//* 作用:                                                                       *
//*    列举 系统进程、系统线程、进程模块 、进程堆(堆块)                         *
//*                                                                             *
//* NOTE:                                                                       *
//*   HelpAPI -- TLHELP32_Hss.pas 由 侯思松  2003.2.22 完成Delphi引入声明单元   *
//*      邮箱: HouSisong@263.net ,  欢迎使用 :)                                 *
//*                                                                             *
//*   所有函数封装为                                                            *
//*      TSnapshot      : 通用系统快照类                                        *
//*   另外单独封装了:                                                           *
//*      TProcessList   : 系统进程列表类      (用于列举系统所有进程)            *
//*      TThreadList    : 系统线程列表类      (用于列举系统所有线程             *
//*                                            或某进程的线程)                  *
//*      TModuleList    : 进程模块列表类      (用于列举进程所有模块)            *
//*      THeapList      : 进程堆列表类        (用于列举进程的所有堆)            *
//*      THeapsHeapList : 进程堆的堆块列表类  (用于列举进程中某个               *
//*                                            堆的所有堆块或进程的所有堆块)    *
//*                                                                             *
//*   (  做完这个单元以后发现Delphi自带了一个TlHelp32.pas , 郁闷 :(       )     *
//*   (  不过我的好像封装得 更清晰一些、有中文帮助、还封装成了几个类:)    )     *
//*                                                                             *
//* 适用于:                                                                     *
//*    Win95,Win98,WinMe,Win2000,WinXP  (NT不支持)                              *
//*                                                                             *
//*                        (2003.3.10更新)                                      *
//*                                                                             *
//******************************************************************************/


interface

uses Windows, SysUtils, Classes;

  const MAX_MODULE_NAME32 = 255 ;
  type  HANDLE   = integer;
  type  LPCVOID  = Pointer;
  type  LPVOID   = Pointer;
  type  LONG     = integer;

//****** Shapshot function **********************************************/

  function CreateToolhelp32Snapshot(
    dwFlags         : DWORD ;
    th32ProcessID   : DWORD
    ):Handle;stdcall;

  type
    TCreateToolhelp32Snapshot =function (
      dwFlags         : DWORD ;
      th32ProcessID   : DWORD
      ):Handle;stdcall;

  function CloseToolhelp32SnapshotHandle(hSnapshot :Handle):BOOL; stdcall; //关闭 Toolhelp32Snapshot快照句柄

//// dwFlags
  const  TH32CS_SNAPHEAPLIST = $00000001;
  const  TH32CS_SNAPPROCESS  = $00000002;
  const  TH32CS_SNAPTHREAD   = $00000004;
  const  TH32CS_SNAPMODULE   = $00000008;
  const  TH32CS_SNAPALL      = (TH32CS_SNAPHEAPLIST or TH32CS_SNAPPROCESS
                              or TH32CS_SNAPTHREAD or TH32CS_SNAPMODULE);
  const  TH32CS_INHERIT      = $80000000;
//
//// Use CloseHandle to destroy the snapshot
//
  {
    dwFlags       参数指定快照包含的信息类型，可以是下列值之一：
                    值                        含义
                    TH32CS_INHERIT            表示快照的句柄是可继承的
                    TH32CS_SNAPALL            相当于同时指定TH32CS_SNAPHEAPLIST、TH32CS_SNAPMODULE、
                                                  TH32CS_SNAPPROCESS和TH32CS_SNAPTHREAD
                    TH32CS_SNAPHEAPLIST       快照中包含指定的Win32进程的堆列表
                    TH32CS_SNAPMODULE         快照中包含指定的Win32进程的模块列表
                    TH32CS_SNAPPROCESS        快照中包含Win32进程的列表
                    TH32CS_SNAPTHREAD         快照中包含Win32线程的列表
    th32ProcessID 参数标识要获取信息的进程，设为0标识当前进程。这个参数只影响模块和堆的列
                      表，因为它们与进程相关的。进程和线程列表是系统范围的。
    CreateToolhelp32Snapshot()  函数返回快照句柄，如果出错则返回-1。该句柄和其他Win32句柄一
                                    样决定进程和线程是否有效。
  }

//****** heap walking ***************************************************/

type tagHEAPLIST32=record
    dwSize        : DWORD ;
    th32ProcessID : DWORD ;   // owning process
    th32HeapID    : DWORD ;      // heap (in owning process's context!)
    dwFlags       : DWORD ;
  end;
  type HEAPLIST32   = tagHEAPLIST32;
       PHEAPLIST32  =^HEAPLIST32;
       LPHEAPLIST32 =^HEAPLIST32;
//
// dwFlags
//
  const  HF32_DEFAULT   =   1;  // process's default heap
  const  HF32_SHARED    =   2;  // is shared heap
  {
    dwSize          是记录大小，在使用记录之前应初始化为SizeOf(THeapList32)。
    th32ProcessID   是所在进程的标识号。
    th32HeapID      是堆标识号，只对ToolHelp32中指定的进程有效。
    dwFlags         是一个标志，决定堆的类型。这个域有两个值，为HF32_DEFAULT，表示当前堆是进程
                        默认堆；为HF32_SHARED，表示当前堆是普通共享堆。
  }

  function Heap32ListFirst(hSnapshot :Handle; lphl : LPHEAPLIST32):BOOL;stdcall;
  function Heap32ListNext (hSnapshot :Handle; lphl : LPHEAPLIST32):BOOL;stdcall;

  type
    THeap32ListFirst = function (hSnapshot :Handle; lphl : LPHEAPLIST32):BOOL;stdcall;
    THeap32ListNext  = function (hSnapshot :Handle; lphl : LPHEAPLIST32):BOOL;stdcall;


type tagHEAPENTRY32 = record
    dwSize        : DWORD  ;
    hHandle       : HANDLE ;     // Handle of this heap block
    dwAddress     : DWORD  ;   // Linear address of start of block
    dwBlockSize   : DWORD  ; // Size of block in bytes
    dwFlags       : DWORD  ;
    dwLockCount   : DWORD  ;
    dwResvd       : DWORD  ;
    th32ProcessID : DWORD  ;   // owning process
    th32HeapID    : DWORD  ;      // heap block is in
  end;
  type  HEAPENTRY32   = tagHEAPENTRY32;
        PHEAPENTRY32  = ^HEAPENTRY32 ;
        LPHEAPENTRY32 = ^HEAPENTRY32 ;
//
// dwFlags
//
  const  LF32_FIXED    = $00000001 ;
  const  LF32_FREE     = $00000002 ;
  const  LF32_MOVEABLE = $00000004 ;

  {
    dwSize          是记录大小，应在使用记录前初始化为SizeOf(THeapEntry32)。
    hHandle         是堆块的句柄。
    dwAddress       是堆块的起始线性地址。
    dwBlockSize     是堆块的字节数。
    dwFlags         指定堆块的类型，可以是下面表中的任一值：
                              值                  含义
                              LF32_FIXED          内存块地址固定
                              LF32_FREE           内存块空闲
                              LF32_MOVEABLE       内存块地址可移动
    dwLockCount     是内存块的锁定计数。进程每次对该块调用GlobalLock()或LocalLock()时，锁定计数加1。
    dwResvd         目前保留未用。
    th32ProcessID   是所在进程的标识号。
    th32HeapID      是块所在堆的标识号。
  }

  function  Heap32First( lphe :LPHEAPENTRY32 ;th32ProcessID : DWORD;th32HeapID : DWORD ):BOOL;stdcall;
  function  Heap32Next ( lphe :LPHEAPENTRY32 ):BOOL;stdcall;

  type
    THeap32First= function  ( lphe :LPHEAPENTRY32 ;th32ProcessID : DWORD;th32HeapID : DWORD ):BOOL;stdcall;
    THeap32Next = function   ( lphe :LPHEAPENTRY32 ):BOOL;stdcall;


  function  Toolhelp32ReadProcessMemory (
    th32ProcessID         : DWORD   ;
    lpBaseAddress         : LPCVOID  ;  //
    lpBuffer              : LPVOID  ;
    cbRead                : DWORD   ;
    lpNumberOfBytesRead   : LPDWORD
    ):BOOL;stdcall;

  type
    TToolhelp32ReadProcessMemory = function   (
      th32ProcessID         : DWORD   ;
      lpBaseAddress         : LPCVOID  ;  //
      lpBuffer              : LPVOID  ;
      cbRead                : DWORD   ;
      lpNumberOfBytesRead   : LPDWORD
      ):BOOL;stdcall;

//***** Process walking *************************************************/

type  tagPROCESSENTRY32W = record
    dwSize               : DWORD   ;
    cntUsage             : DWORD   ;
    th32ProcessID        : DWORD   ;          // this process
    th32DefaultHeapID    : DWORD   ;
    th32ModuleID         : DWORD   ;           // associated exe
    cntThreads           : DWORD   ;
    th32ParentProcessID  : DWORD   ;    // this process's parent process
    pcPriClassBase       : LONG    ;         // Base priority of process's threads
    dwFlags              : DWORD   ;
    szExeFile            : array [0..MAX_PATH-1] of WCHAR   ;    // Path
  end;
  type  PROCESSENTRY32W    = tagPROCESSENTRY32W;
        PPROCESSENTRY32W   = ^PROCESSENTRY32W;
        LPPROCESSENTRY32W  = ^PROCESSENTRY32W ;


  function  Process32FirstW ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32W):BOOL;stdcall;
  function  Process32NextW  ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32W):BOOL;stdcall;

  type
    TProcess32FirstW = function   ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32W):BOOL;stdcall;
    TProcess32NextW  = function   ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32W):BOOL;stdcall;

type tagPROCESSENTRY32 = record
    dwSize               :  DWORD   ;
    cntUsage             :  DWORD   ;
    th32ProcessID        :  DWORD   ;          // this process
    th32DefaultHeapID    :  DWORD   ;
    th32ModuleID         :  DWORD   ;           // associated exe
    cntThreads           :  DWORD   ;
    th32ParentProcessID  :  DWORD   ;    // this process's parent process
    pcPriClassBase       :  LONG    ;         // Base priority of process's threads
    dwFlags              :  DWORD   ;
    szExeFile            :  array [0..MAX_PATH-1] of CHAR   ;    // Path
  end;
  type  PROCESSENTRY32    = tagPROCESSENTRY32;
        PPROCESSENTRY32   = ^PROCESSENTRY32;
        LPPROCESSENTRY32  = ^PROCESSENTRY32;

  {
    dwSize             域是TProcessEntry32记录的大小，应在使用记录前初始化为SizeOf(TProcessEntry32)。
    cntUsage           域是进程的引用计数。当引用计数为0时，操作系统就会卸载这个进程。
    th32ProcessID      域是进程的标识号。
    th32DefaultHeapID  域是进程默认堆的标识号，只在ToolHelp32中有意义，不能用于其他Win32函数。
    thModuleID         域是进程模块标识号，仅在ToolHelp32函数中有意义。
    cntThreads         域表示进程已启动了多少线程。
    th32ParentProcessID标识了本进程的父进程。
    pcPriClassBase     域指定进程的基优先级，操作系统据此来调度线程。
    dwFlags            域是保留的，目前未用。
    szExeFile          域是一个以null结束的字符串，包含进程的EXE文件(或驱动程序)的路径名和文件名。
                           一旦获得包含进程信息的快照，就可以调用Process32First()，继而调用Process32Next()来遍历所
                           有进程，直到Process32Next()返回False为止。
  }


  function  Process32First  ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32):BOOL;stdcall;
  function  Process32Next   ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32):BOOL;stdcall;

  type
    TProcess32First = function    ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32):BOOL;stdcall;
    TProcess32Next  = function    ( hSnapshot : HANDLE; lppe  : LPPROCESSENTRY32):BOOL;stdcall;


//***** Thread walking **************************************************/

type tagTHREADENTRY32 = record
    dwSize              :   DWORD   ;
    cntUsage            :   DWORD   ;
    th32ThreadID        :   DWORD   ;       // this thread
    th32OwnerProcessID  :   DWORD   ; // Process this thread is associated with
    tpBasePri           :   LONG    ;
    tpDeltaPri          :   LONG    ;
    dwFlags             :   DWORD   ;
  end;
  type  THREADENTRY32  = tagTHREADENTRY32;
        PTHREADENTRY32 = ^THREADENTRY32;
        LPTHREADENTRY32= ^THREADENTRY32;

  {
    dwSize              是记录大小，在使用记录之前应初始化为SizeOf(TThreadEntry32)。
    cntUsage            是线程的引用计数，为0时，线程被操作系统卸载。
    th32ThreadID        是线程的标识号，只有在ToolHelp32函数中有效。
    th32OwnerProcessID  是线程所在进程的标识号，可被其他Win32函数使用。
    tpBasePri           是线程的基优先级类。对于一个给定进程中的所有线程，该值相同。这个域的值通常
                            在4到24之间，各值的含义如下：
                              值    含义            值          含义
                              4     Idle            13          High
                              8     Normal          24          Realtime
    thDeltaPri          也是优先级修改量，它与基优先级类组合形成线程的全面优先级。其值是下表中的常量：
                              常量                              值
                              THREAD_PRIORITY_IDLE              -15
                              THREAD_PRIORITY_LOWEST            -2
                              THREAD_PRIORITY_BELOW_NORMAL      -1
                              THREAD_PRIORITY_NORMAL            0
                              THREAD_PRIORITY_ABOVE_NORMAL      1
                              THREAD_PRIORITY_HIGHEST           2
                              THREAD_PRIORITY_TIME_CRITICAL     15
    dwFlags             目前保留未用。
  }
  
  function  Thread32First  ( hSnapshot : HANDLE; lpte  : LPTHREADENTRY32):BOOL;stdcall;
  function  Thread32Next   ( hSnapshot : HANDLE; lpte  : LPTHREADENTRY32):BOOL;stdcall;

  type
    TThread32First = function    ( hSnapshot : HANDLE; lpte  : LPTHREADENTRY32):BOOL;stdcall;
    TThread32Next  = function    ( hSnapshot : HANDLE; lpte  : LPTHREADENTRY32):BOOL;stdcall;



//***** Module walking *************************************************/

type tagMODULEENTRY32W = record
    dwSize           : DWORD   ;
    th32ModuleID     : DWORD   ;       // This module
    th32ProcessID    : DWORD   ;      // owning process
    GlblcntUsage     : DWORD   ;       // Global usage count on the module
    ProccntUsage     : DWORD   ;       // Module usage count in th32ProcessID's context
    modBaseAddr      : PBYTE   ;        // Base address of module in th32ProcessID's context
    modBaseSize      : DWORD   ;        // Size in bytes of module starting at modBaseAddr
    hModule          : HMODULE ;            // The hModule of this module in th32ProcessID's context
    szModule         : array [0..MAX_MODULE_NAME32+1-1] of WCHAR      ;
    szExePath        : array [0..MAX_PATH-1] of WCHAR         ;
  end;
  type   MODULEENTRY32W   = tagMODULEENTRY32W;
         PMODULEENTRY32W  = ^MODULEENTRY32W   ;
         LPMODULEENTRY32W = ^MODULEENTRY32W   ;


  function  Module32FirstW   ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32W):BOOL;stdcall;
  function  Module32NextW    ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32W):BOOL;stdcall;

  type
    TModule32FirstW = function     ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32W):BOOL;stdcall;
    TModule32NextW  = function     ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32W):BOOL;stdcall;

type  tagMODULEENTRY32 = record
     dwSize               :  DWORD     ;
     th32ModuleID         :  DWORD     ;       // This module
     th32ProcessID        :  DWORD           ;      // owning process
     GlblcntUsage         :  DWORD       ;       // Global usage count on the module
     ProccntUsage         :  DWORD         ;       // Module usage count in th32ProcessID's context
     modBaseAddr          :  PBYTE   ;        // Base address of module in th32ProcessID's context
     modBaseSize          :  DWORD       ;        // Size in bytes of module starting at modBaseAddr
     hModule              :  HMODULE         ;            // The hModule of this module in th32ProcessID's context
     szModule             :  array [0..MAX_MODULE_NAME32 + 1-1] of char       ;
     szExePath            :  array [0..MAX_PATH-1] of char      ;
  end;
  type MODULEENTRY32    = tagMODULEENTRY32;
       PMODULEENTRY32   = ^MODULEENTRY32   ;
       LPMODULEENTRY32  = ^MODULEENTRY32   ;


  {
    dwSize          是记录大小，在使用记录之前应初始化为SizeOf(TModuleEntry32)。
    th32ModuleID    是模块标识符，只在ToolHelp32函数中有意义。
    th32ProcessID   是需要检查的进程的标识符，其值可用于其他Win32函数。
    GlblcntUsage    是模块的全局引用计数。
    ProccntUsage    是模块在所属进程环境中的引用计数。
    modBaseAddr     是模块在内存中的基地址。其值只在th32ProcessID指定的进程中有效。
    modBaseSize     是模块在内存中的字节数。
    hModule         是模块句柄，只在th32ProcessID指定的进程中有效。
    szModule        是一个以null结束的字符串，包含模块名称。
    szExePath       是一个以null结束的字符串，包含模块的完整路径。
  }


//
// NOTE CAREFULLY that the modBaseAddr and hModule fields are valid ONLY
// in th32ProcessID's process context.
//


  function  Module32First   ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32):BOOL;stdcall;
  function  Module32Next    ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32):BOOL;stdcall;

  type
    TModule32First = function     ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32):BOOL;stdcall;
    TModule32Next  = function     ( hSnapshot : HANDLE; lpme  : LPMODULEENTRY32):BOOL;stdcall;



type
  //////////////////////////////////////////////////////////////////////////////
  //        TSnapshot   进程、线程、堆、模块 列举类                           //
  //////////////////////////////////////////////////////////////////////////////
  TSnapshot = class(TObject)
  private
     FHandle  : Handle;
     FdwFlags : DWord;
    function GetIsInheritable: Boolean;

  public
    property Handle :Handle read FHandle;                     // TSnapshot句柄
    property IsInheritable :Boolean read GetIsInheritable;    // 是否可继承
    
    function Heap32ListFirst(var hl : HEAPLIST32):Boolean;
    function Heap32ListNext (var hl : HEAPLIST32):Boolean;
    function Heap32First    (var he : HEAPENTRY32 ;const th32ProcessID : DWORD;const th32HeapID : DWORD ):Boolean;
    function Heap32Next     (var he : HEAPENTRY32 ):Boolean;

    function Process32First (var pe  : PROCESSENTRY32):Boolean;
    function Process32Next  (var pe  : PROCESSENTRY32):Boolean;
    function Process32FirstW(var pe  : PROCESSENTRY32W):Boolean;
    function Process32NextW (var pe  : PROCESSENTRY32W):Boolean;

    function Thread32First  (var te  : THREADENTRY32):Boolean;
    function Thread32Next   (var te  : THREADENTRY32):Boolean;

    function Module32First  (var me  : MODULEENTRY32):Boolean;
    function Module32Next   (var me  : MODULEENTRY32):Boolean;
    function Module32FirstW (var me  : MODULEENTRY32W):Boolean;
    function Module32NextW  (var me  : MODULEENTRY32W):Boolean;
  public
    constructor Create(dwFlags: DWORD ;th32ProcessID: DWORD=0); overload;
    { dwFlags :=
        TH32CS_INHERIT	      // Indicates that the snapshot handle is to be inheritable.
        TH32CS_SNAPALL	      // Equivalent to specifying the TH32CS_SNAPHEAPLIST, TH32CS_SNAPMODULE, TH32CS_SNAPPROCESS, and TH32CS_SNAPTHREAD values.
        TH32CS_SNAPHEAPLIST	  // Includes the heap list of the specified  process in the snapshot.
        TH32CS_SNAPMODULE	    // Includes the module list of the specified  process in the snapshot.
        TH32CS_SNAPPROCESS	  // Includes the Win32 process list in the snapshot.
        TH32CS_SNAPTHREAD	    // Includes the Win32 thread list in the snapshot.
        Note: if dwFlags = TH32CS_SNAPHEAPLIST or dwFlags = TH32CS_SNAPMODULE then the th32ProcessID <> 0;
    }
    destructor  Destroy(); override;
  end;

type
  // 系统进程列表 类
  TProcessList = class (TObject)
  private
    FList     : array of PROCESSENTRY32;
    function GetCount: integer;
    function GetListValue(const Index: integer): PROCESSENTRY32;

  public
    property  Count : integer read GetCount;
    property  Items[const Index:integer] : PROCESSENTRY32 read GetListValue;
    procedure GetList();

    function  GetProcessID(const ProcessExeName : string):THandle;
    function  GetProcessExeName(const ProcessID : THandle):string;

  public
    constructor Create();
    destructor  Destroy(); override;
  end;

type
  // 系统线程列表 类
  TThreadList = class (TObject)
  private
    FList     : array of THREADENTRY32;
    function GetCount: integer;
    function GetListValue(const Index: integer): THREADENTRY32;

  public
    property  Count : integer read GetCount;
    property  Items[const Index:integer] : THREADENTRY32 read GetListValue;
    procedure GetList();     overload;
    procedure GetList(const ProcessID : THandle);   overload;

  public
    constructor Create(); overload;
    constructor Create(const ProcessID : THandle); overload;
    destructor  Destroy(); override;
  end;

type
  // 进程模块列表 类
  TModuleList = class (TObject)
  private
    FList     : array of MODULEENTRY32;
    function  GetCount: integer;
    function  GetListValue(const Index: integer): MODULEENTRY32;
    procedure SetListValue(const Index: integer;
                 const Value: MODULEENTRY32);

  public
    property  Count : integer read GetCount;
    property  Items[const Index:integer] : MODULEENTRY32 read GetListValue write SetListValue;
    procedure GetList(const ProcessID: THandle);

  public
    constructor Create(const ProcessID: THandle); overload;
    destructor  Destroy(); override;
  end;


type
  {一个进程可能有很多个"堆",每个堆又可能包括很多个"堆块",所以这里有两个关于堆的类}

  // 进程堆的堆块列表 类   (用于列举进程中某个堆的堆块)
  THeapsHeapList = class;

  // 进程堆列表 类         (用于列举进程的所有堆)
  THeapList = class (TObject)
  private
    FList     : array of HEAPLIST32;
    function GetCount: integer;
    function GetListValue(const Index: integer): HEAPLIST32;

  public
    property  Count : integer read GetCount;
    property  Items[const Index:integer] : HEAPLIST32 read GetListValue;
    procedure GetList(const ProcessID : THandle);

  public
    constructor Create(const ProcessID: THandle); overload;
    destructor  Destroy(); override;
  end;

  // 进程堆的堆块列表 类
  THeapsHeapList = class (TObject)
  private
    FList       : array of HEAPENTRY32;
    function GetCount: integer;
    function GetListValue(const Index: integer): HEAPENTRY32;

  public
    property  Count : integer read GetCount;
    property  Items[const Index:integer] : HEAPENTRY32 read GetListValue;
    procedure GetList(const ProcessID: THandle); overload;
    procedure GetList(const ProcessID: THandle;const HeapHandle: THandle); overload;

  public
    constructor Create(const ProcessID: THandle; const HeapHandle: THandle); overload;
    constructor Create(const ProcHeap : HEAPLIST32); overload;
    destructor  Destroy(); override;
  end;


implementation

const
  kernel32dll  = kernel32;

var
  _kernel32dllHandle            : THandle=0;
  
  _CreateToolhelp32Snapshot     : Pointer=nil;
  _Toolhelp32ReadProcessMemory  : Pointer=nil;
  _Heap32ListFirst              : Pointer=nil;
  _Heap32ListNext               : Pointer=nil;
  _Heap32First                  : Pointer=nil;
  _Heap32Next                   : Pointer=nil;
  _Process32FirstW              : Pointer=nil;
  _Process32NextW               : Pointer=nil;
  _Process32First               : Pointer=nil;
  _Process32Next                : Pointer=nil;
  _Thread32First                : Pointer=nil;
  _Thread32Next                 : Pointer=nil;
  _Module32FirstW               : Pointer=nil;
  _Module32NextW                : Pointer=nil;
  _Module32First                : Pointer=nil;
  _Module32Next                 : Pointer=nil;


function CreateToolhelp32Snapshot;
begin
  if _CreateToolhelp32Snapshot=nil then
  begin
    _kernel32dllHandle:=0;
    _kernel32dllHandle:=windows.LoadLibrary(kernel32dll);
    if  _kernel32dllHandle<>0 then
    begin
      _CreateToolhelp32Snapshot:=windows.GetProcAddress(_kernel32dllHandle,'CreateToolhelp32Snapshot');
      _Toolhelp32ReadProcessMemory:=windows.GetProcAddress(_kernel32dllHandle,'Toolhelp32ReadProcessMemory');
      _Heap32ListFirst:=windows.GetProcAddress(_kernel32dllHandle,'Heap32ListFirst');
      _Heap32ListNext:=windows.GetProcAddress(_kernel32dllHandle,'Heap32ListNext');
      _Heap32First:=windows.GetProcAddress(_kernel32dllHandle,'Heap32First');
      _Heap32Next:=windows.GetProcAddress(_kernel32dllHandle,'Heap32Next');
      _Process32FirstW:=windows.GetProcAddress(_kernel32dllHandle,'Process32FirstW');
      _Process32NextW:=windows.GetProcAddress(_kernel32dllHandle,'Process32NextW');
      _Process32First:=windows.GetProcAddress(_kernel32dllHandle,'Process32First');
      _Process32Next:=windows.GetProcAddress(_kernel32dllHandle,'Process32Next');
      _Thread32First:=windows.GetProcAddress(_kernel32dllHandle,'Thread32First');
      _Thread32Next:=windows.GetProcAddress(_kernel32dllHandle,'Thread32Next');
      _Module32FirstW:=windows.GetProcAddress(_kernel32dllHandle,'Module32FirstW');
      _Module32NextW:=windows.GetProcAddress(_kernel32dllHandle,'Module32NextW');
      _Module32First:=windows.GetProcAddress(_kernel32dllHandle,'Module32First');
      _Module32Next:=windows.GetProcAddress(_kernel32dllHandle,'Module32Next');
    end;
  end;
  if _CreateToolhelp32Snapshot<>nil then
    result:=TCreateToolhelp32Snapshot(_CreateToolhelp32Snapshot)(dwFlags, th32ProcessID)
  else
    result:=0;
end;

function CloseToolhelp32SnapshotHandle(hSnapshot :Handle):BOOL;  stdcall;
begin
  result:=windows.CloseHandle(hSnapshot);
end;

function Heap32ListFirst;
begin
  if _Heap32ListFirst<>nil then
    result:=THeap32ListFirst(_Heap32ListFirst)(hSnapshot, lphl)
  else
    result:=false;
end;

function Heap32ListNext;
begin
  if _Heap32ListNext<>nil then
    result:=THeap32ListNext(_Heap32ListNext)(hSnapshot, lphl)
  else
    result:=false;
end;

function  Heap32First;
begin
  if _Heap32First<>nil then
    result:=THeap32First(_Heap32First)(lphe,th32ProcessID ,th32HeapID)
  else
    result:=false;
end;

function  Heap32Next;
begin
  if _Heap32Next<>nil then
    result:=THeap32Next(_Heap32Next)(lphe)
  else
    result:=false;
end;

function Toolhelp32ReadProcessMemory;
begin
  if _Toolhelp32ReadProcessMemory<>nil then
    Result := TToolhelp32ReadProcessMemory(_Toolhelp32ReadProcessMemory)(th32ProcessID, lpBaseAddress,
      lpBuffer, cbRead, lpNumberOfBytesRead)
  else Result := False;
end;

function  Process32FirstW;
begin
  if _Process32FirstW<>nil then
    result:=TProcess32FirstW(_Process32FirstW)(hSnapshot,lppe)
  else
    result:=false;
end;

function  Process32NextW;
begin
  if _Process32NextW<>nil then
    result:=TProcess32NextW(_Process32NextW)(hSnapshot,lppe)
  else
    result:=false;
end;

function  Process32First;
begin
  if _Process32First<>nil then
    result:=TProcess32First(_Process32First)(hSnapshot,lppe)
  else
    result:=false;
end;

function  Process32Next;
begin
  if _Process32Next<>nil then
    result:=TProcess32Next(_Process32Next)(hSnapshot,lppe)
  else
    result:=false;
end;

function  Thread32First;
begin
  if _Thread32First<>nil then
    result:=TThread32First(_Thread32First)(hSnapshot,lpte)
  else
    result:=false;
end;

function  Thread32Next;
begin
  if _Thread32Next<>nil then
    result:=TThread32Next(_Thread32Next)(hSnapshot,lpte)
  else
    result:=false;
end;

function  Module32FirstW;
begin
  if _Module32FirstW<>nil then
    result:=TModule32FirstW(_Module32FirstW)(hSnapshot,lpme)
  else
    result:=false;
end;

function  Module32NextW;
begin
  if _Module32NextW<>nil then
    result:=TModule32NextW(_Module32NextW)(hSnapshot,lpme)
  else
    result:=false;
end;


function  Module32First;
begin
  if _Module32First<>nil then
    result:=TModule32First(_Module32First)(hSnapshot,lpme)
  else
    result:=false;
end;

function  Module32Next;
begin
  if _Module32Next<>nil then
    result:=TModule32Next(_Module32Next)(hSnapshot,lpme)
  else
    result:=false;
end;



{ Snapshot }

constructor TSnapshot.Create(dwFlags: DWORD ;th32ProcessID: DWORD=0);
begin
  inherited Create;
  self.FHandle:=-1;
  if ((dwFlags=TH32CS_SNAPHEAPLIST) or (dwFlags=TH32CS_SNAPMODULE)) and (th32ProcessID=0) then
    exit
  else
  begin
    self.FHandle:=CreateToolhelp32Snapshot(dwFlags,th32ProcessID);
    FdwFlags:=dwFlags;
  end;
end;

destructor TSnapshot.Destroy;
begin
  if self.FHandle<>-1 then
  begin
    CloseToolhelp32SnapshotHandle(self.FHandle);
  end;
  inherited Destroy;
end;

function TSnapshot.Heap32First(var he : HEAPENTRY32 ;const th32ProcessID,
  th32HeapID: DWORD): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPHEAPLIST <>0 then
    result:=TLHELP32_Hss.Heap32First(@he,th32ProcessID,th32HeapID)
  else
    result:=false;
end;

function TSnapshot.Heap32Next(var he : HEAPENTRY32 ): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPHEAPLIST <>0 then
    result:=TLHELP32_Hss.Heap32Next(@he)
  else
    result:=false;
end;

function TSnapshot.Heap32ListFirst(var hl: HEAPLIST32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPHEAPLIST <>0 then
    result:=TLHELP32_Hss.Heap32ListFirst(self.Handle,@hl)
  else
    result:=false;
end;

function TSnapshot.Heap32ListNext(var hl: HEAPLIST32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPHEAPLIST <>0 then
    result:=TLHELP32_Hss.Heap32ListNext(self.FHandle,@hl)
  else
    result:=false;
end;

function TSnapshot.Module32First(var me: MODULEENTRY32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPMODULE <>0 then
    result:=TLHELP32_Hss.Module32First(self.FHandle,@me)
  else
    result:=false;
end;

function TSnapshot.Module32FirstW(var me: MODULEENTRY32W): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPMODULE <>0 then
    result:=TLHELP32_Hss.Module32FirstW(self.FHandle,@me)
  else
    result:=false;
end;

function TSnapshot.Module32Next(var me: MODULEENTRY32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPMODULE <>0 then
    result:=TLHELP32_Hss.Module32Next(self.FHandle,@me)
  else
    result:=false;
end;

function TSnapshot.Module32NextW(var me: MODULEENTRY32W): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPMODULE <>0 then
    result:=TLHELP32_Hss.Module32NextW(self.FHandle,@me)
  else
    result:=false;
end;

function TSnapshot.Process32First(var pe: PROCESSENTRY32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPPROCESS	 <>0 then
    result:=TLHELP32_Hss.Process32First(self.FHandle,@pe)
  else
    result:=false;
end;

function TSnapshot.Process32FirstW(var pe: PROCESSENTRY32W): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPPROCESS	 <>0 then
    result:=TLHELP32_Hss.Process32FirstW(self.FHandle,@pe)
  else
    result:=false;
end;

function TSnapshot.Process32Next(var pe: PROCESSENTRY32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPPROCESS	 <>0 then
    result:=TLHELP32_Hss.Process32Next(self.FHandle,@pe)
  else
    result:=false;
end;

function TSnapshot.Process32NextW(var pe: PROCESSENTRY32W): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPPROCESS	<>0 then
    result:=TLHELP32_Hss.Process32NextW(self.FHandle,@pe)
  else
    result:=false;
end;

function TSnapshot.Thread32First(var te: THREADENTRY32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPTHREAD	 <>0 then
    result:=TLHELP32_Hss.Thread32First(self.FHandle,@te)
  else
    result:=false;
end;

function TSnapshot.Thread32Next(var te: THREADENTRY32): Boolean;
begin
  if self.FdwFlags and TH32CS_SNAPTHREAD	 <>0 then
    result:=TLHELP32_Hss.Thread32Next(self.FHandle,@te)
  else
    result:=false;
end;

function TSnapshot.GetIsInheritable: Boolean;
begin
  result:= (self.FdwFlags and TH32CS_INHERIT)	<>0;
end;

{ TProcessList }

constructor TProcessList.Create;
begin
  inherited;
  self.GetList;
end;

destructor TProcessList.Destroy;
begin
  inherited;
end;

function TProcessList.GetCount: integer;
begin
  result:=length(self.FList);
end;

function TProcessList.GetListValue(const Index: integer): PROCESSENTRY32;
begin
  result:=self.FList[Index];
end;

function TProcessList.GetProcessExeName(const ProcessID: THandle): string;
var
  i  : integer;
begin
  for i:=0 to self.Count-1 do
  begin
    if self.FList[i].th32ProcessID=ProcessID then
    begin
      result:=self.FList[i].szExeFile;
      exit;
    end;
  end;
  result:='';
end;

function TProcessList.GetProcessID(const ProcessExeName: string): THandle;
var
  i  : integer;
  strE,strP : string;
begin
  strp:=uppercase(ProcessExeName);
  for i:=0 to self.Count-1 do
  begin
    strE:=uppercase(self.FList[i].szExeFile);
    if strE=strP then
    begin
      result:=self.FList[i].th32ProcessID;
      exit;
    end;
  end;
  result:=0;  //不安全,0为系统进程;而且还有可能:一个进程名称对应多个句柄,这时返回的是查到的第一个匹配进程
end;

procedure TProcessList.GetList;
var
  pe   : PROCESSENTRY32;
  IsOk : LongBool;
  iCount  : integer;
  Snapshot  : TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Snapshot:=TSnapshot.Create(TH32CS_SNAPPROCESS);
  try
    pe.dwSize:=sizeof(PROCESSENTRY32);
    IsOk:=Snapshot.Process32First(pe);
    while IsOk do
    begin
      inc(iCount);
      setlength(self.FList,iCount);
      self.FList[iCount-1]:=pe;
      pe.dwSize:=sizeof(PROCESSENTRY32);
      IsOk:=Snapshot.Process32Next(pe);
    end;
  finally
    Snapshot.Free;
  end;
end;

{ TThreadList }

constructor TThreadList.Create;
begin
  inherited;
  self.GetList;
end;

destructor TThreadList.Destroy;
begin
  inherited;
end;

function TThreadList.GetCount: integer;
begin
  result:=length(self.Flist);
end;

function TThreadList.GetListValue(const Index: integer): THREADENTRY32;
begin
  result:=self.FList[Index];
end;

procedure TThreadList.GetList;
var
  te   : THREADENTRY32;
  IsOk : LongBool;
  iCount  : integer;
  Snapshot:TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Snapshot:=TSnapshot.Create(TH32CS_SNAPTHREAD);
  try
    te.dwSize:=sizeof(THREADENTRY32);
    IsOk:=Snapshot.Thread32First(te);
    while IsOk do
    begin
      inc(iCount);
      setlength(self.FList,iCount);
      self.FList[iCount-1]:=te;
      te.dwSize:=sizeof(THREADENTRY32);
      IsOk:=Snapshot.Thread32Next(te);
    end;
  finally
    Snapshot.Free;
  end;
end;

constructor TThreadList.Create(const ProcessID: THandle);
begin
  inherited Create;
  self.GetList(ProcessID);
end;

procedure TThreadList.GetList(const ProcessID: THandle);
var
  te   : THREADENTRY32;
  IsOk : LongBool;
  iCount  : integer;
  Snapshot:TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Snapshot:=TSnapshot.Create(TH32CS_SNAPTHREAD);
  try
    te.dwSize:=sizeof(THREADENTRY32);
    IsOk:=Snapshot.Thread32First(te);
    while IsOk do
    begin
      if te.th32OwnerProcessID=ProcessID then
      begin
        inc(iCount);
        setlength(self.FList,iCount);
        self.FList[iCount-1]:=te;
      end;
      te.dwSize:=sizeof(THREADENTRY32);
      IsOk:=Snapshot.Thread32Next(te);
    end;
  finally
    Snapshot.Free;
  end;
end;

{ TModuleList }

constructor TModuleList.Create(const ProcessID: THandle);
begin
  inherited Create();
  self.GetList(ProcessID);
end;

destructor TModuleList.Destroy;
begin
  inherited;
end;

function TModuleList.GetCount: integer;
begin
  result:=length(self.FList);
end;

function TModuleList.GetListValue(const Index: integer): MODULEENTRY32;
begin
  result:=self.FList[Index];
end;

procedure TModuleList.GetList(const ProcessID: THandle);
var
  me   : MODULEENTRY32;
  IsOk : LongBool;
  iCount  : integer;
  Length  : integer;
  Snapshot  : TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Length:=32;
  setlength(self.FList,Length);

  Snapshot:= TSnapshot.Create(TH32CS_SNAPMODULE,ProcessID);
  try
    me.dwSize:=sizeof(MODULEENTRY32);
    IsOk:=Snapshot.Module32First(me);
    while IsOk do
    begin
      if iCount<Length then
        self.FList[iCount]:=me
      else
      begin
        inc(Length,32);
        setlength(self.FList,Length);
        self.FList[iCount]:=me;
      end;
      inc(iCount);
      me.dwSize:=sizeof(MODULEENTRY32);
      IsOk:=Snapshot.Module32Next(me);
    end;
  finally
    Snapshot.free;
  end;
end;

procedure TModuleList.SetListValue(const Index: integer;
  const Value: MODULEENTRY32);
begin
  self.FList[Index]:=Value;
end;

{ THeapsHeapList }

constructor THeapsHeapList.Create(const ProcessID: THandle; const HeapHandle: THandle);
begin
  inherited Create();
  self.GetList(ProcessID,HeapHandle);
end;

constructor THeapsHeapList.Create(const ProcHeap: HEAPLIST32);
begin
  inherited Create();
  self.GetList(ProcHeap.th32ProcessID,ProcHeap.th32HeapID);
end;

destructor THeapsHeapList.Destroy;
begin
  inherited;
end;

function THeapsHeapList.GetCount: integer;
begin
  result:=length(self.Flist);
end;

function THeapsHeapList.GetListValue(const Index: integer): HEAPENTRY32;
begin
  result:=self.FList[Index];
end;

procedure THeapsHeapList.GetList(const ProcessID: THandle);
var
  he   : HEAPENTRY32;
  IsOk : LongBool;
  iCount,i  : integer;
  Length  : integer;
  HeapList: THeapList;
  Snapshot: TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Length:=1024;
  setlength(self.FList,Length);

  Snapshot:=TSnapshot.Create(TH32CS_SNAPHEAPLIST,ProcessID);
  HeapList:=THeapList.Create(ProcessID);
  try
    for i:=0 to HeapList.Count-1 do
    begin
      he.dwSize:=sizeof(HEAPENTRY32);
      IsOk:=Snapshot.Heap32First(he,ProcessID,HeapList.Items[i].th32HeapID);
      while IsOk do
      begin
        if iCount<Length then
          self.FList[iCount]:=he
        else
        begin
          inc(Length,1024);
          setlength(self.FList,Length);
          self.FList[iCount]:=he;
        end;
        inc(iCount);

        he.dwSize:=sizeof(HEAPENTRY32);
        IsOk:=Snapshot.Heap32Next(he);
      end;
    end;
  finally
    HeapList.Free;
    Snapshot.Free;
  end;
end;


procedure THeapsHeapList.GetList(const ProcessID, HeapHandle: THandle);
var
  he   : HEAPENTRY32;
  IsOk : LongBool;
  iCount  : integer;
  Length  : integer;
  Snapshot:TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Length:=1024;
  setlength(self.FList,Length);

  Snapshot:=TSnapshot.Create(TH32CS_SNAPHEAPLIST,ProcessID);
  try
    he.dwSize:=sizeof(HEAPENTRY32);
    IsOk:=Snapshot.Heap32First(he,ProcessID,HeapHandle);
    while IsOk do
    begin
      if iCount<Length then
        self.FList[iCount]:=he
      else
      begin
        inc(Length,1024);
        setlength(self.FList,Length);
        self.FList[iCount]:=he;
      end;
      inc(iCount);

      he.dwSize:=sizeof(HEAPENTRY32);
      IsOk:=Snapshot.Heap32Next(he);
    end;
  finally
    Snapshot.Free;
  end;
end;

{ THeapList }

constructor THeapList.Create(const ProcessID: THandle);
begin
  inherited Create();
  self.GetList(ProcessID);
end;

destructor THeapList.Destroy;
begin
  inherited;
end;

function THeapList.GetCount: integer;
begin
  result:=length(self.Flist);
end;

function THeapList.GetListValue(const Index: integer): HEAPLIST32;
begin
  result:=self.FList[Index];
end;

procedure THeapList.GetList(const ProcessID : THandle);
var
  hl   : HEAPLIST32;
  IsOkList : LongBool;
  iCount  : integer;
  Length  : integer;
  Snapshot: TSnapshot;
begin
  setlength(self.FList,0);
  iCount:=0;
  Length:=128;
  setlength(self.FList,Length);

  Snapshot:=TSnapshot.Create(TH32CS_SNAPHEAPLIST,ProcessID);
  try
    hl.dwSize:=sizeof(HEAPLIST32);
    IsOkList:=Snapshot.Heap32ListFirst(hl);
    while IsOkList do
    begin
      if iCount<Length then
        self.FList[iCount]:=hl
      else
      begin
        inc(Length,128);
        setlength(self.FList,Length);
        self.FList[iCount]:=hl;
      end;
      inc(iCount);

      hl.dwSize:=sizeof(HEAPLIST32);
      IsOkList:=Snapshot.Heap32ListNext(hl);
    end;
  finally
    Snapshot.free;
  end;
end;

end.
