program MemEdit;



uses
  Forms,
  UnitfrmMain in 'UnitfrmMain.pas' {frmMain},
  UnitAboutAddress in 'UnitAboutAddress.pas',
  UnitfrmChooseProc in 'UnitfrmChooseProc.pas' {frmChooseProc},
  CPUCounter_Hss in 'CPUCounter_Hss.pas',
  UnitfrmAddToAddressList in 'UnitfrmAddToAddressList.pas' {frmAddToAddressList},
  UnitCommon in 'UnitCommon.pas',
  CPUInf_Hss in 'CPUInf_Hss.pas',
  Password_Hss in 'Password_Hss.pas',
  Memory_Hss in 'Memory_Hss.PAS',
  TLHELP32_Hss in 'TLHELP32_Hss.pas',
  UnitHookDllFunction in 'MemEditHook\UnitHookDllFunction.pas',
  UnitfrmFindHelp in 'UnitfrmFindHelp.pas' {frmFindHelp},
  UnitfrmSystemSet in 'UnitfrmSystemSet.pas' {frmSystemSet},
  UnitMemSelect in 'UnitMemSelect.pas' {frmMemSelect};

{$R *.res}
begin
  Application.Initialize;
  Application.Title := 'Freesoft���¾Ž�';
  Application.CreateForm(TfrmMain, frmMain);
  if frmMain.IsClose then
  begin
    frmMain.Close;
    exit;
  end
  else
    Application.Run;
end.
