unit UnitfrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, DBTables, RxMemDS, MemTable, Mask,
  ToolEdit, CurrEdit, RXDBCtrl, Grids, DBGrids, ComCtrls,
  UnitfrmChooseProc, UnitAboutAddress, CPUCounter_Hss, TabNotBk, Math,
  ExtCtrls, Menus, UnitfrmAddToAddressList, UnitCommon, UnitHookDllFunction, Memory_Hss,
  RXShell, CPUInf_Hss ,TLHELP32_Hss, UnitfrmFindHelp, ToolWin, ImgList,shellapi,
  ShellCtrls, FileCtrl, ExtDlgs, marsCap, XPMenu,mousehook,keyboardhook;

type

  TfrmMain = class(TForm)
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RxDBGridAddress: TRxDBGrid;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    StaticText1: TStaticText;
    EditFindValue: TEdit;
    StaticText2: TStaticText;
    ComboBoxValueType: TComboBox;
    BitBtnStartFind: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBoxEx;
    RxDBGridEditMem: TRxDBGrid;
    ProgressBar: TProgressBar;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    tbNew: TToolButton;
    tlDel: TToolButton;
    tbClear: TToolButton;
    ToolButton4: TToolButton;
    tbConfig: TToolButton;
    tbRefresh: TToolButton;
    ToolButton6: TToolButton;
    tbOpen: TToolButton;
    tbSave: TToolButton;
    ToolButton1: TToolButton;
    tbQuit: TToolButton;
    ToolButton5: TToolButton;
    RxMemoryDataAddress: TRxMemoryData;
    RxMemoryDataAddressAddress: TStringField;
    RxMemoryDataAddressValue: TStringField;
    RxMemoryDataAddressValueType: TIntegerField;
    RxMemoryDataAddressMemListIndex: TIntegerField;
    DataSourceAdress: TDataSource;
    RxMemoryDataEditMem: TRxMemoryData;
    RxMemoryDataEditMemMsName: TStringField;
    RxMemoryDataEditMemValueName: TStringField;
    RxMemoryDataEditMemValueAddress: TStringField;
    RxMemoryDataEditMemValue: TStringField;
    RxMemoryDataEditMemValueType: TStringField;
    RxMemoryDataEditMemEditType: TIntegerField;
    RxMemoryDataEditMemHotKey: TIntegerField;
    RxMemoryDataEditMemTMsHwnd: TStringField;
    RxMemoryDataEditMemIsWriteOk: TIntegerField;
    RxMemoryDataEditMemID: TIntegerField;
    DataSourceEditMem: TDataSource;
    PopupMenuMemList: TPopupMenu;
    PopMum_AddToAddressList: TMenuItem;
    N12: TMenuItem;
    PopMum_DelAddress: TMenuItem;
    hen54y5: TMenuItem;
    PopMum_ReadAllValue: TMenuItem;
    TimerAutoEdit: TTimer;
    PopupMenuAddressList: TPopupMenu;
    PopMum_Edit: TMenuItem;
    PopMum_Add: TMenuItem;
    PopMum_Del: TMenuItem;
    PopMum_Copy: TMenuItem;
    N6: TMenuItem;
    PopMum_Read: TMenuItem;
    PopMum_Write: TMenuItem;
    N7: TMenuItem;
    PopMum_ReadAll: TMenuItem;
    PopMum_WriteAll: TMenuItem;
    Hen45: TMenuItem;
    PopMum_DelAll: TMenuItem;
    PopupMenu1: TPopupMenu;
    PopMum_Add_: TMenuItem;
    PopupMenu2: TPopupMenu;
    PopMum_DelActiveMission: TMenuItem;
    TrayIcon: TRxTrayIcon;
    PopupMenuTrayIcon: TPopupMenu;
    TrayIconShowForm: TMenuItem;
    TrayIconHideForm: TMenuItem;
    hendfgdf: TMenuItem;
    TrayIconCloseForm: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    ImageList1: TImageList;
    ImageListProcIcons: TImageList;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    TabSheet2: TTabSheet;
    ListView1: TListView;
    imgListTools: TImageList;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Image2: TImage;
    Image3: TImage;
    Label5: TLabel;
    Label6: TLabel;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Memo1: TMemo;
    Memo2: TMemo;
    imgListPage: TImageList;
    CoolBar2: TCoolBar;
    ToolBar2: TToolBar;
    Panel1: TPanel;
    DriveComboBox1: TDriveComboBox;
    DirectoryListBox1: TDirectoryListBox;
    FileListBox1: TFileListBox;
    FilterComboBox1: TFilterComboBox;
    ScrollBox1: TScrollBox;
    imgShow: TImage;
    tbCapPic: TToolButton;
    SavePictureDialog1: TSavePictureDialog;
    tbSavePic: TToolButton;
    tbCapActiveWin: TToolButton;
    tbRenamePic: TToolButton;
    tbDelPic: TToolButton;
    Image4: TImage;
    Image5: TImage;
    Label4: TLabel;
    lstMission: TListBox;
    ToolButton2: TToolButton;
    XPMenu1: TXPMenu;
    procedure BitBtnNewClick(Sender: TObject);
    procedure BitBtnStartFindClick(Sender: TObject);
    procedure ShowArrayTValueAddress(); //显示当前任务查出的地址列表
    procedure ShowMemEditAddressList();       //显示当前的修改列表
    procedure OnWMHOOKPROC(var Msg: TMessage);message WM_HOOKPROC; //Hook Message 处理挂钩消息
    procedure AddAMission(ProcessesID : Handle;MissionName:string); //增加任务
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtnDelMissionClick(Sender: TObject);
    procedure RxDBGridAddressDrawDataCell(Sender: TObject;
      const Rect: TRect; Field: TField; State: TGridDrawState);
    procedure RxMemoryDataAddressAfterOpen(DataSet: TDataSet);
    procedure RxDBGridAddressMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PopMum_AddToAddressListClick(Sender: TObject);
    procedure RxDBGridAddressDblClick(Sender: TObject);
    procedure RxDBGridEditMemDrawDataCell(Sender: TObject;
      const Rect: TRect; Field: TField; State: TGridDrawState);
    procedure RxMemoryDataEditMemAfterOpen(DataSet: TDataSet);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure EditFindValueKeyPress(Sender: TObject; var Key: Char);
    procedure RxDBGridEditMemDragOver(Sender, Source: TObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure RxDBGridAddressDragOver(Sender, Source: TObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure RxDBGridAddressMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RxDBGridEditMemDragDrop(Sender, Source: TObject; X,
      Y: Integer);
    procedure TimerAutoEditTimer(Sender: TObject);
    procedure PopMum_ReadAllValueClick(Sender: TObject);
    procedure PopMum_DelClick(Sender: TObject);
    procedure RxDBGridEditMemMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PopMum_AddClick(Sender: TObject);
    procedure PopMum_DelAllClick(Sender: TObject);
    procedure PopMum_ReadClick(Sender: TObject);
    procedure PopMum_WriteAllClick(Sender: TObject);
    procedure PopMum_ReadAllClick(Sender: TObject);
    procedure PopMum_WriteClick(Sender: TObject);
    procedure RxDBGridEditMemDblClick(Sender: TObject);
    procedure PopMum_EditClick(Sender: TObject);
    procedure PopMum_Add_Click(Sender: TObject);
    procedure PopMum_DelActiveMissionClick(Sender: TObject);
    procedure PageControlClick(Sender: TObject);
    procedure PopMum_DelAddressClick(Sender: TObject);
    procedure BitBtnMsClearClick(Sender: TObject);
    procedure EditFindValueChange(Sender: TObject);
    procedure TrayIconCloseFormClick(Sender: TObject);
    procedure TrayIconShowFormClick(Sender: TObject);
    procedure TrayIconHideFormClick(Sender: TObject);
    procedure TrayIconClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PopMum_CopyClick(Sender: TObject);
    procedure BitBtnFindHelpClick(Sender: TObject);
    procedure BitBtnOpenClick(Sender: TObject);
    procedure BitBtnSaveClick(Sender: TObject);
    procedure RxDBGridAddressTitleClick(Column: TColumn);
    procedure RxDBGridEditMemTitleClick(Column: TColumn);
    procedure Panel2Click(Sender: TObject);
    procedure BitBtnSetClick(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure tbNewClick(Sender: TObject);
    procedure tlDelClick(Sender: TObject);
    procedure tbClearClick(Sender: TObject);
    procedure tbConfigClick(Sender: TObject);
    procedure tbRefreshClick(Sender: TObject);
    procedure tbOpenClick(Sender: TObject);
    procedure tbSaveClick(Sender: TObject);

    procedure listProcess();
    procedure mnCalcClick(Sender: TObject);
    procedure mnNotepadClick(Sender: TObject);
    procedure mnPaintClick(Sender: TObject);
    procedure tbQuitClick(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure FileListBox1Change(Sender: TObject);
    procedure tbCapPicClick(Sender: TObject);
    procedure tbSavePicClick(Sender: TObject);
    procedure tbCapActiveWinClick(Sender: TObject);
    procedure tbRenamePicClick(Sender: TObject);
    procedure tbDelPicClick(Sender: TObject);
    procedure lstMissionClick(Sender: TObject);
    procedure lstMissionDblClick(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);//列出所有进程
    
  private
    { Private declarations }
    mhook:TMousehook;
    khook:TKeyboardHook;
    mhwnd:HWND;
    //procedure HookMouseMove(const h: HWND; const X, Y: Integer);
    //procedure HookMouseClick(const h: HWND; const X, Y: Integer);
    procedure HookKeyboardDown(const KeyCode: Integer);
  public
    { Public declarations }
    MemEditMissionList      : TMissionList;
    MemEditAddressList      : TMemEditAddressList;
    MissionNameCount        : integer;
    IsClose                 : boolean;
    OrderByMsAdr,OrderByAdl : string;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}
//新任务
procedure TfrmMain.BitBtnNewClick(Sender: TObject);
begin
  if frmChooseProc<>nil then
    frmChooseProc.Free;
  frmChooseProc:=TfrmChooseProc.Create(nil);
  frmChooseProc.ShowModal;
  if frmChooseProc.IsOk then
  begin
    self.AddAMission(frmChooseProc.FProcessesID,frmChooseProc.FMissionName);
  end;
  frmChooseProc.Free;
  frmChooseProc:=nil;
  ShowArrayTValueAddress();
end;


  //进度条显示 回调函数
  function CallBackShowProgressBar(const xPosition : extended; const Mission : TMemEditMission):boolean;
  var
    i  : integer;
  begin
    if frmMain.ProgressBar.Tag=Integer(Mission) then
    begin
      i:=round (xPosition);
      if i<=0 then
      begin
        frmMain.ProgressBar.Position:=0;
        frmMain.ProgressBar.Visible:=false;
      end
      else if i>100 then
      begin
        frmMain.ProgressBar.Position:=100;
        frmMain.ProgressBar.Visible:=false;
      end
      else
      begin
        if frmMain.ProgressBar.Position<>i then
        begin
          if not frmMain.ProgressBar.Visible then
          begin
            frmMain.RxDBGridEditMem.Align:=alNone;
            frmMain.Splitter1.Align:=alNone;
            frmMain.ProgressBar.Visible:=true;
            frmMain.Splitter1.Align:=alBottom;
            frmMain.RxDBGridEditMem.Align:=alBottom;
            frmMain.StatusBar1.Panels.Items[0].Text:=Mission.Hint0;
            frmMain.StatusBar1.Panels.Items[1].Text:=Mission.Hint1;
            frmMain.StatusBar1.Hint:=Mission.Hint0+'    '+Mission.Hint1;
          end;
          frmMain.ProgressBar.Position:=i;
        end;
      end; //end if i=...
    end;//end if
    result:=not frmMain.IsClose;
    Application.ProcessMessages;
  end;

//开始查询
procedure TfrmMain.BitBtnStartFindClick(Sender: TObject);
var
  iFindValue : int64;
  fFindValue : Extended;
  sFindValue : string;
  IsOk       : boolean;
  time1      : Extended;
  Time0      : double;
  MemEditMission : TMemEditMission;
  strCommand : string;
  xCallBack  : TCallBackShowProgressBar;
  myError    : Exception;
begin
    strCommand:=self.EditFindValue.Text;
    if self.MemEditMissionList.Count=0 then
    begin
      self.BitBtnNewClick(Sender);
      if self.MemEditMissionList.Count>0 then
        self.BitBtnStartFindClick(Sender);
      exit;
    end;
    if strCommand='' then exit;

    self.ShowArrayTValueAddress();
    self.ComboBoxValueType.Enabled:=false;

    screen.Cursor:=crHourGlass;
    Application.ProcessMessages;

  try
    //MemEditMission:=MemEditMissionList.Items[self.PageControl.PageIndex];
    MemEditMission:=MemEditMissionList.Items[self.lstmission.itemindex];
    xCallBack:=@CallBackShowProgressBar;
    self.ProgressBar.Position:=0;
    self.ProgressBar.Visible:=false;
    self.ProgressBar.Tag:=integer(MemEditMission);
    if (length(strCommand)=1)  and (strCommand[1]='?') and (MemEditMission.FindType=ftnodefine)
      and (self.ComboBoxValueType.ItemIndex=-1) then
    begin
      self.ComboBoxValueType.ItemIndex:=3;
    end
    else if self.ComboBoxValueType.ItemIndex=-1 then
    begin
      if StrIsInt(strCommand) then
        self.ComboBoxValueType.ItemIndex:=0
      else if StrIsFloat(strCommand) then
        self.ComboBoxValueType.ItemIndex:=1;
    end;

    if self.ComboBoxValueType.ItemIndex=0 then
    begin
      try
        iFindValue:=strtoint64(strCommand);
      except
        myError := Exception.Create(' 数据类型不匹配,你搜索的数据不符合整形格式!  ');
        Raise myError;
      end;
      if (MemEditMission.FindIndex=0) or (MemEditMission.ValueAddressListCount=0) then
      begin
        if iFindValue=0 then
        begin
          if Windows.MessageBox(self.Handle,' 对于查找整数的任务,建议第一次不要查找"零"值 !  一定要查找吗?  ',
            '提示',MB_ICONQUESTION+MB_OKCANCEL)<>ID_OK then
            begin
              exit;
            end;
        end;
        //self.Enabled:=false;
        if not MemEditSysSet.ShowFormToSetModuleSuffix(MemEditMission.ProcessesID) then exit;
        if not MemEditSysSet.ShowFormToSetType(ftInt) then  exit;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.FirstToFindInt(MemEditMission.ProcessesID,iFindValue,MemEditSysSet.SysSet.WatchTypeInt,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end
      else
      begin
        //self.Enabled:=false;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.ToFindInt(iFindValue,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end;
    end
    else if self.ComboBoxValueType.ItemIndex=1 then
    begin
      try
        fFindValue:=strtoFloat(strCommand);
      except
        myError := Exception.Create(' 数据类型不匹配,你搜索的数据不符合浮点数格式!  ');
        Raise myError;
      end;
      if (MemEditMission.FindIndex=0) or (MemEditMission.ValueAddressListCount=0) then
      begin
        if fFindValue=0.0 then
        begin
          if Windows.MessageBox(self.Handle,' 对于查找浮点数的任务,建议第一次不要查找"零"值 !  一定要查找吗?  ',
          '提示',MB_ICONQUESTION+MB_OKCANCEL)<>ID_OK then
            exit;
        end;
        //self.Enabled:=false;
        if not MemEditSysSet.ShowFormToSetModuleSuffix(MemEditMission.ProcessesID) then exit;
        if not MemEditSysSet.ShowFormToSetType(ftFloat) then exit;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.FirstToFindFloat(MemEditMission.ProcessesID,fFindValue,strCommand,MemEditSysSet.SysSet.WatchTypeFloat,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end
      else
      begin
        //self.Enabled:=false;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.ToFindFloat(fFindValue,strCommand,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end;
    end
    else if self.ComboBoxValueType.ItemIndex=2 then
    begin
      sFindValue:=self.EditFindValue.Text;
      if length(sFindValue)>256 then setlength(sFindValue,256);
      if (MemEditMission.FindIndex=0) or (MemEditMission.ValueAddressListCount=0) then
      begin
        //self.Enabled:=false;
        if not MemEditSysSet.ShowFormToSetModuleSuffix(MemEditMission.ProcessesID) then exit;
        if not MemEditSysSet.ShowFormToSetType(ftString) then exit;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.FirstToFindString(MemEditMission.ProcessesID,sFindValue,MemEditSysSet.SysSet.WatchTypeString,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end
      else
      begin
        //self.Enabled:=false;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.ToFindString(sFindValue,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end;
    end
    else if self.ComboBoxValueType.ItemIndex=3 then
    begin
      if (MemEditMission.FindIndex=0)  then
      begin
        if not MemEditSysSet.ShowFormToSetModuleSuffix(MemEditMission.ProcessesID) then exit;
        if not MemEditSysSet.ShowFormToSetType(ftBlur) then exit;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.FirstToFindBlur(MemEditMission.ProcessesID,MemEditSysSet.SysSet.WatchTypeBlur,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
        strCommand:='?';
      end
      else if (length(strCommand)=1)  and (strCommand[1] in ['?','+','-','=','!']) then
      begin
        time1:=CPUTimeCounterQPC();
        //self.Enabled:=false;
        IsOk:=MemEditMission.ToBlurFind(strCommand[1],xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end
      else
      begin
        //self.Enabled:=false;
        time1:=CPUTimeCounterQPC();
        IsOk:=MemEditMission.ToBlurFindAValue(strCommand,xCallBack);
        time1:=CPUTimeCounterQPC()-time1;
        self.Enabled:=true;
      end;
    end
    else
    begin
      Windows.MessageBox(self.Handle,'  请先选择要查找的数据的类型!  ','提示',MB_ICONINFORMATION+MB_OK);
      exit;
    end;

    if self.IsClose then exit;
    time0:=Time1/1000000.0;
    if not IsOk then
    begin
      Windows.MessageBox(self.Handle,'  查找失败!  ','失败',MB_ICONSTOP+MB_OK);
      MemEditMission.Hint0:='(第'+inttostr(MemEditMission.FindIndex)+'次) 查找失败！';
      MemEditMission.Hint1:='所用时间: '+Format('%8.4f', [time0])+ ' 秒';
    end
    else
    begin
      if (strCommand='?') and (MemEditMission.FindType=ftBlur) then
      begin
        self.EditFindValue.Text:='?';
        if MemEditMission.FindType=ftBlur then
        begin
          MemEditMission.Hint0:=' 设定开始低阶模糊查找! '+'共监视 '+
              inttostr(MemEditMission.FMemBlurValueCopyTrueCount)+ ' 个地址.';
        end;
      end
      else
      begin
        if MemEditMission.FindType=ftBlur then
        begin
          MemEditMission.Hint0:='(第'+inttostr(MemEditMission.FindIndex)+'次) 找到 '+
              inttostr(MemEditMission.FMemBlurValueCopyTrueCount)+ ' 个地址.';
        end
        else
        begin
          MemEditMission.Hint0:='(第'+inttostr(MemEditMission.FindIndex)+'次) 找到 '+
              inttostr(MemEditMission.ValueAddressListCount)+ ' 个地址.';                 
        end;
      end;
      MemEditMission.Hint1:='所用时间: '+Format('%8.4f', [time0])+ ' 秒.';
      if (MemEditMission.FindType<>ftBlur) then
      begin
        if MemEditMission.ValueAddressListCount=0 then MemEditMission.Clear;
      end;
    end;
  finally
    self.ShowArrayTValueAddress();
    screen.Cursor:=crDefault;
  end;
end;

//显示地址数组
procedure TfrmMain.ShowArrayTValueAddress();
var
  i   : integer;
  ValueType  : TSetOfValueType;
  Ms      : TMemEditMission;
  xProc   : THandle;
  Value  : string;
begin
  self.StatusBar1.Panels.Items[0].Text:='';
  self.StatusBar1.Panels.Items[1].Text:='';
  self.StatusBar1.Hint:='';

  if self.IsClose then exit;
  try
    //self.ComboBoxValueType.ItemIndex:=-1;
    self.ComboBoxValueType.Enabled:=true;
    //if (self.MemEditMissionList.Count<=0) or (self.PageControl.PageIndex<0) then
    if (self.MemEditMissionList.Count<=0) or (self.lstmission.itemindex<0) then
    begin
      self.RxMemoryDataAddress.Active:=false;
      self.RxMemoryDataAddress.Active:=true;
      exit;
    end;

    //Ms:=self.MemEditMissionList.Items[self.PageControl.PageIndex];
    Ms:=self.MemEditMissionList.Items[self.lstmission.itemindex];
    if Ms.FindType=ftInt then
      self.ComboBoxValueType.ItemIndex:=0
    else if Ms.FindType=ftFloat then
      self.ComboBoxValueType.ItemIndex:=1
    else if Ms.FindType=ftString then
      self.ComboBoxValueType.ItemIndex:=2
    else if Ms.FindType=ftBlur then
      self.ComboBoxValueType.ItemIndex:=3;
    //else
    //  self.ComboBoxValueType.ItemIndex:=-1;

    if (self.ComboBoxValueType.ItemIndex>=0) and (length(Ms.ValueAddressList)>0) then
      self.ComboBoxValueType.Enabled:=false;

    self.RxDBGridAddress.DataSource:=nil;

    self.RxMemoryDataAddress.Active:=false;
    self.RxMemoryDataAddress.Active:=true;

      xProc:=Ms.ToolGetProcHandle(ms.ProcessesID);
      if xProc=0 then exit;

      for i:=min(length(ms.ValueAddressList),MemEditSysSet.SysSet.ShowAddressCount)-1 downto low(ms.ValueAddressList) do
      begin
        if (ms.ValueAddressList[i].Address=nil) or (ms.ValueAddressList[i].ValueType=[]) then continue;
        self.RxMemoryDataAddress.Insert;
        self.RxMemoryDataAddress.FieldByName('Address').AsString:=inttoHex(integer(ms.ValueAddressList[i].Address),8);
        ValueType:=ms.ValueAddressList[i].ValueType;
        self.RxMemoryDataAddress.FieldByName('ValueType').AsString:=inttostr(Pword(@ValueType)^);
        self.RxMemoryDataAddress.FieldByName('MemListIndex').AsInteger:=i;
        Value:='';
        if not (ms.ToolReadValueFromProcHandle(xProc,ms.ValueAddressList[i].Address,Value,ValueType)) then
          Value:='';
        self.RxMemoryDataAddress.FieldByName('Value').AsString:=Value;
        self.RxMemoryDataAddress.Post;
      end;
      ms.ToolCloseProcHandle(xProc);
      

    OrderByMsAdr:='Address';
    self.RxMemoryDataAddress.SortOnFields(OrderByMsAdr);
    self.RxMemoryDataAddress.Active:=true;
    self.RxDBGridAddress.DataSource:=self.DataSourceAdress;
    self.EditFindValue.SetFocus;
    self.EditFindValue.SelectAll;

    self.ProgressBar.Position:=0;
    self.ProgressBar.Visible:=false;
    self.ProgressBar.Tag:=integer(ms);
    self.StatusBar1.Panels.Items[0].Text:=ms.Hint0;
    self.StatusBar1.Panels.Items[1].Text:=ms.Hint1;
    self.StatusBar1.Hint:=ms.Hint0+'    '+ms.Hint1;
    
  finally
    self.RxMemoryDataAddress.Active:=true;
  end;
end;

type
  TSetHook=function ():HHOOK; stdcall;
  TDelHook=procedure (); stdcall;
var
  SetHook : pointer=nil;
  DelHook : pointer=nil;
  hDll    : dword=0;
  

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  //self.Caption:=GetHookMapName(inttostr(windows.GetTickCount()));
  //application.Title:=PShortString(GetHookMapName(inttostr(windows.GetTickCount)))^;
  IsClose:=false;
  application.Title:=self.Caption;
  MemEditMissionList:=TMissionList.Create;
  MemEditAddressList:=TMemEditAddressList.Create;

  MemEditSysSet:=TMemEditSysSet.Create;

  self.RxMemoryDataAddress.Active:=true;
  self.RxMemoryDataEditMem.Active:=true;
  
  self.Left:=max(0,(screen.Width-self.Width) div 2);
  self.Top:=max(0,(screen.Height-self.Height) div 2);

  self.MissionNameCount:=0;
  hDll:=windows.LoadLibrary(PChar(MyDLLName));
  if hDll<>0 then
  begin
    SetHook:=GetProcAddress(hDll,'SetHook');
    DelHook:=GetProcAddress(hDll,'DelHook');
    MapHandle:=MapGlobalData(GetMapName,sizeof(THookGlobalData), Pointer(HookGlobalData));
    if (HookGlobalData.AppHwnd<>0) and (windows.IsWindow(HookGlobalData.AppHwnd)) then
    begin
      windows.SendNotifyMessage(HookGlobalData.AppHwnd,WM_HOOKPROC,MSG_HOOKPROC_SHOWSELF,0);
      self.IsClose:=true;
      exit;
    end
    else
    begin
      HookGlobalData.AppHwnd:=self.Handle;
      HookGlobalData.HookHwnd:=0;
      if SetHook<>nil then
      begin
        HookGlobalData.HookHandle:=TSetHook(SetHook)();
      end
      else
        HookGlobalData.HookHandle:=0;
    end;
  end;
  self.ComboBoxValueType.ItemIndex:=-1;
  self.TimerAutoEdit.Enabled:=true;

  listprocess;
  pagecontrol1.ActivePageIndex:=0;

  //创建mouse hook实例
  {*
  mhook := TMouseHook.Create;
  mhook.DLLName := 'mousehook.dll';
  mhook.Blocked := False;
  mhook.OnMouseMove := HookMouseMove;
  mhook.OnMouseClick := HookMouseClick;
  *}

  // 创建keyboard hook实例
  khook := TKeyboardHook.Create;
  khook.DLLName := 'keyboardhook.dll';
  khook.OnKeyDown := HookKeyboardDown;

  //mhook.Start;
  //khook.Start;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i  : integer;
begin
  self.TimerAutoEdit.Enabled:=false;
  self.IsClose:=true;
  MemEditSysSet.Free;
  for i:=0 to 20 do
  begin
    application.ProcessMessages;
    application.ProcessMessages;
    application.ProcessMessages;
    application.ProcessMessages;
    application.ProcessMessages;
    sleep(10);
  end;
  MemEditMissionList.Free;
  MemEditAddressList.Free;

  if DelHook<>nil then
    TDelHook(DelHook)();
  if hDll<>0 then
    windows.FreeLibrary(hDll);
  if MapHandle<>0 then
    ReleaseGlobalData(MapHandle,Pointer(HookGlobalData));
  MapHandle:=0;
  HookGlobalData:=nil;
  SetHook:=nil;
  DelHook:=nil;
  hDll:=0;
end;

//删除任务
procedure TfrmMain.BitBtnDelMissionClick(Sender: TObject);
var
  Index  : integer;
begin
  //Index:=self.PageControl.PageIndex;
  Index:=self.lstmission.ItemIndex;
  if (self.MemEditMissionList.Count>0) and (Index>=0) then
  begin
    if self.MemEditMissionList.Items[Index].IsRunning then
    begin
      Windows.MessageBox(self.Handle,' 任务正在运行,请稍后!  ','提示',MB_ICONINFORMATION+MB_OK);
    end
    else
    begin
      MemEditMissionList.DelMission(Index);
      if self.MemEditMissionList.Count=0 then
      begin
        self.RxMemoryDataAddress.Active:=false;
        self.RxMemoryDataAddress.Active:=true;
        //self.PageControl.Pages[0]:='没有任务';
        self.RxMemoryDataAddress.Active:=false;
        self.RxMemoryDataAddress.Active:=true;
      end
      else
      begin
        //self.PageControl.Pages.Delete(Index);
        self.lstMission.Items.Delete(self.lstMission.itemindex);
      end;
      self.ComboBoxValueType.ItemIndex:=-1;
      ShowArrayTValueAddress();
    end;
  end;

end;

procedure TfrmMain.RxDBGridAddressDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
var
  DText     : string;
  ValueType : set of TValueType;
  xValue    : integer;
begin
  if (Field.FieldName='ValueType') then
  begin
    if (Field.DisplayText='') or (Field.DisplayText='0') then
      DText:=''
    else
    begin
      xValue:=strtoint(Field.DisplayText);
      Pword(@ValueType)^:=xValue;
      if vtByte in ValueType then
        DText:='字节 ';
      if vtWord in ValueType then
        DText:=DText+'双字节 ';
      if vtDWord in ValueType then
        DText:=DText+'四字节 ';
      if vtDDword in ValueType then
        DText:=DText+'八字节 ';
      if vtSingle in ValueType then
        DText:=DText+'单精度 ';
      if vtDouble in ValueType then
        DText:=DText+'双精度 ';
      if vtLDouble in ValueType then
        DText:=DText+'扩展精度 ';
      if vtAString in ValueType then
        DText:=DText+'字符串 ';
      if vtWString in ValueType then
        DText:=DText+'宽字符串 ';
    end;
    self.RxDBGridAddress.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,DText);
  end
  else
    self.RxDBGridAddress.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,Field.DisplayText);
end;

procedure TfrmMain.RxMemoryDataAddressAfterOpen(DataSet: TDataSet);
var
  i : integer;
  CaptionName  : string;
begin
  for i:= 0 to self.RxDBGridAddress.Columns.Count-1 do
  begin
    CaptionName:=self.RxDBGridAddress.Columns.Items[i].Title.Caption;
    if CaptionName='Address' then
      CaptionName:='数值地址'
    else if CaptionName='Value' then
      CaptionName:='数值'
    else if CaptionName='ValueType' then
      CaptionName:='数值的可能类型'
    else if CaptionName='MemListIndex' then
      CaptionName:='序号';
    self.RxDBGridAddress.Columns.Items[i].Title.Caption:=CaptionName;
  end;
end;

procedure TfrmMain.RxDBGridAddressMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Point : TPoint;
begin
  //
  if Button=mbRight then
  begin
    if self.RxDBGridAddress.Row>=1 then
    begin
      if self.RxMemoryDataAddress.FieldByName('Address').AsString<>'' then
      begin
        windows.GetCursorPos(Point);
        self.PopupMenuMemList.Popup(Point.x,Point.y);
      end;
    end;
  end;
end;

procedure TfrmMain.PopMum_AddToAddressListClick(Sender: TObject);
var
  xAddValue  : TAddressList;
begin
  //
  if self.MemEditMissionList.Count=0 then exit;

  if frmAddToAddressList<>nil then
    frmAddToAddressList.Free;
  frmAddToAddressList:=TfrmAddToAddressList.Create(nil);
  //
  //frmAddToAddressList.FAdd:=true;
  if self.RxMemoryDataAddress.fieldByName('Address').AsString<>'' then
  begin
    frmAddToAddressList.FName:='';
    frmAddToAddressList.FValue:=self.RxMemoryDataAddress.fieldByName('Value').AsString;
    frmAddToAddressList.FAddress:=Pointer(strtoint('$'+self.RxMemoryDataAddress.fieldByName('Address').AsString));
    frmAddToAddressList.FEditType:=etAuto;
    frmAddToAddressList.FHotKey:=GetUnitHotKey(byte('R'),1,0,0);
    frmAddToAddressList.FValueType:=vtNoDefine;
    Pword(@frmAddToAddressList.FValueTypeAll)^:=self.RxMemoryDataAddress.fieldByName('ValueType').AsInteger;
  end
  else
  begin
    frmAddToAddressList.FName:='';
    frmAddToAddressList.FValue:='';
    frmAddToAddressList.FAddress:=nil;
    frmAddToAddressList.FEditType:=etAuto;
    frmAddToAddressList.FHotKey:=GetUnitHotKey(byte('R'),1,0,0);
    frmAddToAddressList.FValueType:=vtNoDefine;
    frmAddToAddressList.FValueTypeAll:=[];
  end;
  frmAddToAddressList.ShowModal;

  if (frmAddToAddressList.IsOk) and (frmAddToAddressList.FAddress<>nil) then
  begin
    xAddValue:=TAddressList.Create;
    //xAddValue.MsName:=self.MemEditMissionList.Items[self.PageControl.PageIndex].Name;
    xAddValue.MsName:=self.MemEditMissionList.Items[self.lstmission.itemindex].Name;
    xAddValue.ValueName:=frmAddToAddressList.FName;
    xAddValue.ValueAddress:=frmAddToAddressList.FAddress;
    xAddValue.Value:=frmAddToAddressList.FValue;
    xAddValue.ValueType:=frmAddToAddressList.FValueType;
    xAddValue.EditType:=frmAddToAddressList.FEditType;
    xAddValue.HotKey:=frmAddToAddressList.FHotKey;
    //xAddValue.ProcessesID:= self.MemEditMissionList.Items[self.PageControl.PageIndex].ProcessesID;
    xAddValue.ProcessesID:= self.MemEditMissionList.Items[self.lstmission.itemindex].ProcessesID;
    xAddValue.IsWriteOk:=true;
    self.MemEditAddressList.AddAddress(xAddValue,frmAddToAddressList.CheckBoxWriteOnce.Checked);
    xAddValue.Free;
    self.ShowMemEditAddressList;
  end;

  frmAddToAddressList.Free;
  frmAddToAddressList:=nil;
end;

procedure TfrmMain.RxDBGridAddressDblClick(Sender: TObject);
begin
  PopMum_AddToAddressListClick(Sender);
end;

procedure TfrmMain.RxDBGridEditMemDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
var
  DText     : string;
  ValueType : TValueType;
  EditType  : TEditType;
  xValue    : integer;
begin
  if (Field.FieldName='ValueType') then
  begin
    DText:='';
    if (Field.DisplayText<>'') then
    begin
      xValue:=strtoint(Field.DisplayText);
      ValueType:=vtNodefine;
      Pbyte(@ValueType)^:=xValue;
      if vtByte = ValueType then
        DText:='字节 '
      else if vtWord = ValueType then
        DText:=DText+'双字节 '
      else if vtDWord = ValueType then
        DText:=DText+'四字节 '
      else if vtDDword = ValueType then
        DText:=DText+'八字节 '
      else if vtSingle = ValueType then
        DText:=DText+'单精度 '
      else if vtDouble = ValueType then
        DText:=DText+'双精度 '
      else if vtLDouble = ValueType then
        DText:=DText+'扩展精度 '
      else if vtAString = ValueType then
        DText:=DText+'字符串 '
      else if vtWString = ValueType then
        DText:=DText+'宽字符串 ';
    end;
    self.RxDBGridEditMem.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,DText);
  end
  else if (Field.FieldName='EditType') then
  begin
    DText:='';
    if (Field.DisplayText<>'') then
    begin
      xValue:=strtoint(Field.DisplayText);
      Pbyte(@EditType)^:=xValue;
      if etAuto =EditType then
        DText:='自动锁定'
      else if etSelf =EditType then
        DText:='手动修改'
      else if etHotKey =EditType then
        DText:='热键修改';
    end;
    self.RxDBGridEditMem.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,DText);
  end
  else if (Field.FieldName='HotKey') then
  begin
    DText:='';
    if (Field.DisplayText<>'') then
    begin
      DText:=HotKeyToStr(strtoint(Field.DisplayText));
    end;
    self.RxDBGridEditMem.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,DText);
  end
  else if (Field.FieldName='IsWriteOk') then
  begin
    if Field.DisplayText='1' then
      DText:='正常'
    else if Field.DisplayText='0' then
      DText:='出错';
    self.RxDBGridEditMem.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,DText);
  end
  else
    self.RxDBGridEditMem.Canvas.TextRect(Rect,Rect.Left+3,Rect.Top+3,Field.DisplayText);

end;

procedure TfrmMain.RxMemoryDataEditMemAfterOpen(DataSet: TDataSet);
var
  i : integer;
  CaptionName  : string;
begin
  for i:= 0 to self.RxDBGridEditMem.Columns.Count-1 do
  begin
    CaptionName:=self.RxDBGridEditMem.Columns.Items[i].Title.Caption;
    if CaptionName='MsName' then
      CaptionName:='任务名称'
    else if CaptionName='ValueName' then
      CaptionName:='数值名称'
    else if CaptionName='ValueAddress' then
      CaptionName:='数值地址'
    else if CaptionName='Value' then
      CaptionName:='数值'
    else if CaptionName='ValueType' then
      CaptionName:='数值类型'
    else if CaptionName='EditType' then
      CaptionName:='修改类型'
    else if CaptionName='HotKey' then
      CaptionName:='热键'
    else if CaptionName='ProcessesID' then
      CaptionName:='进程ID'
    else if CaptionName='IsWriteOk' then
      CaptionName:='状态'
    else if CaptionName='ID' then
      CaptionName:='ID';
    self.RxDBGridEditMem.Columns.Items[i].Title.Caption:=CaptionName;
  end;
end;

procedure TfrmMain.BitBtnRefreshClick(Sender: TObject);
begin
  self.ShowArrayTValueAddress();
  self.ShowMemEditAddressList();
end;

procedure TfrmMain.EditFindValueKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#13) or (Key=#10) then
  begin
    Key:=#0;
    self.BitBtnStartFindClick(sender);
  end;
end;

procedure TfrmMain.RxDBGridEditMemDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  if Source = self.RxDBGridAddress then
    Accept:=true
  else
    Accept:=false;
end;

procedure TfrmMain.RxDBGridAddressDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  if (Source = self.RxDBGridAddress) and (self.RxDBGridAddress.SelectedRows.Count>0) then
  begin
    Accept:=true;
    if self.RxDBGridAddress.SelectedRows.Count=1 then
      RxDBGridAddress.DragCursor:=crDrag
    else if self.RxDBGridAddress.SelectedRows.Count>1 then
      RxDBGridAddress.DragCursor:=crMultiDrag;

  end;
end;

procedure TfrmMain.RxDBGridAddressMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (ssLeft in Shift) then
  begin
    if self.RxDBGridAddress.SelectedRows.Count>0 then
    begin
      //if self.RxDBGridAddress.SelectedRows.Count=1 then
      //  RxDBGridAddress.DragCursor:=crDrag
      //else //if self.RxDBGridAddress.SelectedRows.Count>1 then
      //  RxDBGridAddress.DragCursor:=crMultiDrag;
      RxDBGridAddress.BeginDrag(false);
    end;
  end;
end;

procedure TfrmMain.RxDBGridEditMemDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  i : integer;
  ValueType : TSetofValueType;
  tempValueType : TSetofValueType;
var
  xAddValue  : TAddressList;
begin
  if (Source=self.RxDBGridAddress) and (self.RxDBGridAddress.SelectedRows.Count>0) then
  begin
    if self.RxDBGridAddress.SelectedRows.Count=1 then
      PopMum_AddToAddressListClick(sender)
    else if  self.RxDBGridAddress.SelectedRows.Count>1 then
    begin
      //
      self.RxMemoryDataEditMem.Active:=true;
      if self.MemEditMissionList.Count=0 then exit;

      if frmAddToAddressList<>nil then
        frmAddToAddressList.Free;
      frmAddToAddressList:=TfrmAddToAddressList.Create(nil);
      //
      //frmAddToAddressList.FAdd:=true;

      ValueType:=[vtByte,vtWord,vtDWord,vtDDWord,vtSingle,vtDouble,vtLDouble,vtAString,vtWString];
      for i:=0 to RxDBGridAddress.SelectedRows.count-1 do
      begin
        self.RxMemoryDataAddress.GotoBookmark(pointer(RxDBGridAddress.SelectedRows.Items[i]));
        Pword(@TempValueType)^:=self.RxMemoryDataAddress.fieldByName('ValueType').AsInteger;
        ValueType:=ValueType*TempValueType;
      end;


      self.RxMemoryDataAddress.GotoBookmark(pointer(RxDBGridAddress.SelectedRows.Items[0]));

      frmAddToAddressList.FName:='';
      frmAddToAddressList.FValue:=self.RxMemoryDataAddress.fieldByName('Value').AsString;
      frmAddToAddressList.FAddress:=Pointer(strtoint('$'+self.RxMemoryDataAddress.fieldByName('Address').AsString));
      frmAddToAddressList.EditAddress.Enabled:=false;
      frmAddToAddressList.FEditType:=etSelf;
      frmAddToAddressList.FHotKey:=GetUnitHotKey(byte('R'),1,0,0);
      frmAddToAddressList.FValueTypeAll:=ValueType;

      frmAddToAddressList.ShowModal;

      if (frmAddToAddressList.IsOk) and (frmAddToAddressList.FAddress<>nil) then
      begin
        xAddValue:=TAddressList.Create;
        for i:=0 to RxDBGridAddress.SelectedRows.count-1 do
        begin
          self.RxMemoryDataAddress.GotoBookmark(pointer(RxDBGridAddress.SelectedRows.Items[i]));
          //xAddValue.MsName:=self.MemEditMissionList.Items[self.PageControl.PageIndex].Name;
          xAddValue.MsName:=self.MemEditMissionList.Items[self.lstmission.itemindex].Name;
          xAddValue.ValueName:=frmAddToAddressList.FName;
          xAddValue.ValueAddress:=Pointer(strtoint('$'+self.RxMemoryDataAddress.FieldByName('Address').AsString));
          xAddValue.Value:=frmAddToAddressList.FValue;
          xAddValue.ValueType:=frmAddToAddressList.FValueType;
          xAddValue.EditType:=frmAddToAddressList.FEditType;
          xAddValue.HotKey:=frmAddToAddressList.FHotKey;
          //xAddValue.ProcessesID:= self.MemEditMissionList.Items[self.PageControl.PageIndex].ProcessesID;
          xAddValue.ProcessesID:= self.MemEditMissionList.Items[self.lstmission.itemindex].ProcessesID;
          xAddValue.IsWriteOk:=true;
          self.MemEditAddressList.AddAddress(xAddValue,frmAddToAddressList.CheckBoxWriteOnce.Checked);
        end;
        xAddValue.Free;
        self.ShowMemEditAddressList;
      end;

      frmAddToAddressList.Free;
      frmAddToAddressList:=nil;
    end;
  end;
end;

procedure TfrmMain.TimerAutoEditTimer(Sender: TObject);
begin
  self.MemEditAddressList.WriteProcValueAuto;
  if IsDebuggerPresent() then
    self.Close;
end;

procedure TfrmMain.PopMum_ReadAllValueClick(Sender: TObject);
begin
  self.BitBtnRefreshClick(sender);
end;

procedure TfrmMain.PopMum_DelClick(Sender: TObject);
begin
  self.MemEditAddressList.DelAddressByID(self.RxMemoryDataEditMem.FieldByName('ID').AsInteger);
  self.ShowMemEditAddressList;
end;

procedure TfrmMain.RxDBGridEditMemMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Point : TPoint;
begin
  //
  if Button=mbRight then
  begin
    if self.RxDBGridEditMem.Row>=1 then
    begin
      if self.RxMemoryDataEditMem.FieldByName('ValueAddress').AsString<>'' then
      begin
        windows.GetCursorPos(Point);
        self.PopupMenuAddressList.Popup(Point.x,Point.y);
      end;
    end;
  end;
end;

procedure TfrmMain.PopMum_AddClick(Sender: TObject);
var
  xAddValue  : TAddressList;
begin
  //
  self.RxMemoryDataEditMem.Active:=true;
  if self.MemEditMissionList.Count=0 then
  begin
    BitBtnNewClick(sender);
  end;
  if self.MemEditMissionList.Count=0 then exit;

  if frmAddToAddressList<>nil then
    frmAddToAddressList.Free;
  frmAddToAddressList:=TfrmAddToAddressList.Create(nil);
  //
  //frmAddToAddressList.FAdd:=true;
  frmAddToAddressList.FName:='';
  frmAddToAddressList.FValue:='';
  frmAddToAddressList.FAddress:=nil;
  frmAddToAddressList.FEditType:=etAuto;
  frmAddToAddressList.FHotKey:=GetUnitHotKey(byte('R'),1,0,0);
  frmAddToAddressList.FValueTypeAll:=[];
  frmAddToAddressList.ShowModal;

  if (frmAddToAddressList.IsOk) and (frmAddToAddressList.FAddress<>nil) then
  begin
    xAddValue:=TAddressList.Create;
    //xAddValue.MsName:=self.MemEditMissionList.Items[self.PageControl.PageIndex].Name;
    xAddValue.MsName:=self.MemEditMissionList.Items[self.lstmission.itemindex].Name;
    xAddValue.ValueName:=frmAddToAddressList.FName;
    xAddValue.ValueAddress:=frmAddToAddressList.FAddress;
    xAddValue.Value:=frmAddToAddressList.FValue;
    xAddValue.ValueType:=frmAddToAddressList.FValueType;
    xAddValue.EditType:=frmAddToAddressList.FEditType;
    xAddValue.HotKey:=frmAddToAddressList.FHotKey;
    //xAddValue.ProcessesID:= self.MemEditMissionList.Items[self.PageControl.PageIndex].ProcessesID;
    xAddValue.ProcessesID:= self.MemEditMissionList.Items[self.lstmission.itemindex].ProcessesID;
    xAddValue.IsWriteOk:=true;
    self.MemEditAddressList.AddAddress(xAddValue,frmAddToAddressList.CheckBoxWriteOnce.Checked);
    xAddValue.Free;
    self.ShowMemEditAddressList;
  end;

  frmAddToAddressList.Free;
  frmAddToAddressList:=nil;
end;

procedure TfrmMain.PopMum_DelAllClick(Sender: TObject);
begin
  self.MemEditAddressList.Clear; 
  self.ShowMemEditAddressList;
end;

procedure TfrmMain.PopMum_ReadClick(Sender: TObject);
begin
  self.MemEditAddressList.ReadProcAValueByID(self.RxMemoryDataEditMem.FieldByName('ID').AsInteger);
  self.ShowMemEditAddressList;
end;

procedure TfrmMain.PopMum_WriteAllClick(Sender: TObject);
begin
  self.MemEditAddressList.WriteProcAllValue;
  self.ShowMemEditAddressList;
end;

procedure TfrmMain.PopMum_ReadAllClick(Sender: TObject);
begin
  self.MemEditAddressList.ReadProcAllValue;
  self.ShowMemEditAddressList;
end;

procedure TfrmMain.ShowMemEditAddressList;
var
  i : integer;
begin

  if self.IsClose then exit;
  self.RxMemoryDataEditMem.Active:=false;
  self.RxMemoryDataEditMem.Active:=true;
  if self.MemEditAddressList.Count>0 then
  begin
    for i:=self.MemEditAddressList.Count-1 downto 0 do
    begin
      self.RxMemoryDataEditMem.Insert;
      self.RxMemoryDataEditMem.FieldByName('MsName').AsString:=self.MemEditAddressList.Items[i].MsName;
      self.RxMemoryDataEditMem.FieldByName('ValueName').AsString:=self.MemEditAddressList.Items[i].ValueName;
      self.RxMemoryDataEditMem.FieldByName('ValueAddress').AsString:=inttoHex(integer(self.MemEditAddressList.Items[i].ValueAddress),8);
      self.RxMemoryDataEditMem.FieldByName('Value').AsString:=self.MemEditAddressList.Items[i].Value;
      self.RxMemoryDataEditMem.FieldByName('ValueType').AsString:=inttostr(Pword(@self.MemEditAddressList.Items[i].ValueType)^);
      self.RxMemoryDataEditMem.FieldByName('EditType').AsInteger:=integer(self.MemEditAddressList.Items[i].EditType);
      self.RxMemoryDataEditMem.FieldByName('HotKey').AsInteger:=integer(self.MemEditAddressList.Items[i].HotKey);
      self.RxMemoryDataEditMem.FieldByName('ProcessesID').AsString:=inttohex(self.MemEditAddressList.Items[i].ProcessesID,8);
      self.RxMemoryDataEditMem.FieldByName('IsWriteOk').AsInteger:=integer(self.MemEditAddressList.Items[i].IsWriteOk);
      self.RxMemoryDataEditMem.FieldByName('ID').AsInteger:=integer(self.MemEditAddressList.Items[i].ID);
      self.RxMemoryDataEditMem.Post;
    end;
  end;
end;

procedure TfrmMain.PopMum_WriteClick(Sender: TObject);
begin
  self.MemEditAddressList.WriteProcAValueByID(self.RxMemoryDataEditMem.FieldByName('ID').AsInteger);
  self.ShowMemEditAddressList;
end;

procedure TfrmMain.RxDBGridEditMemDblClick(Sender: TObject);
var
  xAddValue  : TAddressList;
  ID         : DWord;
  ProcessesID   : THandle;
  MsName     : string;
begin
  //
  self.RxMemoryDataEditMem.Active:=true;
  if self.RxMemoryDataAddress.RecordCount=0 then
  begin
    if (self.MemEditMissionList.Count=0) then
    begin
      BitBtnNewClick(Sender);
      if self.MemEditMissionList.Count<>0 then
        PopMum_AddClick(sender);
    end
    else
      PopMum_AddClick(sender);
    exit;
  end;

  if self.MemEditMissionList.Count=0 then exit;
  if self.MemEditAddressList.Count=0 then exit;
  
  if frmAddToAddressList<>nil then
    frmAddToAddressList.Free;
  frmAddToAddressList:=TfrmAddToAddressList.Create(nil);
  //
  //frmAddToAddressList.FAdd:=false;
  frmAddToAddressList.FName:=self.RxMemoryDataEditMem.FieldByName('ValueName').AsString;
  frmAddToAddressList.FValue:=self.RxMemoryDataEditMem.FieldByName('Value').AsString;
  frmAddToAddressList.FAddress:=Pointer(strtoint('$'+self.RxMemoryDataEditMem.FieldByName('ValueAddress').AsString));
  frmAddToAddressList.FEditType:=TEditType(self.RxMemoryDataEditMem.FieldByName('EditType').AsInteger);
  frmAddToAddressList.FHotKey:=self.RxMemoryDataEditMem.FieldByName('HotKey').AsInteger;
  frmAddToAddressList.FValueType:=TValueType((strtoint(self.RxMemoryDataEditMem.FieldByName('ValueType').AsString)));
  frmAddToAddressList.FValueTypeAll:=[];
  ID:=self.RxMemoryDataEditMem.FieldByName('ID').AsInteger;
  ProcessesID:=THandle(strtoint('$'+self.RxMemoryDataEditMem.FieldByName('ProcessesID').AsString));
  MsName:=self.RxMemoryDataEditMem.FieldByName('MsName').AsString;
  frmAddToAddressList.ShowModal;

  if (frmAddToAddressList.IsOk) and (frmAddToAddressList.FAddress<>nil) then
  begin
    xAddValue:=TAddressList.Create;
    xAddValue.ID:=ID;
    xAddValue.ProcessesID:=ProcessesID;
    xAddValue.MsName:=MsName;
    xAddValue.ValueName:=frmAddToAddressList.FName;
    xAddValue.ValueAddress:=frmAddToAddressList.FAddress;
    xAddValue.Value:=frmAddToAddressList.FValue;
    xAddValue.ValueType:=frmAddToAddressList.FValueType;
    xAddValue.EditType:=frmAddToAddressList.FEditType;
    xAddValue.HotKey:=frmAddToAddressList.FHotKey;
    xAddValue.IsWriteOk:=true;
    self.MemEditAddressList.SetValueByID(xAddValue);
    xAddValue.Free;
    self.ShowMemEditAddressList;
  end;

  frmAddToAddressList.Free;
  frmAddToAddressList:=nil;
end;

procedure TfrmMain.PopMum_EditClick(Sender: TObject);
begin
  RxDBGridEditMemDblClick(sender);
end;

procedure TfrmMain.PopMum_Add_Click(Sender: TObject);
begin
  PopMum_AddClick(sender);
end;

procedure TfrmMain.OnWMHOOKPROC(var Msg: TMessage);
type
  TShellExecute = function  (
    hwnd            :HWND;	    // handle to parent window
    lpOperation     :LPCTSTR;	// pointer to string that specifies operation to perform
    lpFile          :LPCTSTR;	// pointer to filename or folder name string
    lpParameters    :LPCTSTR;	// pointer to string that specifies executable-file parameters
    lpDirectory     :LPCTSTR;	// pointer to string that specifies default directory
    nShowCmd        :INTeger):LongWord; stdcall;
var
  i : integer;
  ShellExecute : Pointer;
  ProcessesID  : THandle;
  LibHandle    : THandle;
begin
  case Msg.WParam of
    MSG_HOOKPROC_START:
      begin
        //Hook成功
        Msg.Result:=0;
      end;
    MSG_HOOKPROC_SHOWSELF:
      begin
        //显示自己的消息

        //下面是为了弹出自己，但有些问题:(
        self.Visible:=true;
        LibHandle:=windows.LoadLibrary('shell32.dll');
        ShellExecute:=windows.GetProcAddress(LibHandle,'ShellExecuteA');
        if ShellExecute<>nil then
          TShellExecute(ShellExecute)(application.handle,'open', pchar('Refresh.scf'),'','', SW_NORMAL);
        windows.FreeLibrary(LibHandle);
        for i:=0 to 20 do
        begin
          application.ProcessMessages;
          application.ProcessMessages;
          application.ProcessMessages;
          application.ProcessMessages;
          application.ProcessMessages;
          sleep(10);
        end;
        windows.SetWindowPos(self.Handle,windows.GetForegroundWindow,
                max(0,(screen.Width-self.Width) div 2),max(0,(screen.Height-self.Height) div 2),
                self.Width,self.Height,SWP_SHOWWINDOW);
        Application.Restore;
        Application.BringToFront;

        self.EditFindValue.SetFocus;
        self.EditFindValue.SelectAll;

      end;

    MSG_HOOKPROC_HOTKEY:
      begin
        //键盘消息

        Msg.Result:=0;
        
        // 用户是否触发修改热键
        for i:=0 to self.MemEditAddressList.Count-1 do
        begin
          with self.MemEditAddressList.Items[i] do
          begin
            if (EditType=etHotKey) and (HotKey=Msg.LParam) then
              self.MemEditAddressList.WriteProcAValue(i);
          end;
        end;

        if Msg.LParam=MemEditSysSet.SysSet.CallHotKey then //用户触发热键
        begin

          ProcessesID:=HwndToProcessesID(HookGlobalData.Msg.hwnd);
          if (windows.GetForegroundWindow=self.Handle)
          or (ProcessesID=HwndToProcessesID(Application.Handle)) then  //原来是自己:)
            exit;

          if (self.MemEditMissionList.Count=0)
            //or (self.MemEditMissionList.Items[self.PageControl.PageIndex].ProcessesID
            or (self.MemEditMissionList.Items[self.lstMission.itemindex].ProcessesID
              <>ProcessesID) then
          begin
            self.AddAMission(ProcessesID,''); //增加任务
          end;
          windows.SendNotifyMessage(HookGlobalData.AppHwnd,WM_HOOKPROC,MSG_HOOKPROC_SHOWSELF,0);
          {HookGlobalData.AppHwnd:=self.Handle;
          HookGlobalData.AppLeft:=self.Left;
          HookGlobalData.AppTop:=self.Top;
          HookGlobalData.AppWidth:=self.Width;
          HookGlobalData.AppHeight:=self.Height;
          self.Left:=0;
          self.Top:=0;
          windows.PostMessage(HookGlobalData.Msg.hwnd,WM_HOOKPROC,MSG_HOOKPROC_SHOWSELF,0);
           }
        end;
      end;
    else
      inherited;
  end;
end;
//自定义过程-添加任务
procedure TfrmMain.AddAMission(ProcessesID : Handle;MissionName:string);
var
  PageIndex : integer;
begin
    MemEditMissionList.AddMission(ProcessesID);
    inc(MissionNameCount);
    PageIndex:=MemEditMissionList.Count-1;
    if MissionName='' then
      MissionName:='任务'+inttostr(MissionNameCount);
    //if self.PageControl.GetIndexForPage(MissionName)>=0 then
    if self.lstmission.items.count>=0 then
      MissionName:=MissionName+'_'+inttostr(MissionNameCount);
    MemEditMissionList.Items[PageIndex].Name:=MissionName;

    if MemEditMissionList.Count=1 then
    begin
      //self.PageControl.Pages[PageIndex]:=MissionName;
      self.lstMission.Items.Add(missionname);
      self.lstMission.ItemIndex:=0;
    end
    else
    begin
      //self.PageControl.Pages.Add(MissionName);
      self.lstMission.Items.Add(missionname);
      self.lstMission.itemindex:=0;
    end;

    ShowArrayTValueAddress();
end;

procedure TfrmMain.PopMum_DelActiveMissionClick(Sender: TObject);
begin
  tlDelClick(Sender);
end;

procedure TfrmMain.PageControlClick(Sender: TObject);
begin
  ShowArrayTValueAddress();
end;

procedure TfrmMain.PopMum_DelAddressClick(Sender: TObject);
var
  index : integer;
  Ms    : TMemEditMission;
begin
  index:=self.RxMemoryDataAddress.FieldByName('MemListIndex').AsInteger;
  //Ms:=self.MemEditMissionList.Items[self.PageControl.PageIndex];
  Ms:=self.MemEditMissionList.Items[self.lstmission.itemindex];
  ms.ValueAddressList[index].Address:=nil;
  self.RxMemoryDataAddress.Delete;
end;

procedure TfrmMain.BitBtnMsClearClick(Sender: TObject);
var
  Index  : integer;
begin
  //Index:=self.PageControl.PageIndex;
  index:=self.lstMission.ItemIndex;
  if (self.MemEditMissionList.Count>0) and (Index>=0) then
  begin
    if self.MemEditMissionList.Items[Index].IsRunning then
    begin
      Windows.MessageBox(self.Handle,' 任务正在运行,请稍后!  ','提示',MB_ICONINFORMATION+MB_OK);
    end
    else
    begin
      self.MemEditMissionList.Items[Index].Clear;
      ShowArrayTValueAddress();
    end;
  end;

end;

procedure TfrmMain.EditFindValueChange(Sender: TObject);
begin
  self.EditFindValue.Hint:=self.EditFindValue.Text;
end;

procedure TfrmMain.TrayIconCloseFormClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmMain.TrayIconShowFormClick(Sender: TObject);
begin
    self.Visible:=true;
    Application.Restore;
    Application.BringToFront;
end;

procedure TfrmMain.TrayIconHideFormClick(Sender: TObject);
begin
    Application.Minimize;
    Application.BringToFront;
end;

procedure TfrmMain.TrayIconClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if windows.IsIconic(Application.Handle) then
    self.TrayIconShowFormClick(Sender)
  else
    self.TrayIconHideFormClick(Sender);
end;

procedure TfrmMain.PopMum_CopyClick(Sender: TObject);
var
  xAddValue  : TAddressList;
  ID         : DWord;
  ProcessesID   : THandle;
  MsName     : string;
begin
  if self.MemEditMissionList.Count=0 then exit;

  if frmAddToAddressList<>nil then
    frmAddToAddressList.Free;
  frmAddToAddressList:=TfrmAddToAddressList.Create(nil);
  //
  //frmAddToAddressList.FAdd:=true;
  frmAddToAddressList.FName:=self.RxMemoryDataEditMem.FieldByName('ValueName').AsString;
  frmAddToAddressList.FValue:=self.RxMemoryDataEditMem.FieldByName('Value').AsString;
  frmAddToAddressList.FAddress:=Pointer(strtoint('$'+self.RxMemoryDataEditMem.FieldByName('ValueAddress').AsString));
  frmAddToAddressList.FEditType:=TEditType(self.RxMemoryDataEditMem.FieldByName('EditType').AsInteger);
  frmAddToAddressList.FHotKey:=self.RxMemoryDataEditMem.FieldByName('HotKey').AsInteger;
  frmAddToAddressList.FValueType:=TValueType((strtoint(self.RxMemoryDataEditMem.FieldByName('ValueType').AsString)));
  frmAddToAddressList.FValueTypeAll:=[];
  ID:=self.RxMemoryDataEditMem.FieldByName('ID').AsInteger;
  ProcessesID:=THandle(strtoint('$'+self.RxMemoryDataEditMem.FieldByName('ProcessesID').AsString));
  MsName:=self.RxMemoryDataEditMem.FieldByName('MsName').AsString;
  frmAddToAddressList.ShowModal;

  if (frmAddToAddressList.IsOk) and (frmAddToAddressList.FAddress<>nil) then
  begin
    xAddValue:=TAddressList.Create;
    xAddValue.ID:=ID;
    xAddValue.ProcessesID:=ProcessesID;
    xAddValue.MsName:=MsName;
    xAddValue.ValueName:=frmAddToAddressList.FName;
    xAddValue.ValueAddress:=frmAddToAddressList.FAddress;
    xAddValue.Value:=frmAddToAddressList.FValue;
    xAddValue.ValueType:=frmAddToAddressList.FValueType;
    xAddValue.EditType:=frmAddToAddressList.FEditType;
    xAddValue.HotKey:=frmAddToAddressList.FHotKey;
    xAddValue.IsWriteOk:=true;
    self.MemEditAddressList.AddAddress(xAddValue);
    xAddValue.Free;
    self.ShowMemEditAddressList;
  end;

  frmAddToAddressList.Free;
  frmAddToAddressList:=nil;
end;

procedure TfrmMain.BitBtnFindHelpClick(Sender: TObject);
begin
  if frmFindHelp<>nil then
    frmFindHelp.Free;
  frmFindHelp:=TfrmFindHelp.Create(nil);
  frmFindHelp.ShowModal;
  frmFindHelp.Free;
  frmFindHelp:=nil;
end;

procedure TfrmMain.BitBtnOpenClick(Sender: TObject);
var
  L           : integer;
  ProcessList : TProcessList;
  AddressSave : TAddressListSave;
  AddressList : TAddressList;
  xFile       : File of TAddressListSave;
  FileName    : string;
  OpenCount   : integer;
begin
  if self.OpenDialog.Execute then
  begin
    FileName:=self.OpenDialog.FileName ;
    L:=length(FileName);

    if (L<=4) or (uppercase(copy(FileName,L-4+1,4))<>'.ADL') then
    begin
      FileName:=FileName+'.Adl';
    end;

    ProcessList:=TProcessList.Create;
    AddressList:=TAddressList.Create;
    AssignFile(xFile,FileName);

    try
      Reset(xFile);
      OpenCount:=0;
      While not eof(xFile) do
      begin
        Read(xFile,AddressSave);
        AddressSave.ProcessesID:=ProcessList.GetProcessID(AddressSave.MsExeName);
        if AddressSave.ProcessesID<>0 then
        begin
          AddressList.Assign(AddressSave);
          self.MemEditAddressList.AddAddress(AddressList);
          inc(OpenCount);
        end;
      end;
      if OpenCount=0 then
      begin
        Windows.MessageBox(self.Handle,' 没有找到对应的保存资料,是否被修改的程序没有运行! ',
            '提示',MB_ICONWARNING+MB_OK);
      end;
    except
      Windows.MessageBox(self.Handle,' 打开文件时发生错误! ',
          '出错',MB_ICONERROR+MB_OK);
    end;

    CloseFile(xFile);
    AddressList.Free;
    ProcessList.Free;
    self.ShowMemEditAddressList;
  end;

end;

procedure TfrmMain.BitBtnSaveClick(Sender: TObject);
var
  i,L         : integer;
  ProcessList : TProcessList;
  AddressSave : TAddressListSave;
  xFile       : File of TAddressListSave;
  FileName    : string;
begin

  if self.MemEditAddressList.Count=0 then
  begin
    Windows.MessageBox(self.Handle,' 监视的地址列表为空,没有数据可以保存! ',
       '提示',MB_ICONINFORMATION+MB_OK);
  end
  else
  begin
    if self.SaveDialog.Execute then
    begin
      FileName:=self.SaveDialog.FileName ;
      L:=length(FileName);

      if (L<=4) or (uppercase(copy(FileName,L-4+1,4))<>'.ADL') then
      begin
        FileName:=FileName+'.Adl';
      end;

      ProcessList:=TProcessList.Create;
      AssignFile(xFile,FileName);

      try
        Rewrite(xFile);
        for i:=0 to self.MemEditAddressList.Count-1 do
        begin
          self.MemEditAddressList.Items[i].AssignTo(AddressSave);
          AddressSave.MsExeName:=ProcessList.GetProcessExeName(AddressSave.ProcessesID);
          Write(xFile,AddressSave);
        end;
      except
        Windows.MessageBox(self.Handle,' 保存文件时发生错误! ',
            '出错',MB_ICONERROR+MB_OK);
      end;

      CloseFile(xFile);
      ProcessList.Free;
    end;
  end;

end;

procedure TfrmMain.RxDBGridAddressTitleClick(Column: TColumn);
begin
  if Column.FieldName=OrderByMsAdr then
  begin
    self.RxMemoryDataAddress.SortOnFields(OrderByMsAdr,true,true);
    OrderByMsAdr:='';
  end
  else
  begin
    OrderByMsAdr:=Column.FieldName;
    self.RxMemoryDataAddress.SortOnFields(OrderByMsAdr);
  end;
  self.RxMemoryDataAddress.First;
end;

procedure TfrmMain.RxDBGridEditMemTitleClick(Column: TColumn);
begin
  if Column.FieldName=OrderByAdl then
  begin
    self.RxMemoryDataEditMem.SortOnFields(OrderByAdl,true,true);
    OrderByAdl:='';
  end
  else
  begin
    OrderByAdl:=Column.FieldName;
    self.RxMemoryDataEditMem.SortOnFields(OrderByAdl);
  end;
  self.RxMemoryDataEditMem.First;
end;

procedure TfrmMain.Panel2Click(Sender: TObject);
begin
 // self.ListBox1.Visible:=not self.ListBox1.Visible;
end;

procedure TfrmMain.BitBtnSetClick(Sender: TObject);
begin
  MemEditSysSet.ShowFormToSetAll;
end;
                                                           
procedure TfrmMain.PopupMenu2Popup(Sender: TObject);
var
  Point  : TPoint;
begin
  windows.GetCursorPos(Point);
  windows.mouse_event(MOUSEEVENTF_LEFTDOWN,Point.X,Point.Y,0,0);
  windows.mouse_event(MOUSEEVENTF_LEFTUP,Point.X,Point.Y,0,0);
  application.ProcessMessages;
end;
//新建任务
procedure TfrmMain.tbNewClick(Sender: TObject);
begin
  if frmChooseProc<>nil then
    frmChooseProc.Free;
  frmChooseProc:=TfrmChooseProc.Create(nil);
  frmChooseProc.ShowModal;
  if frmChooseProc.IsOk then
  begin
    self.AddAMission(frmChooseProc.FProcessesID,frmChooseProc.FMissionName);
  end;
  frmChooseProc.Free;
  frmChooseProc:=nil;
  ShowArrayTValueAddress();
end;

procedure TfrmMain.tlDelClick(Sender: TObject);
var
  Index  : integer;
begin
  //Index:=self.PageControl.PageIndex;
  Index:=self.lstmission.itemindex;
  if (self.MemEditMissionList.Count>0) and (Index>=0) then
  begin
    if self.MemEditMissionList.Items[Index].IsRunning then
    begin
      Windows.MessageBox(self.Handle,' 任务正在运行,请稍后!  ','提示',MB_ICONINFORMATION+MB_OK);
    end
    else
    begin
      MemEditMissionList.DelMission(Index);
      if self.MemEditMissionList.Count=0 then
      begin
        self.RxMemoryDataAddress.Active:=false;
        self.RxMemoryDataAddress.Active:=true;
        //self.PageControl.Pages[0]:='没有任务';
        self.RxMemoryDataAddress.Active:=false;
        self.RxMemoryDataAddress.Active:=true;
      end
      else
      begin
        //self.PageControl.Pages.Delete(Index);
        self.lstMission.Items.Delete(index);
      end;
      self.ComboBoxValueType.ItemIndex:=-1;
      ShowArrayTValueAddress();
    end;
  end;

end;

procedure TfrmMain.tbClearClick(Sender: TObject);
var
  Index  : integer;
begin
  //Index:=self.PageControl.PageIndex;
  Index:=self.lstmission.itemindex;
  if (self.MemEditMissionList.Count>0) and (Index>=0) then
  begin
    if self.MemEditMissionList.Items[Index].IsRunning then
    begin
      Windows.MessageBox(self.Handle,' 任务正在运行,请稍后!  ','提示',MB_ICONINFORMATION+MB_OK);
    end
    else
    begin
      self.MemEditMissionList.Items[Index].Clear;
      ShowArrayTValueAddress();
    end;
  end;

end;

procedure TfrmMain.tbConfigClick(Sender: TObject);
begin
  MemEditSysSet.ShowFormToSetAll;
end;

procedure TfrmMain.tbRefreshClick(Sender: TObject);
begin
  self.ShowArrayTValueAddress();
  self.ShowMemEditAddressList();
end;

procedure TfrmMain.tbOpenClick(Sender: TObject);
var
  L           : integer;
  ProcessList : TProcessList;
  AddressSave : TAddressListSave;
  AddressList : TAddressList;
  xFile       : File of TAddressListSave;
  FileName    : string;
  OpenCount   : integer;
begin
  if self.OpenDialog.Execute then
  begin
    FileName:=self.OpenDialog.FileName ;
    L:=length(FileName);

    if (L<=4) or (uppercase(copy(FileName,L-4+1,4))<>'.ADL') then
    begin
      FileName:=FileName+'.Adl';
    end;

    ProcessList:=TProcessList.Create;
    AddressList:=TAddressList.Create;
    AssignFile(xFile,FileName);

    try
      Reset(xFile);
      OpenCount:=0;
      While not eof(xFile) do
      begin
        Read(xFile,AddressSave);
        AddressSave.ProcessesID:=ProcessList.GetProcessID(AddressSave.MsExeName);
        if AddressSave.ProcessesID<>0 then
        begin
          AddressList.Assign(AddressSave);
          self.MemEditAddressList.AddAddress(AddressList);
          inc(OpenCount);
        end;
      end;
      if OpenCount=0 then
      begin
        Windows.MessageBox(self.Handle,' 没有找到对应的保存资料,是否被修改的程序没有运行! ',
            '提示',MB_ICONWARNING+MB_OK);
      end;
    except
      Windows.MessageBox(self.Handle,' 打开文件时发生错误! ',
          '出错',MB_ICONERROR+MB_OK);
    end;

    CloseFile(xFile);
    AddressList.Free;
    ProcessList.Free;
    self.ShowMemEditAddressList;
  end;

end;

procedure TfrmMain.tbSaveClick(Sender: TObject);
var
  i,L         : integer;
  ProcessList : TProcessList;
  AddressSave : TAddressListSave;
  xFile       : File of TAddressListSave;
  FileName    : string;
begin

  if self.MemEditAddressList.Count=0 then
  begin
    Windows.MessageBox(self.Handle,' 监视的地址列表为空,没有数据可以保存! ',
       '提示',MB_ICONINFORMATION+MB_OK);
  end
  else
  begin
    if self.SaveDialog.Execute then
    begin
      FileName:=self.SaveDialog.FileName ;
      L:=length(FileName);

      if (L<=4) or (uppercase(copy(FileName,L-4+1,4))<>'.ADL') then
      begin
        FileName:=FileName+'.Adl';
      end;

      ProcessList:=TProcessList.Create;
      AssignFile(xFile,FileName);

      try
        Rewrite(xFile);
        for i:=0 to self.MemEditAddressList.Count-1 do
        begin
          self.MemEditAddressList.Items[i].AssignTo(AddressSave);
          AddressSave.MsExeName:=ProcessList.GetProcessExeName(AddressSave.ProcessesID);
          Write(xFile,AddressSave);
        end;
      except
        Windows.MessageBox(self.Handle,' 保存文件时发生错误! ',
            '出错',MB_ICONERROR+MB_OK);
      end;

      CloseFile(xFile);
      ProcessList.Free;
    end;
  end;

end;

procedure tfrmmain.listProcess();
var
  Snapshot  : TProcessList;
  i         : integer;
  pe: PROCESSENTRY32 ;
  TreeNode  : TTreeNode;
  NodeName  : string;
  AddCount  : integer;
  Icon      : TIcon;
  MyProcessesID : THandle;
  IsCanOpen : THandle;
  nItemIndex:integer;
begin
  self.combobox1.items.clear;

  Snapshot:=TProcessList.Create();
  try
    AddCount:=0;
    MyProcessesID:=windows.GetCurrentProcessId();
    for i:=Snapshot.Count-1 downto 0 do
    begin
      pe:=Snapshot.Items[i];
      if (MyProcessesID<>pe.th32ProcessID) then   //不显示自己
      begin
        IsCanOpen:=OpenProcess(PROCESS_VM_OPERATION or PROCESS_VM_READ
                      or PROCESS_VM_WRITE or PROCESS_QUERY_INFORMATION,true, pe.th32ProcessID);
        if IsCanOpen<>0 then  //能打开
        begin
          windows.CloseHandle(IsCanOpen);
          NodeName:=pe.szExeFile;//inttohex(pe.,8);
          NodeName:=ExtractFileName(NodeName);
          //TreeNode:=self.TreeViewProcList.Items.AddChild(nil,NodeName);//+' '+inttostr(pe.th32ProcessID)
          self.ComboBox1.Items.Add(nodename);
          //TreeNode.Data:=pointer(pe.th32ProcessID);
          Icon:=TIcon.Create;
          Icon.Handle:=ExtractIcon(hinstance,pe.szExeFile,0);
          if Icon.Handle<>0 then
          begin
            inc(AddCount);
            self.ImageListProcIcons.AddIcon(Icon);
            //TreeNode.ImageIndex:=AddCount;
            //TreeNode.SelectedIndex:=AddCount;
            self.ComboBox1.itemsex[0].ImageIndex:=addcount;
            self.combobox1.ItemIndex:=addcount;
          end
          else
          begin
            TreeNode.ImageIndex:=0;
            TreeNode.SelectedIndex:=0;
          end;
          Icon.Free;

        end; //end if
      end; //end if
    end;//end for i

  finally
    //self.TreeViewProcList.Enabled:=true;
    Snapshot.Free;
  end;
end;

procedure TfrmMain.mnCalcClick(Sender: TObject);
begin
  shellexecute(0,'open','calc.exe',0,0,SW_SHOW);
end;

procedure TfrmMain.mnNotepadClick(Sender: TObject);
begin
  shellexecute(0,'open','notepad.exe',0,0,SW_SHOW);
end;

procedure TfrmMain.mnPaintClick(Sender: TObject);
begin
  shellexecute(0,'open','mspaint.exe',0,0,SW_SHOW);
end;

procedure TfrmMain.tbQuitClick(Sender: TObject);
begin
  self.Close;
  application.Terminate;
end;

procedure TfrmMain.ListView1DblClick(Sender: TObject);
begin
  if (listview1.itemindex=0) then
    begin
    shellexecute(0,'open','calc.exe',0,0,SW_SHOW);
    end;
  if (listview1.itemindex=1) then
    begin
    shellexecute(0,'open','notepad.exe',0,0,SW_SHOW);
    end;
  if (listview1.itemindex=2) then
    begin
    shellexecute(0,'open','mspaint.exe',0,0,SW_SHOW);
    end;
end;

procedure TfrmMain.FileListBox1Change(Sender: TObject);
begin
  if filelistbox1.filename<>'' then
  try
    begin
    imgShow.Picture.LoadFromFile(filelistbox1.filename);
    end
  except
    on e:exception do application.messagebox(pchar(e.message),pchar(inttostr(e.HelpContext)),MB_OK);
  end;
end;

procedure TfrmMain.tbCapPicClick(Sender: TObject);
var
  mycanvas:tcanvas ;
  dc:hdc;
  rect:trect;
  bmp:tbitmap;
begin
  mycanvas:=tcanvas.create;
  dc:=getwindowdc(0);
  bmp:=tbitmap.create;

  mycanvas.Handle:=dc;
  rect.left:=0;
  rect.Top:=0;
  rect.Right:=screen.width;
  rect.Bottom:=screen.height;
  bmp.Width:=screen.width;
  bmp.Height:=screen.height;

  try
    begin
    bmp.canvas.copyrect(rect,mycanvas,rect);
    imgshow.Picture.Bitmap.Assign(bmp);
    end
  except
    on e:exception do application.messagebox(pchar(e.message),pchar(inttostr(e.HelpContext)),MB_OK);
  end;
  releasedc(0,dc);
  mycanvas.Handle:=0;
end;

procedure TfrmMain.tbSavePicClick(Sender: TObject);
begin
  if(savepicturedialog1.Execute) then
    begin
    imgshow.Picture.savetofile(savepicturedialog1.FileName+'.bmp');
    end;
    filelistbox1.Update;
end;

procedure TfrmMain.tbCapActiveWinClick(Sender: TObject);
var
  mycanvas:tcanvas ;
  dc:hdc;
  rect:trect;
  bmp:tbitmap;
  mh_hWnd:hwnd;
  n:integer;
begin
  mycanvas:=tcanvas.create;
  mh_hwnd:=getactivewindow();
  dc:=getdc(mh_hwnd);
  bmp:=tbitmap.create;

  getwindowrect(mh_hwnd,rect);
  n:=rect.left;
  mycanvas.Handle:=dc;
  bmp.Width:=rect.Right-rect.left;
  bmp.Height:=rect.bottom-rect.top;

  try
    begin
    bmp.canvas.copyrect(rect,mycanvas,rect);
    imgshow.Picture.Bitmap.Assign(bmp);
    end
  except
    on e:exception do application.messagebox(pchar(e.message),pchar(inttostr(e.HelpContext)),MB_OK);
  end;

  releasedc(0,dc);
  mycanvas.Handle:=0;
end;

procedure TfrmMain.tbRenamePicClick(Sender: TObject);
var
  strInput:string;
  strPath:string;
  strFile:string;
begin
  strFile:=extractfilename(filelistbox1.FileName);
  strPath:=extractfilepath(filelistbox1.FileName);
  strInput:=inputbox('提示:','请输入文件名:',strfile);
  try
    renamefile(filelistbox1.FileName,strpath+strInput);
  except
    on e:exception do application.messagebox(pchar(e.message),pchar(inttostr(e.HelpContext)),MB_OK);
  end;
  filelistbox1.Update;
end;

procedure TfrmMain.tbDelPicClick(Sender: TObject);
begin
  if(messagedlg('确定要删除此文件吗?',mtConfirmation,[mbYes, mbNo], 0)=mryes) then
    begin
    if fileexists(filelistbox1.FileName) then
      begin
      deletefile(filelistbox1.FileName);
      end;
    end;
    filelistbox1.Update;
end;

procedure TfrmMain.lstMissionClick(Sender: TObject);
begin
  //pagecontrol.PageIndex:=lstmission.ItemIndex;
  ShowArrayTValueAddress();
end;

//修改任务名称
procedure TfrmMain.lstMissionDblClick(Sender: TObject);
var
  strTemp:string;
begin
  strtemp:=inputbox('提示:','请输入任务名称:',lstmission.Items[lstmission.itemindex]);
  if strtemp<>'' then
  begin
  lstmission.Items[lstmission.ItemIndex]:=strtemp;
  //pagecontrol.Pages[lstmission.ItemIndex]:=strtemp;
  end;
end;
//帮助
procedure TfrmMain.ToolButton2Click(Sender: TObject);
begin
  if frmFindHelp<>nil then
    frmFindHelp.Free;
  frmFindHelp:=TfrmFindHelp.Create(nil);
  frmFindHelp.ShowModal;
  frmFindHelp.Free;
  frmFindHelp:=nil;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  if khook.Active then
    khook.Stop;
  if assigned(khook) then
    khook.Free;
  {*
  if mhook.Active then
    mhook.Stop;
  if assigned(mhook) then
    mhook.Free;
  *}
end;

//键盘钩子
procedure tfrmmain.HookKeyboardDown(const keycode:integer);
begin
  // 键盘消息测试
  if keycode=VK_MULTIPLY then
    begin
    mhwnd:=getforegroundwindow();
    //mhwnd:=getactivewindow();
    //mhwnd:=getcurrentprocess();
    showwindow(mhwnd,SW_MINIMIZE);
    showwindow(self.Handle,SW_NORMAL);
    //setwindowpos(self.Handle,HWND_TOPMOST,self.left,self.Top,self.Width,self.Height,SWP_SHOWWINDOW);
    setforegroundwindow(self.Handle);
    setactivewindow(self.Handle);
    windows.SetFocus(self.handle);
    //self.windowstate:=wsNormal;
    end;
end;

{*
procedure HookMouseMove(const h: HWND; const X, Y: Integer);
begin

end;

procedure HookMouseClick(const h: HWND; const X, Y: Integer);
begin

end;
*}

//返回
procedure TfrmMain.ToolButton5Click(Sender: TObject);
begin
  SHOWWINDOW(mhwnd,SW_SHOWNORMAL);
  setwindowpos(self.Handle,HWND_NOTOPMOST,self.left,self.Top,self.Width,self.Height,SWP_SHOWWINDOW);
  //showwindow(self.Handle,SW_MINIMIZE);
end;

end.
