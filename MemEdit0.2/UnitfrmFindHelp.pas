unit UnitfrmFindHelp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, math, ExtCtrls;

type
  TfrmFindHelp = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    BitBtnOK: TBitBtn;
    Memo1: TMemo;
    procedure BitBtnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFindHelp: TfrmFindHelp=nil;

implementation

{$R *.dfm}

procedure TfrmFindHelp.BitBtnOKClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmFindHelp.FormCreate(Sender: TObject);
begin
  self.Left:=max(0,(screen.Width-self.Width) div 2);
  self.Top:=max(0,(screen.Height-self.Height) div 2);

end;

end.
