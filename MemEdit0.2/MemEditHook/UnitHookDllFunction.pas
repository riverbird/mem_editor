unit UnitHookDllFunction;

// HouSisong 2003.3
// 处理钩子的函数

interface
  uses
    windows,imagehlp, SysUtils, messages, IniFiles, UnitCommon, Memory_Hss;

  var
    MyDLLName : string='MemEditHook.dll';         //本DLL的名称


  //钩子处理回调函数，设置挂钩时用
  function  GetMsgProc(code :integer ; wParam : windows.WPARAM;
               lParam : windows.LPARAM):windows.LRESULT;stdcall;

  function  SetHook():HHOOK; stdcall; //设置挂钩
  procedure DelHook(); stdcall;       //删除挂钩
  function  myGetMsgProc(wParam:windows.WPARAM; LParam:windows.LPARAM):LRESULT ;stdcall; // 按键消息转发回主程序
  procedure SetMsg(const Msg: TMsg);  //将得到的原始消息放入共享内存(文件映射中)，备主程序查用


  

{  // 替换模块中的函数地址(函数所在模块名称,原函数地址,新的函数的地址,要进行替换的模块句柄);
  Function  ReplaceModuleFunctionAddress(const ModuleName: string; const OldFunctionAddress :Pointer;
                                         const NewFunctionAddress :Pointer; const HModuleCaller : HModule):Boolean;
}

implementation

procedure OpenMapHandle();
begin
  MapHandle:=MapGlobalData(GetMapName,sizeof(THookGlobalData), Pointer(HookGlobalData));
end;

function GetHhk():HHOOK;
begin
    if MapHandle=0 then OpenMapHandle();
    result:= HookGlobalData.HookHandle;
end;

function GetAppHWND():HWND;
begin
    if MapHandle=0 then OpenMapHandle();
    result:= HookGlobalData.AppHwnd;
end;

procedure SetMsg(const Msg: TMsg);
begin
    if MapHandle=0 then OpenMapHandle();
    HookGlobalData.Msg:=Msg;
end;

var
  hhk  : HHOOK=0;
function SetHook():HHOOK;  stdcall;
var
  hDll: integer;
begin
  hDll:=windows.LoadLibrary(PChar(MyDLLName)); //Hook DLL的文件名称
  hhk:=windows.SetWindowsHookEx(WH_GETMESSAGE,@GetMsgProc,hDll,0) ;
  windows.FreeLibrary(hDll);
  result:=hhk;
  if result<>0 then windows.SendNotifyMessage(GetAppHWND,WM_HOOKPROC,MSG_HOOKPROC_START,0);
end;

procedure DelHook();  stdcall;
begin
  if hhk<>0 then
  begin
    windows.UnhookWindowsHookEx(hhk);
    hhk:=0;
  end;
end;

//钩子处理回调函数
function GetMsgProc(Code :integer ; wParam : windows.WPARAM; lParam : windows.LPARAM):windows.LRESULT;stdcall;
begin
  if Code>=0 then
    myGetMsgProc(wParam,LParam);
  result:=windows.CallNextHookEx(GetHhk,code,wParam,lParam)
end;



//新的处理过程
function  myGetMsgProc(wParam:windows.WPARAM; LParam:windows.LPARAM):LRESULT ;stdcall;
//var
//  frmHook: TfrmHook;

begin


  //在此填写自己的处理代码
  case PMsg(LParam).message of
    WM_KEYUP,WM_SYSKEYUP:
      begin       //把所有键盘UP消息送回
        windows.SendNotifyMessage(GetAppHWND,WM_HOOKPROC,MSG_HOOKPROC_HOTKEY,
                GetUnitHotKey(
                          byte(PMsg(LParam).wParam)                        ,
                          windows.GetAsyncKeyState(VK_CONTROL) and (1 shl 15) shr 15,
                          windows.GetAsyncKeyState(VK_SHIFT) and (1 shl 15) shr 15  ,
                          windows.GetAsyncKeyState(VK_MENU) and (1 shl 15) shr 15
                              )
                              );
       SetMsg(PMsg(LParam)^);
      end;
    WM_HOOKPROC:  //主程序来了命令,需要处理
      begin
        if PMsg(LParam).wParam=MSG_HOOKPROC_SHOWSELF then   
        begin
          ////
         { frmHook:=TfrmHook.Create(nil);
          frmHook.ShowModal;
          frmHook.Free;
          ////          }
        end;
      end;
  end;
  result:=0;
end;

{
// 替换模块中的函数地址(函数所在模块名称,原函数地址,新的函数的地址,要进行替换的模块句柄);
Function  ReplaceModuleFunctionAddress(const ModuleName: string; const OldFunctionAddress :Pointer;
                                         const NewFunctionAddress :Pointer; const HModuleCaller : HModule):Boolean;


type
   _IMAGE_IMPORT_DESCRIPTOR  = record
    Characteristics_OriginalFirstThunk :  DWORD; // 0 for terminating null import descriptor
                                                  // RVA to original unbound IAT (PIMAGE_THUNK_DATA)

    TimeDateStamp     :  DWORD;               // 0 if not bound,
                                              // -1 if bound, and real date\time stamp
                                              //     in IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT (new BIND)
                                              // O.W. date/time stamp of DLL bound to (Old BIND)

    ForwarderChain    :  DWORD;               // -1 if no forwarders
    Name              :  DWORD;
    FirstThunk        :  DWORD;               // RVA to IAT (if bound this IAT has actual addresses)
  end;
  IMAGE_IMPORT_DESCRIPTOR = _IMAGE_IMPORT_DESCRIPTOR;
  PIMAGE_IMPORT_DESCRIPTOR=^IMAGE_IMPORT_DESCRIPTOR;
var
  ilSize  : DWord;
  PImportDesc : PIMAGE_IMPORT_DESCRIPTOR;

begin
  //ImageDirectoryEntryToData(

end;
}

end.
