unit UnitfrmHook;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmHook = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TfrmHook.Button1Click(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmHook.Button2Click(Sender: TObject);
begin
  self.Close;
end;

end.
