library MemEditHook;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  UnitHookDllFunction in 'UnitHookDllFunction.pas',
  UnitCommon in '..\UnitCommon.pas',
  CPUCounter_Hss in '..\CPUCounter_Hss.pas',
  CPUInf_Hss in '..\CPUInf_Hss.pas',
  Memory_Hss in '..\Memory_Hss.PAS',
  PassWord_Hss in '..\Password_Hss.pas';

{$R *.res}

exports
  SetHook,DelHook;
end.
