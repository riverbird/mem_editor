unit CPUInf_Hss;

          ////////////////////////////////////////////////////////
          //                                                    //
          //   CPU指令支持、系统 信息  作者：侯思松   2002年。  //
          //                                                    //
          //                  HouSisong@263.net                 //
          //                                                    //
          ////////////////////////////////////////////////////////

interface
uses
  windows,SysUtils;

  function  CPUSupportMMX():boolean;    //判断CPU是否支持MMX指令
  function  CPUSupportSSE():boolean;    //判断CPU是否支持SSE指令(SIMD)
  function  CPUSupportSSE2():boolean;   //判断CPU是否支持SSE2指令
  function  SysSupportSSE():boolean;    //判断操作系统是否支持SSE指令(SIMD)
  function  CPUSupportFPU():boolean;    //判断CPU是否有FPU(数学协处理器)
  function  GetCPUOEMString():string;   //取得CPU OEM字符串,判断CPU厂商
  function  GetCPUFamily():integer;     //获得CPU Family  (?86)
  function  CPUSupportCPUID():boolean;  //判断CPU是否支持CPUID指令

  function  CPUSupportMMX2():boolean;   //判断CPU是否支持MMX2指令
  function  CPUSupport3DNow():boolean;  //判断CPU是否支持3DNow指令
  function  CPUSupport3DNow2():boolean; //判断CPU是否支持3DNow2指令

  function  GetComputerName():string;   //得到计算机名称
  function  GetUserName():string;       //得到当前用户名称
  function  GetSYSVersion():string;     //得到操作系统版本信息
  function  GetSYSMemorySize():string;  //得到物理内存信息信息

  function  IsSupportToStr(const B:Boolean):string;

implementation

  type TSupportInf=(SupportYes,SupportNo,SupportNotTest);

  var
    vCPUSupportCPUID  :TSupportInf=SupportNotTest;
    vCPUSupportFPU    :TSupportInf=SupportNotTest;
    vCPUSupportMMX    :TSupportInf=SupportNotTest;
    vCPUSupportSSE    :TSupportInf=SupportNotTest;
    vCPUSupportSSE2   :TSupportInf=SupportNotTest;
    vSysSupportSSE    :TSupportInf=SupportNotTest;
    vCPUSupportMMX2   :TSupportInf=SupportNotTest;
    vCPUSupport3DNow  :TSupportInf=SupportNotTest;
    vCPUSupport3DNow2 :TSupportInf=SupportNotTest;


function  CPUSupportMMX():boolean;  //判断CPU是否支持MMX指令
var
  MMXInf  : integer;
begin

  case vCPUSupportMMX of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupportMMX:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,1
      cpuid
      mov   MMXInf,edx
      pop   ebx
    end;
    MMXInf:=MMXInf and (1 shl 23);  //检测edx第23位
    if MMXInf=$800000 then
    begin
      result:=true;
      vCPUSupportMMX:=SupportYes;
    end
    else
    begin
      result:=false;
      vCPUSupportMMX:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupportMMX:=SupportNo;
  end;
end;

function  CPUSupportMMX2():boolean;  //判断CPU是否支持MMX指令
var
  MMX2Inf  : Cardinal	;
begin

  case vCPUSupportMMX2 of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupportMMX2:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,$80000000
      cpuid
      mov   MMX2Inf,eax
      pop   ebx
    end;
    if MMX2Inf>$80000000 then
    begin
      asm
        push  ebx
        mov   eax,$80000001
        cpuid
        mov   MMX2Inf,edx
        pop   ebx
      end;
      MMX2Inf:=MMX2Inf and (1 shl 22);  //检测edx第22位
      if MMX2Inf=(1 shl 22) then
      begin
        result:=true;
        vCPUSupportMMX2:=SupportYes;
      end
      else
      begin
        result:=false;
        vCPUSupportMMX2:=SupportNo;
      end;
    end
    else
    begin
        result:=false;
        vCPUSupportMMX2:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupportMMX2:=SupportNo;
  end;
end;

function  CPUSupport3DNow():boolean;  //判断CPU是否支持3DNow指令
var
  M3DNowInf  : Cardinal	;
begin

  case vCPUSupport3DNow of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupport3DNow:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,$80000000
      cpuid
      mov   M3DNowInf,eax
      pop   ebx
    end;
    if M3DNowInf>$80000000 then
    begin
      asm
        push  ebx
        mov   eax,$80000001
        cpuid
        mov   M3DNowInf,edx
        pop   ebx
      end;
      M3DNowInf:=M3DNowInf and (1 shl 31);  //检测edx第31位
      if (M3DNowInf and (1 shl 31))<>0 then
      begin
        result:=true;
        vCPUSupport3DNow:=SupportYes;
      end
      else
      begin
        result:=false;
        vCPUSupport3DNow:=SupportNo;
      end;
    end
    else
    begin
        result:=false;
        vCPUSupport3DNow:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupport3DNow:=SupportNo;
  end;
end;


function  CPUSupport3DNow2():boolean;  //判断CPU是否支持3DNow2指令
var
  M3DNow2Inf  : Cardinal	;
begin

  case vCPUSupport3DNow2 of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupport3DNow2:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,$80000000
      cpuid
      mov   M3DNow2Inf,eax
      pop   ebx
    end;
    if M3DNow2Inf>$80000000 then
    begin
      asm
        push  ebx
        mov   eax,$80000001
        cpuid
        mov   M3DNow2Inf,edx
        pop   ebx
      end;
      M3DNow2Inf:=M3DNow2Inf and (1 shl 30);  //检测edx第30位
      if M3DNow2Inf=(1 shl 30) then
      begin
        result:=true;
        vCPUSupport3DNow2:=SupportYes;
      end
      else
      begin
        result:=false;
        vCPUSupport3DNow2:=SupportNo;
      end;
    end
    else
    begin
        result:=false;
        vCPUSupport3DNow2:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupport3DNow2:=SupportNo;
  end;
end;

function  CPUSupportSSE():boolean;  //判断CPU是否支持SSE指令(SIMD)
var
  SSEInf  : integer;
begin

  case vCPUSupportSSE of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupportSSE:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,1
      cpuid
      mov   SSEInf,edx
      pop   ebx
    end;
    SSEInf:=SSEInf and (1 shl 25);  //检测edx第25位
    if SSEInf=(1 shl 25) then
    begin
      result:=true;
      vCPUSupportSSE:=SupportYes;
    end
    else
    begin
      result:=false;
      vCPUSupportSSE:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupportSSE:=SupportNo;
  end;
end;

function  CPUSupportSSE2():boolean;  //判断CPU是否支持SSE2指令
var
  SSE2Inf  : integer;
begin

  case vCPUSupportSSE2 of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupportSSE2:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,1
      cpuid
      mov   SSE2Inf,edx
      pop   ebx
    end;
    SSE2Inf:=SSE2Inf and (1 shl 26);  //检测edx第26位
    if SSE2Inf=(1 shl 26) then
    begin
      result:=true;
      vCPUSupportSSE2:=SupportYes;
    end
    else
    begin
      result:=false;
      vCPUSupportSSE2:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupportSSE2:=SupportNo;
  end;
end;

function  SysSupportSSE():boolean;  //判断操作系统是否支持SSE指令(SIMD)
//触发异常来判断
begin

  case vSysSupportSSE of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
  end;

  try
    asm
        //xorps   xmm0,xmm0
        movups     xmm0,xmm0
    end;
    result:=true;
    vSysSupportSSE:=SupportYes;
  except
    result:=false;
    vSysSupportSSE:=SupportNo;
  end;
end;

function  CPUSupportFPU():boolean;  //判断CPU是否有FPU(数学协处理器)
var
  FPUInf  : integer;
begin

  case vCPUSupportFPU of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
    SupportNotTest :
      begin
        if not CPUSupportCPUID then
        begin
          result:=false;
          vCPUSupportFPU:=SupportNo;
          exit;
        end;
      end;
  end;

  try
    asm
      push  ebx
      mov   eax,1
      cpuid
      mov   FPUInf,edx
      pop   ebx
    end;
    FPUInf:=FPUInf and $1;  //检测第0位
    if FPUInf=$1 then
    begin
      result:=true;
      vCPUSupportFPU:=SupportYes;
    end
    else
    begin
      result:=false;
      vCPUSupportFPU:=SupportNo;
    end;
  except
    result:=false;
    vCPUSupportFPU:=SupportNo;
  end;
end;

function  GetCPUFamily():integer;  //获得CPU Family  (?86)
{
3 - 386 
4 - i486 
5 - Pentium ...
6 - Pentium Pro or Pentium II ... 
2 - Dual Processors 
}
var
  CPUFamily  : integer;
begin
  try
    asm
      push  ebx
      mov   eax,1
      cpuid
      mov   CPUFamily,eax
      pop   ebx
    end;
    CPUFamily:=(CPUFamily and $f00) shr 8 ;
    result:= CPUFamily ;
  except
    result:=0;
  end;
end;

function  GetCPUOEMString():string;  //取得CPU OEM字符串,判断CPU厂商
var
  OEMString   :string;
  p           :pointer;
begin
  OEMString:='            ';
  p:=@OEMString[1];
  try
    asm
      push  ebx

      mov   eax,0
      cpuid
      mov   eax,p
      mov   [eax],ebx
      mov   [eax+4],edx
      mov   [eax+8],ecx
      
      pop   ebx
    end;
    result:=OEMString;
  except
    result:='';
  end;
end;

function  CPUSupportCPUID():boolean;
var
  CPUIDInfOld :Cardinal;
  CPUIDInfNew :Cardinal;
begin

  case vCPUSupportCPUID of
    SupportYes  : begin result:=true ; exit; end;
    SupportNo   : begin result:=false ; exit; end;
  end;

  try
    asm
    
          pushfd                   // 保存原 EFLAGS
          pop     eax
          mov     edx,eax
          mov     CPUIDInfOld,eax  //

          xor     eax,00200000h    // 改写 第21位
          push    eax
          popfd                    // 改写 EFLAGS

          pushfd                   // 保存新 EFLAGS
          pop     eax              
          mov     CPUIDInfNEW,eax

          push    edx              // 恢复原 EFLAGS
          popfd

    end;
    
    if CPUIDInfOld<>CPUIDInfNew then
    begin
      result:=true;               // EFLAGS 第21位 可以改写
      vCPUSupportCPUID:=SupportYes;
    end
    else
    begin
      result:=false;
      vCPUSupportCPUID:=SupportNo;
    end;
    
  except
    result:=false;
    vCPUSupportCPUID:=SupportNo;
  end;
end;



function  GetComputerName():string;   //得到计算机名称
var
  sName : string;
  L     : Dword;
begin
  L:=1024;
  setlength(sName,L);
  if windows.GetComputerName(Pchar(sName),L) then
  begin
    setlength(sName,strLen(PChar(sName)));
    result:=sName;
  end
  else
  begin
    result:='';
  end;
end;

function  GetUserName():string;       //得到当前用户名称
var
  sName : string;
  L     : Dword;
begin
  L:=1024;
  setlength(sName,L);
  if windows.GetUserName(Pchar(sName),L) then
  begin
    setlength(sName,strLen(PChar(sName)));
    result:=sName;
  end
  else
  begin
    result:='';
  end;
end;

function  GetSYSVersion():string;     //得到操作系统版本信息
var
  strVs  : string;
  vsInfo : TOSVersionInfo;
begin
  vsInfo.dwOSVersionInfoSize:=sizeof(TOSVersionInfo);
  if windows.GetVersionEx(vsInfo) then
  begin
    strVs:=inttostr(vsInfo.dwMajorVersion)+'.'+inttostr(vsInfo.dwMinorVersion)
      +'.'+inttostr(vsInfo.dwBuildNumber);
    result:=strVs;
  end
  else
  begin
    result:='';
  end;
end;

function  GetSYSMemorySize():string;  //得到物理内存信息信息
var
  strMem  : string;
  memInfo : TMemoryStatus;
begin
  memInfo.dwLength:=sizeof(TMemoryStatus);
  memInfo.dwTotalPhys:=0;
  windows.GlobalMemoryStatus(memInfo) ;
  strMem:=formatfloat('#######0.00',(memInfo.dwTotalPhys/1024.0/1024.0))+'M';
  result:=strMem;
end;

function  IsSupportToStr(const B:Boolean):string;
begin
  if B then
    result:='支持'
  else
    result:='不支持';
end;


//==============================================================================

            {    CPU指令支持等信息  作者：侯思松   2002年。    }

{CPU指令支持等信息库单元结束}

end.
