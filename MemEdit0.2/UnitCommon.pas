unit UnitCommon;

interface
  uses
    windows, SysUtils, messages, math,
     CPUInf_Hss, PassWord_Hss,Menus;

  {$define Hssdebug}  //是否调试

type
  TShortCut = Low(Word)..High(Word);

  const
    WM_HOOKPROC        = WM_USER+100;//100-$7fff  //自定义消息号


    MSG_HOOKPROC_START    =  1;                               //Hook的代码注入开始消息
    MSG_HOOKPROC_CLOSE    =MSG_HOOKPROC_START + 2;            //Hook的代码注入结束消息

    MSG_HOOKPROC_HOTKEY   =MSG_HOOKPROC_START + 3;            //Hook代码宿主产生按键消息

    MSG_HOOKPROC_SHOWSELF =MSG_HOOKPROC_START + 4;            //显示自己窗口的消息




    function  GetAGUIName(GUID: string):string;  //产生一个GUID，不同的电脑,不同的原GUID产生不同的新的GUID
    function  GetRandomGUID():string;            //产生一个随机GUID字符串

    function  GetMapName():string;               //得到用来通信的映射文件名称，不同的电脑产生不同的名称
    function  GetRandomBlurFileName():string;    //得到一个随机文件名称

    //模拟硬件键盘按键(虚拟键码，系统键的状态);//(系统键的状态,1表示按下，0表示没有按下)
    procedure SendInput(const sVK,sCtrl,sShift,sAlt:integer);     stdcall; overload;
    //模拟硬件键盘按键(热键);
    procedure SendInput(const HotKey :TShortCut);  stdcall; overload;

    //合成热键值(虚拟键码，系统键的状态);
    function  GetUnitHotKey(const sVK,sCtrl,sShift,sAlt:integer):TShortCut;

    function HotKeyToStr(const HotKey: TShortCut):string;overload;
    function HotKeyToStr(const sVK,sCtrl,sShift,sAlt:integer):string;overload;
    function StrToHotKey(const strHotKey: string):TShortCut;

    function StrIsInt(const xValue:string):Longbool;
    function StrIsFloat(const xValue:string):Longbool;
    function GetFloatDX(const floatStr:string):extended;  //按有效位数获得允许误差

    function HwndToProcessesID(const Xhwnd : HWND):THandle;

    function IsDebuggerPresent():longBool;

type
   THookGlobalData = record
      AppHwnd     : HWND;
      AppLeft     : integer;
      AppTop      : integer;
      AppWidth    : integer; 
      AppHeight   : integer;
      HookHwnd    : HWND;
      HookHandle  : HHOOK;
      Msg         : TMSG;
      GetProcParhNameHwnd :Hwnd;
      StrPathName : array [0..1024-1] of char;
   end;
   type PTHookGlobalData = ^THookGlobalData;
   var  HookGlobalData   : PTHookGlobalData=nil;
   var  MapHandle  : THandle=0;

implementation

function HwndToProcessesID(const Xhwnd : HWND):THandle;
var
  PID : THandle;
begin
  windows.GetWindowThreadProcessId(XHwnd,@PID);
  result:=PID;
end;

function StrIsHex(const xValue:string):Longbool;
var
  i  : integer;
begin
  if (Length(xValue)<2) or (xValue[1]<>'$') then
  begin
    result:=false;
    exit;
  end;

  for i:=2 to length(xValue) do
  begin
    if not (xValue[i] in ['0'..'9','A'..'F','a'..'f']) then
    begin
      result:=false;
      exit;
    end;
  end;
  result:=true;
end;

function StrIsInt(const xValue:string):Longbool;
var
  i  : integer;
begin
  for i:=1 to length(xValue) do
  begin
    if not (xValue[i] in [' ','0'..'9','+','-']) then
    begin
      result:=StrIsHex(xValue);
      exit;
    end;
  end;
  try
    strtoint64(xValue);
    result:=true;
  except
    result:=false;
  end;
end;

function StrIsFloat(const xValue:string):Longbool;
var
  i  : integer;
begin
  for i:=1 to length(xValue) do
  begin
    if not (xValue[i] in [' ','0'..'9','.','E','e','+','-']) then
    begin
      result:=false;
      exit;
    end;
  end;
  try
    strtofloat(xValue);
    result:=true;
  except
    result:=false;
  end;
end;

function GetFloatDX(const floatStr:string):Extended; //按有效位数的获得允许误差
var                                                                  
  i    : integer;
  index: integer;
  DotIndex: integer;
  myStr : string;
begin             //没有考虑 非法格式
  DotIndex:=maxint;
  myStr:=floatStr;
  index:=0;
  for i:=1 to length(myStr) do
  begin
    if floatStr[i]='.' then
      DotIndex:=i
    else if floatStr[i] in ['e','E'] then
    begin
      break;
    end
    else if myStr[i] in ['0'..'9'] then
    begin
      myStr[i]:='0';
      index:=i;
    end;
  end;
  if index>DotIndex then
    myStr:=copy(myStr,1,index)+'9999999999999999'+copy(myStr,index+1,length(myStr)-index)
  else if DotIndex=maxint then
    myStr:= '0.9999999999999999'+copy(myStr,index+1,length(myStr)-index)
  else
    myStr:= '0.9999999999999999'+copy(myStr,DotIndex+1,length(myStr)-DotIndex);

  result:=abs(strtofloat(myStr));
end;


function  GetAGUIName(GUID: string):string;
var
  fName : string;
const
  GUID1 : string='4728AD0E6E3348EE';
  GUID2 : string='9D96E66AE003DA6A';
begin
  fName:=GetNewData(GUID1,GetCPUOEMString);
  fName:=GetNewData(fName,GetUserName());
  fName:=GetNewData(GUID,fName);
  fName:=GetNewData(fName,GetComputerName());
  fName:=GetNewData(GUID2,fName);
  fName:=GetNewData(fName,inttoHex(GetCPUFamily,8));
  fName:=DataToHEX(FName);
  fName:='{'+copy(fName,1,8)+'-'+copy(fName,9,4)+'-'+
      copy(fName,13,4)+'-'+copy(fName,17,4)+'-'+copy(fName,21,12)+'}';
  result:=fName;
end;

function  GetMapName():string;
begin
  result:=GetAGUIName('7A5B4A8A19F84F25868401EB7A90CC88');
end;

function  GetRandomGUID():string;
var
  GUID  : TGUID;
begin
  if CreateGUID(GUID)=S_OK  then
    result:=GUIDToString(GUID)
  else
  begin
    Randomize;
    GUID.D1:=random(Maxint);
    GUID.D2:=random(MaxWord);
    GUID.D3:=random(MaxWord);
    Pint64(@GUID.D4[0])^:=int64(random(MaxWord))*random(MaxWord);
    result:=GUIDToString(GUID);
  end;
end;

function  GetRandomBlurFileName():string;    //得到一个随机文件名称
var
  tempFileName :string;
begin
  setlength(tempFileName,260);
  windows.GetTempPath(260,PChar(tempFileName));
  setlength(tempFileName,strlen(PChar(tempFileName)));
  tempFileName:=tempFileName+GetAGUIName(GetRandomGUID())+'.TMP';
  result:=tempFileName;
end;

procedure SendInput(const HotKey :TShortCut);  stdcall; overload;
var
  sCtrl   : boolean;
  sShift  : boolean;
  sAlt    : boolean;
  sVK     : integer;

  sInput  : TInput;

  procedure KeyDown(wVk:integer);
  begin
    sInput.ki.dwFlags:=0;
    sInput.ki.wVk:=wVk;
    windows.SendInput(1,sInput,sizeof(TInput));
  end;
  procedure KeyUP(wVk:integer);
  begin
    sInput.ki.dwFlags:=KEYEVENTF_KEYUP;
    sInput.ki.wVk:=wVk;
    windows.SendInput(1,sInput,sizeof(TInput));
  end;

begin
  sVK:=byte(HotKey);

  sCtrl :=(HotKey and (1 shl 14))>0;
  sShift:=(HotKey and (1 shl 13))>0;
  sAlt  :=(HotKey and (1 shl 15))>0;

  sInput.Itype:=INPUT_KEYBOARD;
  sInput.ki.wScan:=0;
  sInput.ki.time:=1;
  sInput.ki.dwExtraInfo:=0;

  if sCtrl  then KeyDown(VK_CONTROL);
  if sShift then KeyDown(VK_SHIFT);
  if sAlt   then KeyDown(VK_MENU);

  if (sVK in [1..254]) then
  begin
    KeyDown(sVK);
    KeyUp(sVK);
  end;
  
  if sAlt   then KeyUp(VK_MENU);
  if sShift then KeyUp(VK_SHIFT);
  if sCtrl  then KeyUp(VK_CONTROL);

end;

procedure SendInput(const sVK,sCtrl,sShift,sAlt:integer);     stdcall; overload;
begin
  SendInput(GetUnitHotKey(sVK,sCtrl,sShift,sAlt));
end;

function  GetUnitHotKey(const sVK,sCtrl,sShift,sAlt:integer):TShortCut;
var
  HotKey :TShortCut;
begin
  HotKey:=sVk;
  HotKey:=HotKey or (sCtrl shl 14);
  HotKey:=HotKey or (sShift shl 13);
  HotKey:=HotKey or (sAlt shl 15);
  result:=HotKey;
end;


function HotKeyToStr(const HotKey: TShortCut):string;overload;
begin
  result:=ShortCutToText(HotKey);
end;

function HotKeyToStr(const sVK,sCtrl,sShift,sAlt:integer):string;overload;
begin
  result:=ShortCutToText(GetUnitHotKey(sVK,sCtrl,sShift,sAlt));
end;

function StrToHotKey(const strHotKey: string):TShortCut;
begin
  result:=TextToShortCut(strHotKey);
end;

//function IsDebuggerPresent; external kernel32 name 'IsDebuggerPresent';
function IsDebuggerPresent():longBool;
{$ifdef Hssdebug}
begin
  result:= false;
end;
{$else}
var
  IsByDebug  : DWORD;
begin

  asm

        //////  Check Debugger
        mov     eax,fs:[$18]
        mov     eax,[eax+$30]
        movzx   eax,[eax+$02]
        mov     IsByDebug,eax

  end;

  result:= (DWord(IsByDebug) and 5)<>0;

end;
{$endif}

end.
