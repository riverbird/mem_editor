unit UnitMemSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Forms,
  Buttons, RXCtrls, StdCtrls, Controls, ExtCtrls;

type
  TfrmMemSelect = class(TForm)
    BitBtnOK: TBitBtn;
    BitBtnCancel: TBitBtn;
    GroupBox1: TGroupBox;
    RxCheckListBoxModuleList: TRxCheckListBox;
    BitBtnRefresh: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure BitBtnOKClick(Sender: TObject);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    IsOkClose  : Boolean;
    ProcessId  : Thandle;
    PTSysSet   : pointer;
  end;


implementation
  uses
    UnitAboutAddress;


{$R *.dfm}

procedure TfrmMemSelect.FormCreate(Sender: TObject);
begin
  IsOkClose:=false;
  ProcessId:=0;
end;

procedure TfrmMemSelect.BitBtnCancelClick(Sender: TObject);
begin
  self.IsOkClose:=false;
  self.Close;
end;

procedure TfrmMemSelect.BitBtnOKClick(Sender: TObject);
begin
  self.IsOkClose:=true;
  self.Close;
end;

procedure TfrmMemSelect.BitBtnRefreshClick(Sender: TObject);
begin
  TMemEditSysSet(self.PTSysSet).RefreshModuleList;
end;

procedure TfrmMemSelect.BitBtn1Click(Sender: TObject);
var
  i  : integer;
begin
  for i:=0 to self.RxCheckListBoxModuleList.Items.Count-1 do
    self.RxCheckListBoxModuleList.Checked[i]:=true;
  self.RxCheckListBoxModuleList.Refresh;
end;

procedure TfrmMemSelect.BitBtn2Click(Sender: TObject);
var
  i  : integer;
begin
  for i:=0 to self.RxCheckListBoxModuleList.Items.Count-1 do
    self.RxCheckListBoxModuleList.Checked[i]:=false;
  self.RxCheckListBoxModuleList.Refresh;
end;

end.
