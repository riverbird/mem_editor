unit UnitfrmSystemSet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ActnList, Menus,UnitCommon,
  ExtCtrls;

type
  TfrmSystemSet = class(TForm)
    PageControlMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheetType: TTabSheet;
    BitBtnOK: TBitBtn;
    BitBtnCancel: TBitBtn;
    Label1: TLabel;
    HotKeyApp: THotKey;
    Label2: TLabel;
    RadioButtonProcStop: TRadioButton;
    RadioButtonProcNoStop: TRadioButton;
    GroupBox1: TGroupBox;
    MemoModuleSuffix: TMemo;
    GroupBox2: TGroupBox;
    RadioButtonFiltrateNO: TRadioButton;
    RadioButtonFiltrateSYS: TRadioButton;
    RadioButtonFiltrateALL: TRadioButton;
    ButtonDefaultType: TButton;
    ButtonDefaultHOT: TButton;
    CheckBoxFindAsk: TCheckBox;
    TabSheet5: TTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ButtonDefaultCapabilitySet: TButton;
    EditFindBuffuer: TEdit;
    Label6: TLabel;
    EditShowAddressMaxCount: TEdit;
    Label7: TLabel;
    EditFindMaxMem: TEdit;
    Label8: TLabel;
    ButtonDefaultMem: TButton;
    PageControlType: TPageControl;
    TabSheetTypeInt: TTabSheet;
    TabSheetTypeFloat: TTabSheet;
    TabSheetTypeString: TTabSheet;
    GroupBox3: TGroupBox;
    CheckBoxTypeIntByte: TCheckBox;
    CheckBoxTypeIntWord: TCheckBox;
    CheckBoxTypeIntDword: TCheckBox;
    CheckBoxTypeIntDDWord: TCheckBox;
    GroupBox4: TGroupBox;
    CheckBoxTypeFloatSingle: TCheckBox;
    CheckBoxTypeFloatDouble: TCheckBox;
    CheckBoxTypeFloatExtended: TCheckBox;
    BitBtnAppily: TBitBtn;
    TabSheetTypeBlur: TTabSheet;
    GroupBox5: TGroupBox;
    CheckBoxTypeBlurByte: TCheckBox;
    CheckBoxTypeBlurWord: TCheckBox;
    CheckBoxTypeBlurDword: TCheckBox;
    CheckBoxTypeBlurDDWord: TCheckBox;
    CheckBoxTypeBlurSingle: TCheckBox;
    CheckBoxTypeBlurDouble: TCheckBox;
    CheckBoxTypeBlurExtended: TCheckBox;
    GroupBox6: TGroupBox;
    CheckBoxTypeStringAString: TCheckBox;
    CheckBoxTypeStringWString: TCheckBox;
    ActionList: TActionList;
    ActionEnableAppily: TAction;
    CheckBoxModuleSuffixAsk: TCheckBox;
    Image1: TImage;
    Label9: TLabel;
    procedure ButtonDefaultHOTClick(Sender: TObject);
    procedure ButtonDefaultMemClick(Sender: TObject);
    procedure ButtonDefaultTypeClick(Sender: TObject);
    procedure ButtonDefaultCapabilitySetClick(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure BitBtnAppilyClick(Sender: TObject);
    procedure BitBtnOKClick(Sender: TObject);
    procedure ActionEnableAppilyExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HotKeyAppChange(Sender: TObject);
  private
    { Private declarations }
    
  public
    { Public declarations }
    PSysSet : Pointer;
    IsOkClose:boolean;
  end;

implementation

  uses  UnitAboutAddress;


{$R *.dfm}

procedure TfrmSystemSet.ButtonDefaultHOTClick(Sender: TObject);
begin
  TMemEditSysSet(PSysSet).SetDefultHot();
end;

procedure TfrmSystemSet.ButtonDefaultMemClick(Sender: TObject);
begin
  TMemEditSysSet(PSysSet).SetDefultFiltrate();
end;

procedure TfrmSystemSet.ButtonDefaultTypeClick(Sender: TObject);
begin
  TMemEditSysSet(PSysSet).SetDefultType();
end;

procedure TfrmSystemSet.ButtonDefaultCapabilitySetClick(Sender: TObject);
begin
  TMemEditSysSet(PSysSet).SetDefultBuffer();
end;

procedure TfrmSystemSet.BitBtnCancelClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmSystemSet.BitBtnAppilyClick(Sender: TObject);
begin
  if (self.BitBtnAppily.Enabled) and
    (TMemEditSysSet(PSysSet).GetUserSet(TMemEditSysSet(PSysSet).SysSet)) then
  begin
    self.BitBtnAppily.Enabled:=false;
    TMemEditSysSet(PSysSet).ShowSysSet(TMemEditSysSet(PSysSet).SysSet);
    application.ProcessMessages;
  end;
end;

procedure TfrmSystemSet.BitBtnOKClick(Sender: TObject);
begin
  if TMemEditSysSet(PSysSet).GetUserSet(TMemEditSysSet(PSysSet).SysSet) then
  begin
    TMemEditSysSet(PSysSet).SaveToIniSet();
    self.IsOkClose:=true;
    self.Close;
  end;
end;

procedure TfrmSystemSet.ActionEnableAppilyExecute(Sender: TObject);
begin
  if self.Visible then
    self.BitBtnAppily.Enabled:=true;
end;

procedure TfrmSystemSet.FormCreate(Sender: TObject);
begin
  IsOkClose:=false;
end;

procedure TfrmSystemSet.HotKeyAppChange(Sender: TObject);
begin
  self.HotKeyApp.Hint:=HotKeyToStr(self.HotKeyApp.HotKey);
  ActionEnableAppilyExecute(Sender);
end;

end.
